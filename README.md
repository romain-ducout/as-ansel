# ANSEL

Ansel is Aerosense's solution to visualize the content of a drone and the results of its flights.

Ansel is capable of the following:

- Visualize the result of a given flight: its images and GPS logs.

- Extract and visualize PX4 logs.

- Follow the drone in flight using WIFI connection.

- Display stream of thermal camera.

- Clear flight data from the drone.

Currently Ansel is compatible with the following models:
- AS-MC03

## Getting Started

These instruction will show you how to set up Ansel on a drone and run it.

### Prerequisites

Node.js is required to run on your system: 

```
sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install nodejs
sudo npm install -g nodemon
```

### Installing

Clone the repository at the home directory of your device:

```
git clone git@bitbucket.org:romain-ducout/as-ansel.git
cd as-ansel
```

Update Ansel:

```
git checkout master (or desired branch)
git pull
git fetch
```

Install Node.js dependencies:

```
npm install
npm run build-dev
```

Build using WebPack:

```
npm run build-dev
```

## Running the tests

Automated tests can be run using Jest with the following command:

```
npm run test
```

## Deploying the documentation

You can generate the documentation for Ansel with the following command:

```
npm run build-doc
```

This will generate 2 documentation, 1 for the client web application and 1 for the server application running on drone.
Documentation are generated in the docs/out folder.

You can also generate documentations separately:

```
npm run build-doc-server
npm run build-doc-client
```

## Deployment

Ansel comes installed and deployed on AS-MC03 drones in flashed images.

## Dependencies

Here is a list of the NPM dependencies of Ansel application:

*  [body-parser](https://www.npmjs.com/package/body-parser): Allows parsing of JSON body of requests.

*  [cesium](https://www.npmjs.com/package/cesium): 3D map viewer.

*  [express](https://www.npmjs.com/package/express): Route framework.

*  [fast-exif](https://www.npmjs.com/package/fast-exif): Fast EXIF manipulation in pictures.

*  [jpeg-turbo](https://www.npmjs.com/package/jpeg-turbo): Fast JPEG manipulation.

*  [material-components-web](https://www.npmjs.com/package/material-components-web): Material U.I components.

*  [plotly.js](https://www.npmjs.com/package/plotly.js): Plot graphs.

*  [request](https://www.npmjs.com/package/request): Perform requests on server side.

*  [roslib](https://www.npmjs.com/package/roslib): Communicate with ROS bridge.

*  [sharp](https://www.npmjs.com/package/sharp): Image manipulation. 

*  [socket.io](https://www.npmjs.com/package/socket.io): Socket communication for client-server.

*  [zip-js](https://www.npmjs.com/package/zip-js): Zip and unzip files.

## Authors

* **Romain Ducout**

