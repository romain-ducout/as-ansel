/**
 * Ansel root module that launches the application.
 * 
 * @module
 * @author Romain Ducout
 */
process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();

const Ansel = require('./src/server/ansel');
const AnselConf = require('./ansel.config.js');

AnselConf.APP_PATH = process.env.NODE_PATH;

var ansel = new Ansel(AnselConf);
ansel.start();

module.exports = ansel;