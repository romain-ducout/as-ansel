#!/usr/bin/env python

__author__ = "Romain Ducout"
__version__ = "1.1"

import serial
import time
import sys
import os
import re

TIMEOUT = 50.0
LOG_DIR = "/fs/microsd/log"

def _get_log_list(ser):
    res = ""

    log_folder_pattern = re.compile("\d\d\d\d-\d\d-\d\d")

    ls_res = _exec_cmd(ser, "ls " + LOG_DIR)
    for line in ls_res.splitlines():
        curr_folder = line.strip()[:-1]
        
        if(log_folder_pattern.match(curr_folder)):
            ls_sub_res = _exec_cmd(ser, "ls " + LOG_DIR + "/" + curr_folder)
            for sub_line in ls_sub_res.splitlines():
                if(sub_line.endswith(".bin")):
                    if(len(res) > 0):
                        res = res + "\r\n"
                    res = res + curr_folder + "-" + sub_line.strip()[:-4]

    return res

def _get_param_list(ser):
    param_res = _exec_cmd(ser, "param show")
    return param_res

def _wait_for_string(ser, s):
    t0 = time.time()
    buf = []
    res = []
    while (True):
        c = ser.read()
        buf.append(c)
        if len(buf) > len(s):
            res.append(buf.pop(0))
        if "".join(buf) == s:
            break
        if TIMEOUT > 0.0 and time.time() - t0 > TIMEOUT:
            raise Exception("Timeout while waiting for: " + s)
    return "".join(res)


def _exec_cmd(ser, cmd):
    ser.write(cmd + "\n")
    ser.flush()
    _wait_for_string(ser, cmd + "\r\n")
    return _wait_for_string(ser, "nsh> \x1b[K")


def _get_file(ser, fn, fn_out, force):
    if not force:
        # Check if file already exists with the same size
        try:
            os.stat(fn_out)
        except:
            pass
        else:
            return

    cmd = "dumpfile " + fn
    ser.write(cmd + "\n")
    ser.flush()
    _wait_for_string(ser, cmd + "\r\n")
    res = _wait_for_string(ser, "\n")
    if res.startswith("OK"):
        # Got correct responce, open temp file
        fn_out_part = fn_out + ".part"
        fout = open(fn_out_part, "wb")

        size = int(res.split()[1])
        sys.stdout.write(" [%i bytes] " % size)
        n = 0
        while (n < size):
            buf = ser.read(min(size - n, 8192))
            n += len(buf)
            sys.stdout.write(".")
            sys.stdout.flush()
            fout.write(buf)
        fout.close()
        os.rename(fn_out_part, fn_out)
    else:
        raise Exception("Error reading file")
    _wait_for_string(ser, "nsh> \x1b[K")



def _usage():
    print """
Usage: python px4_utils.py device speed command args...
\tdevice\tSerial device
\tspeed\tSerial baudrate
\tcommand\tCommand to execute
"""

def _main():
    if(len(sys.argv) < 4):
        _usage()
        exit(1)
    dev = sys.argv[1]
    speed = sys.argv[2]
    cmd = sys.argv[3]
    args = sys.argv[4:]

    # Connect to serial port
    ser = serial.Serial(dev, speed, timeout=0.2)

    try:
        if cmd == "get_log_list":
            print _get_log_list(ser)
        if cmd == "get_log":
            if(len(args) < 2):
                exit(1)
            log_id = args[0]
            path_in = LOG_DIR + "/" + log_id[:10] + "/" + log_id[11:] + ".bin"
            path_out = args[1] + "/" + log_id + ".bin"
            _get_file(ser, path_in, path_out, True)
        if cmd == "delete_log":
            if(len(args) < 2):
                exit(1)
            pass
        if cmd == "get_parameters_list":
            print _get_param_list(ser)

    except Exception:
        pass

    ser.close()

if __name__ == "__main__":
    _main()
