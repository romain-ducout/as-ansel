const webpack = require('webpack');
const path = require('path');

// Plugins
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const I18nPlugin = require('i18n-webpack-plugin');


module.exports = (env, options) => {
    const dev = (options.mode === 'development');

    var languages = {
        'en': require('./src/client/assets/i18n/en.json'),
        'ja': require('./src/client/assets/i18n/ja.json')
    };

    let cssLoaders = [
        MiniCssExtractPlugin.loader,
        {
            loader: 'css-loader',
            options: {
                minimize: !dev
            }
        }
    ];

    return Object.keys(languages).map(function(language) {

        let config = {
            entry: './src/client/index.js',

            watch: dev,
            devtool: dev ? 'cheap-module-eval-source-map' : false,

            output: {
                path: path.resolve(__dirname, './dist'),
                publicPath: './',
                filename: `ansel.${language}.js`
            },

            resolve: {
                modules: [
                    path.resolve(__dirname, './src/client/scripts'),
                    'node_modules'
                ],
                alias: {
                    'css': path.resolve(__dirname, './src/client/css'),
                    'assets': path.resolve(__dirname, './src/client/assets'),
                    // Libraries
                    'zip-js': 'zip-js/WebContent/'
                }
            },

            module: {
                rules: [
                    {
                        test: /\.js$/,
                        use: 'babel-loader'
                    },
                    {
                        test: /\.css$/,
                        use: cssLoaders
                    },
                    {
                        test: /\.(html)$/,
                        use: {
                            loader: 'html-loader',
                            options: {
                                attrs: [':data-src'],
                                interpolate: true,
                                minimize: true
                            }
                        }
                    },
                    {
                        test: /\.(woff2|gif|png|jpg|svg)$/,
                        use: {
                            loader: 'url-loader',
                            options: {
                                name: '[name].[ext]',
                                limit: 8192
                            }
                        }
                    },
                    {
                        test: /\.(glb)$/,
                        use: 'file-loader?name=[name].[ext]'
                    }
                ]
            },
            plugins: [
                new MiniCssExtractPlugin({
                    filename: '[name].css',
                    disable: dev
                }),
                new HtmlWebPackPlugin({
                    template: './src/client/index.html',
                    favicon: './src/client/assets/imgs/favicon.png',
                    filename: `index.${language}.html`
                }),
                new I18nPlugin(
                    languages[language]
                )
            ]
        };
        return config;
    });
};