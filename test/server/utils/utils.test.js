const Request = require('request');

global.__ansel_root = '../../../src/server/';
const Utils = require('utils/utils');

describe('Test utils/utils', () => {
    var ansel;

    test('Test getROS()', (done) => {
        Utils.getROS()
            .then(function(iROS){
                expect(iROS).toBeDefined();
                done();
            })
            .catch(function(){
                fail('Error in getROS()')
            })
    });

    test('Test latitudeDMSToDecimal(iPosition, iOrientation)', () => {
        expect(Utils.latitudeDMSToDecimal([0, 0, 0], 'N')).toBe(0);
        expect(Utils.latitudeDMSToDecimal([35, 0, 0], 'N')).toBe(35);        
        expect(Utils.latitudeDMSToDecimal([15, 15, 0], 'S')).toBe(-15.25);
        expect(Utils.latitudeDMSToDecimal([15, 15, 9], 'S')).toBe(-15.2525);
    });

    test('Test longitudeDMSToDecimal(iPosition, iOrientation)', () => {
        expect(Utils.longitudeDMSToDecimal([0, 0, 0], 'E')).toBe(0);
        expect(Utils.longitudeDMSToDecimal([35, 0, 0], 'E')).toBe(35);        
        expect(Utils.longitudeDMSToDecimal([15, 15, 0], 'W')).toBe(-15.25);
        expect(Utils.longitudeDMSToDecimal([15, 15, 9], 'W')).toBe(-15.2525);
    });

    test('Test latitudeDecimalToDMS(iValue)', () => {
        expect(Utils.latitudeDecimalToDMS(0)).toEqual({orientation: 'N', position: [0, 0, 0]});
        expect(Utils.latitudeDecimalToDMS(35)).toEqual({orientation: 'N', position: [35, 0, 0]});
        expect(Utils.latitudeDecimalToDMS(-15.25)).toEqual({orientation: 'S', position: [-15, -15, 0]});
        expect(Utils.latitudeDecimalToDMS(-15.2525)).toEqual({orientation: 'S', position: [-15, -15, -8.99999999999821]});
    });

    test('Test longitudeDecimalToDMS(iValue)', () => {
        expect(Utils.longitudeDecimalToDMS(0)).toEqual({orientation: 'E', position: [0, 0, 0]});
        expect(Utils.longitudeDecimalToDMS(35)).toEqual({orientation: 'E', position: [35, 0, 0]});        
        expect(Utils.longitudeDecimalToDMS(-15.25)).toEqual({orientation: 'W', position: [-15, -15, 0]});
        expect(Utils.longitudeDecimalToDMS(-15.2525)).toEqual({orientation: 'W', position: [-15, -15, -8.99999999999821]});
    });

    test('Test deleteFolderRecursive(iPath)', () => {
        // TODO
    });
});

