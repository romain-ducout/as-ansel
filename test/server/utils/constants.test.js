const Request = require('request');
const Constants = require('utils/constants');
const fs = require('fs');


describe('Test utils/constants', () => {
    var ansel;

    beforeAll(() => {
        ansel = require('app');
    });
    afterAll(() => {
        ansel.getServer().close();
    });

    test('Module to be loaded', () => {
        expect(Constants).toBeDefined();
    });

    test('Values to be defined', () => {
        expect(Constants.OPTIONS).toBeDefined();
        expect(Constants.ROS).toBeDefined();
        expect(Constants.PX4).toBeDefined();
        expect(Constants.ANSEL_ENV).toBeDefined();
    });
});

