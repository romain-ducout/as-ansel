const Request = require('request');

describe('Test basic behavior of application server.', () => {
    var ansel;

    beforeAll(() => {
        ansel = require('app');
    });
    afterAll(() => {
        ansel.getServer().close();
    });

    describe('Access to resources', () => {
        test('http://localhost:4200/', (done) => {
            Request.get('http://localhost:4200/', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        });
        test('http://localhost:4200/main.css', (done) => {
            Request.get('http://localhost:4200/main.css', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        });
        test('http://localhost:4200/ansel.jpg', (done) => {
            Request.get('http://localhost:4200/ansel.jpg', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        });
        test('http://localhost:4200/boson.jpg', (done) => {
            Request.get('http://localhost:4200/boson.jpg', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        });
        test('http://localhost:4200/qx30.jpg', (done) => {
            Request.get('http://localhost:4200/qx30.jpg', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        });
        test('http://localhost:4200/topsky.jpg', (done) => {
            Request.get('http://localhost:4200/topsky.jpg', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        });
        test('http://localhost:4200/favicon.png', (done) => {
            Request.get('http://localhost:4200/favicon.png', (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            })
        });
    });

    describe('Access to libraries', () => {
        describe('Cesium - 3D Map viewer', () => {
            test('http://localhost:4200/cesium/Cesium.js', (done) => {
                Request.get('http://localhost:4200/cesium/Cesium.js', (error, response, body) => {
                    expect(response.statusCode).toBe(200);
                    done();
                })
            });
        });
        describe('Plotly - Graph drawing', () => {
            test('http://localhost:4200/plotly/plotly.min.js', (done) => {
                Request.get('http://localhost:4200/plotly/plotly.min.js', (error, response, body) => {
                    expect(response.statusCode).toBe(200);
                    done();
                })
            });
        });
        describe('Material for the web - Material UI', () => {
            test('http://localhost:4200/material/material-components-web.min.css', (done) => {
                Request.get('http://localhost:4200/material/material-components-web.min.css', (error, response, body) => {
                    expect(response.statusCode).toBe(200);
                    done();
                })
            });
            test('http://localhost:4200/material/material-components-web.min.js', (done) => {
                Request.get('http://localhost:4200/material/material-components-web.min.js', (error, response, body) => {
                    expect(response.statusCode).toBe(200);
                    done();
                })
            });
        });
        describe('Socket.io - Real time communication', () => {
            test('http://localhost:4200/socket.io/socket.io.js', (done) => {
                Request.get('http://localhost:4200/socket.io/socket.io.js', (error, response, body) => {
                    expect(response.statusCode).toBe(200);
                    done();
                })
            });
        });
    });
});

