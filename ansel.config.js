module.exports = {
    "SERVER_PORT": 4200,
    "HTTPS": false,
    "IMAGES_PATH": "/media/TransferJet",
    "DATA_PATH": "/media/TransferJet/as-ansel",
    "PX4_DEV": "/dev/ttyTHS1",
    "PX4_BAUDRATE": "115200",
};
