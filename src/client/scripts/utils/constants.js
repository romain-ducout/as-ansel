/**
 * Constant values for Ansel client application.
 * 
 * @module utils/constants
 * @author Romain Ducout
 */

/**
 * Constant application object.
 * 
 * @type {Object}
 * @property {Object} DEFAULT_MAP_POSITION Default position on map.
 * @property {number} DEFAULT_MAP_POSITION.latitude Default latitude position on map.
 * @property {number} DEFAULT_MAP_POSITION.longitude Default longitude position on map.
 * @property {number} DEFAULT_MAP_POSITION.zoom Default zoom position on map.
 * 
 * @property {Array} SETTINGS_LANGUAGES List of available languages. * 
 * @property {Array} MAP_IMAGES_PROVIDERS List of image providers for maps.
 * @property {Array} ROS_NODE_LIST List of existing nodes for ROS.
 * @property {Array} ROS_TOPIC_LIST List of existing topics for ROS.
 * @property {Object} UAS_SYSTEM_NAMES Give name of drone according to identifier.
 */
const Constants = {
    DEFAULT_MAP_POSITION: {
        latitude: 35.652832,
        longitude: 139.839478,
        zoom: 9
    },

    SETTINGS_LANGUAGES: ['en', 'ja'],

    MAP_IMAGES_PROVIDERS: [
        'https://cyberjapandata.gsi.go.jp/xyz/seamlessphoto/{z}/{x}/{y}.jpg',
        'https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png'
    ],

    ROS_NODE_LIST: [
        ['exif_writer', 'listener.py'],
        ['exif_writer', 'logger.py'],
        ['mavlink_interface', 'listener'],
        ['camera_interface', 'camera_node'],
        ['futaba_interface', 'futaba_node.py']
    ],

    ROS_TOPIC_LIST: [
        ['/camera/CameraState', 'camera_interface/CameraState'],
        ['/camera/IntervalState', 'camera_interface/IntervalState'],
        ['/camera/ParamRequestList', 'camera_interface/ParamRequestList'],
        ['/camera/ParamRequestRead', 'camera_interface/ParamRequestRead'],
        ['/camera/ParamSet', 'camera_interface/ParamSet'],
        ['/camera/ParamValue', 'camera_interface/ParamValue'],
        ['/camera/TriggerCameraShutter', 'camera_interface/TriggerCameraShutter'],
        ['/camera/capture_infrared', 'camera_interface/CaptureInfrared'],
        ['/camera/picture', 'camera_interface/Picture'],
        ['/mavlink_interface/DroneAttitude', 'mavlink_interface/DroneAttitude'],
        ['/mavlink_interface/DroneGlobalPosition', 'mavlink_interface/DroneGlobalPosition'],
        ['/mavlink_interface/DroneMissionState', 'mavlink_interface/DroneMissionState'],
        ['/mavlink_interface/DroneSystemState', 'mavlink_interface/DroneSystemState'],
        ['/mavlink_interface/PictureIntervalState', 'mavlink_interface/PictureIntervalState']
    ],

    UAS_SYSTEM_NAMES: {
        1008: "VTOL (Outdoor)",
        1009: "VTOL (Indoor)",
        4030: "Dynamis (Indoor)",
        4031: "Dynamis (Outdoor)",
        4032: "MkII (Indoor)",
        4033: "MkII (Outdoor)",
        4040: "Throne (Indoor)",
        4041: "Throne (Outdoor)"
    }
};

export default Constants;
