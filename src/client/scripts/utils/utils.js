/**
 * Module of utility class for Ansel application.
 * 
 * @requires module:components/element
 * 
 * @module utils/utils
 * @author Romain Ducout
 */
import Element from 'components/element';

/**
 * Promise that Plotly library has been loaded.
 * 
 * @private
 * @type {Promise}
 */
var PLOTLY_PROMISE = null;

/**
 * Promise that Cesium library has been loaded.
 * 
 * @private
 * @type {Promise}
 */
var CESIUM_PROMISE = null;

/**
 * Utilities methods for Ansel application.
 */
class Utils{
    /**
     * Creates an instance of Utils.
     */
    constructor(){
        /**
         * Access to Ansel application root object. 
         * @private
         * @member {module:ansel~Ansel} 
         * */
        this._ANSEL_APP_ = null;
    }

    /**
     * Gets Ansel application root object.
     * 
     * @static
     * @returns {module:ansel~Ansel} Ansel application.
     */
    static getApp(){
        return this._ANSEL_APP_;
    }

    /**
     * Checks if we are on a mobile device.
     * 
     * @static
     * @returns {boolean} True if navigator is on mobile.
     */
    static isMobile(){
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }

    /**
     * Gets a promise of Cesium being loaded.
     * 
     * @static
     * @returns {Promise} Promise of Cesium being loaded.
     */
    static getCesiumPromise(){
        if(!CESIUM_PROMISE){
            CESIUM_PROMISE = new Promise(function(resolve, reject){
                Utils.loadScript('/cesium/Cesium.js', function(){
                    resolve();
                });
            });
        }

        return CESIUM_PROMISE;
    }
    
    /**
     * Gets a promise of Plotly being loaded.
     * 
     * @static
     * @returns {Promise} Promise of Plotly being loaded.
     */
    static getPlotlyPromise(){
        if(!PLOTLY_PROMISE){
            PLOTLY_PROMISE = new Promise(function(resolve, reject){
                Utils.loadScript('/plotly/plotly.min.js', function(){
                    resolve();
                });
            });
        }

        return PLOTLY_PROMISE;
    }

    /**
     * Loads a given javascript file.
     * 
     * @static
     * @param {string} iURL - URL of script.
     * @param {Function} iCallback - Method to call after file loaded.
     */
    static loadScript(iURL, iCallback)
    {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = iURL;
        
        script.onreadystatechange = iCallback;
        script.onload = iCallback;
    
        head.appendChild(script);
    }

    /**
     * Computes a date object for a given flight name.
     * Returns null if the name does not represent a date.
     * 
     * @static
     * @param {string} iFlightName - Flight name to convert.
     * @returns {Date} Date corresponding to flight name.
     */
    static computeFlightDate(iFlightName){
        var pattern = /(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})*/;
        var date = new Date(iFlightName.replace(pattern,'$1-$2-$3T$4:$5'));

        return (!isNaN(date.getTime()))?date:null;
    }

    /**
     * Download a blob object as file.
     * 
     * @static
     * @param {Blob} iBlob - Blob to download.
     * @param {string} iFilename - Name to use for downloaded file.
     */
    static downloadBlobAsFile(iBlob, iFilename){
        var tmp_link = document.createElement('a');
        document.body.appendChild(tmp_link);
        tmp_link.style = 'display: none';
        
        var url = window.URL.createObjectURL(iBlob);
        tmp_link.href = url;
        tmp_link.download = iFilename;
        tmp_link.click();
        window.URL.revokeObjectURL(url);

        document.body.removeChild(tmp_link);
    }

    /**
     * Open file chooser and return promise of file.
     * 
     * @static
     * @returns {Promise} Promise of selected.
     */
    static openFileChooser(){
        return new Promise(function(resolve, reject){
            var tmp_input = document.createElement('input');
            document.body.appendChild(tmp_input);
            tmp_input.style = 'display: none';
            tmp_input.type = 'file';
    
            tmp_input.onchange = function(iEvent){
                var files = iEvent.target.files;
                document.body.removeChild(tmp_input);
                resolve(files);
            }
            
            tmp_input.click();
        });
    }

    /**
     * Downloads a file.
     * 
     * @static
     * @param {string} iUrl - URL to download.
     */
    static downloadFile(iUrl){
        var tmp_link = document.createElement('a');
        document.body.appendChild(tmp_link);
        tmp_link.style = 'display: none';
        
        tmp_link.href = iUrl;
        tmp_link.click();
        window.URL.revokeObjectURL(iUrl);

        document.body.removeChild(tmp_link);
    }

    /**
     * Puts a DOM element on fullscreen.
     * Helper method to use fullscreen api of navigator.
     * 
     * @static
     * @param {HTMLElement} iElt - Element to display.
     */
    static setFullScreen(iElt){
        if(
            "fullscreenEnabled" in document || 
            "webkitFullscreenEnabled" in document || 
            "mozFullScreenEnabled" in document || 
            "msFullscreenEnabled" in document
        ) {
            if(
                document.fullscreenEnabled || 
                document.webkitFullscreenEnabled || 
                document.mozFullScreenEnabled || 
                document.msFullscreenEnabled
            ){  
                if("requestFullscreen" in iElt) 
                {
                    iElt.requestFullscreen();
                } 
                else if ("webkitRequestFullscreen" in iElt) 
                {
                    iElt.webkitRequestFullscreen();
                } 
                else if ("mozRequestFullScreen" in iElt) 
                {
                    iElt.mozRequestFullScreen();
                } 
                else if ("msRequestFullscreen" in iElt) 
                {
                    iElt.msRequestFullscreen();
                }

            }
        }
    }

    /**
     * Translates latitude/longitude coordinates by meters distances.
     * 
     * @static
     * @param {Object} iPoint - Point to convert {x, y}.
     * @param {Object} iOriginLatLng - Origine of translation {latitude, longitude}.
     * @returns {Object} Translated coordinates (latitude, longitude).
     */
    static convertMetersToLatLong(iPoint, iOriginLatLng){
        return {
            latitude: 360 * iPoint.y / 40008000 + iOriginLatLng.latitude,
            longitude: (360 * iPoint.x / (40075160 * Math.cos(iOriginLatLng.latitude * Math.PI/180)) + iOriginLatLng.longitude)
        };
    }
    
    /**
     * Rotates a {x,y} point by a given angle.
     * 
     * @static
     * @param {Object} iPoint - Point to rorate {x, y}.
     * @param {number} iAngle - Angle to rotate.
     * @returns {Object} Point rotated by angle (x, y).
     */
    static rotate(iPoint, iAngle){
        return {
            x: iPoint.x * Math.cos(iAngle) - iPoint.y * Math.sin(iAngle),
            y: iPoint.x * Math.sin(iAngle) + iPoint.y * Math.cos(iAngle)
        }
    }
    
    /**
     * Converts simple CSV file to JSON.
     * First raw is headers and other raws are content.
     * 
     * @static
     * @param {string} iCSV - CSV file content.
     * @returns {Object} JSON corresponding to CSV.
     */
    static convertCSVToJSON(iCSV){
        var json = {};

        var lines = iCSV.match(/[^\r\n]+/g);

        var header_line = lines[0];
        var headers = lines[0].split(',');

        for(let i = 0; i < headers.length; i++){
            json[headers[i]] = [];
        }

        for(let i = 1; i < lines.length; i++){
            let curr_line = lines[i];
            let curr_values = curr_line.split(',');

            for(let j = 0; j < curr_values.length; j++){
                json[headers[j]].push(curr_values[j]);
            }
        }
        return json;
    }
    
    /**
     * Converts degrees to radians.
     * 
     * @static
     * @param {number} iDeg - degree value to convert.
     * @returns {number} radian value for given degrees.
     */
    static DegToRad(iDeg) {
        return iDeg * Math.PI / 180;
    }

    /**
     * from http://jsperf.com/vincenty-vs-haversine-distance-calculations, all credit for code to author of that link.
     * 
     * Calculates geodetic distance between two points specified by latitude/longitude using 
     * Vincenty inverse formula for ellipsoids
     *
     * @static
     * @param {number} lat1 - First point latitude in decimal degrees
     * @param {number} lon1 - First point longitude in decimal degrees
     * @param {number} lat2 - Second point latitude in decimal degrees
     * @param {number} lon2 - Second point longitude in decimal degrees
     * @returns {number} distance in metres between points
     */
    static distVincenty(lat1, lon1, lat2, lon2) {
        var a = 6378137,
        b = 6356752.314245,
        f = 1 / 298.257223563; // WGS-84 ellipsoid params
        var L = this.DegToRad(lon2 - lon1);
        var U1 = Math.atan((1 - f) * Math.tan(this.DegToRad(lat1)));
        var U2 = Math.atan((1 - f) * Math.tan(this.DegToRad(lat2)));
        var sinU1 = Math.sin(U1),
        cosU1 = Math.cos(U1);
        var sinU2 = Math.sin(U2),
        cosU2 = Math.cos(U2);

        var lambda = L,
        lambdaP, iterLimit = 100;
        do {
            var sinLambda = Math.sin(lambda),
            cosLambda = Math.cos(lambda);
            var sinSigma = Math.sqrt((cosU2 * sinLambda) * (cosU2 * sinLambda) + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
            if (sinSigma == 0) return 0; // co-incident points
            var cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
            var sigma = Math.atan2(sinSigma, cosSigma);
            var sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
            var cosSqAlpha = 1 - sinAlpha * sinAlpha;
            var cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
            if (isNaN(cos2SigmaM)) cos2SigmaM = 0; // equatorial line: cosSqAlpha=0 (§6)
            var C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
            lambdaP = lambda;
            lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
        } while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0);

        if (iterLimit == 0) return NaN // formula failed to converge
        var uSq = cosSqAlpha * (a * a - b * b) / (b * b);
        var A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
        var B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
        var deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
        var s = b * A * (sigma - deltaSigma);

        s = parseFloat(s.toFixed(3)); // round to 1mm precision
        return s;
    }
};

export default Utils;
