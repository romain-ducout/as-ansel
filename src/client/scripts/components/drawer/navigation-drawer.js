/**
 * Module of Ansel's navigation drawer class.
 * 
 * @requires module:managers/page-manager
 * @requires module:managers/message-manager
 * @requires module:managers/request-manager
 * @requires module:utils/utils
 * @requires module:components/element
 * @requires module:models/drone
 * 
 * @module components/drawer/navigation-drawer
 * @author Romain Ducout
 */
import PageManager from 'managers/page-manager';
import MessageManager from 'managers/message-manager';
import RequestManager from 'managers/request-manager';
import Utils from 'utils/utils';
import Element from 'components/element';
import Drone from 'models/drone';

import template from './navigation-drawer.html';

/**
 * Ansel's navigation drawer class.
 * Access to different pages and different flights stored on drone.
 */
class NavigationDrawer{
    /**
     * Creates an instance of NavigationDrawer.
     * @param {module:ansel~Ansel} iApp - Ansel application.
     */
    constructor(iApp){
        /**
         * Ansel application.
         * @private
         * @member {module:ansel~Ansel} 
         * */
        this.app = iApp;
        /**
         * List of flight identifiers on drone.
         * @private
         * @member {Array.<string>} 
         * */
        this.flight_list = [];
    }
    
    /**
     * Initializes the drawer.
     */
    init(){
        this.flight_list = [];
        this.app.getRootElt().innerHTML += template;

        Drone.connect(this.onDroneChange.bind(this));
        
        this.navigation_drawer_elt = document.querySelector('#as-navigation-drawer');
        this.flight_list_container = this.navigation_drawer_elt.querySelector('.as-flight-list-container');

        this.drone_button = this.navigation_drawer_elt.querySelector('.as-drawer-actions-drone-button');   
        this.cmd_button = this.navigation_drawer_elt.querySelector('.as-drawer-actions-cmd-button'); 
        this.ros_button = this.navigation_drawer_elt.querySelector('.as-drawer-actions-ros-button'); 
        this.px4_button = this.navigation_drawer_elt.querySelector('.as-drawer-actions-px4-button'); 
        this.parameters_button = this.navigation_drawer_elt.querySelector('.as-drawer-actions-parameters-button'); 
        this.cameras_button = this.navigation_drawer_elt.querySelector('.as-drawer-actions-cameras-button'); 
        this.settings_button = this.navigation_drawer_elt.querySelector('.as-drawer-actions-settings-button');
        this.about_button = this.navigation_drawer_elt.querySelector('.as-drawer-actions-about-button');

        this.drone_button.onclick = this.onDroneButtonClick.bind(this);
        this.cmd_button.onclick = this.onCommandButtonClick.bind(this);
        this.ros_button.onclick = this.onROSButtonClick.bind(this);
        this.px4_button.onclick = this.onPX4ButtonClick.bind(this);
        this.parameters_button.onclick = this.onParametersButtonClick.bind(this);
        this.cameras_button.onclick = this.onCamerasButtonClick.bind(this);
        this.settings_button.onclick = this.onSettingsButtonClick.bind(this);
        this.about_button.onclick = this.onAboutButtonClick.bind(this);

        this.navigation_drawer = new mdc.drawer.MDCTemporaryDrawer(this.navigation_drawer_elt);
        
        this.loadFlightList()
            .then(flights => {
                this.updateFlightList(flights);
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_FLIGHT_LIST_LOADING'));
            });
    }

    /**
     * Loads the list of flights available on the drone
     * 
     * @returns {Promise} Promise of the list of flights loaded.
     */
    loadFlightList(){
        var context = this;
        return new Promise(function(resolve, reject){
            RequestManager.get('api/flights', {type: 'json'})
                .then(flights => {
                    resolve(flights);
                })
                .catch(err => {
                    reject(err);
                }
            );
        });
    }

    /**
     * Called when drone status changed.
     */
    onDroneChange(){
        this.setArmedMode(Drone.isArmed());
    }

    /**
     * Opens the drawer.
     */
    open(){
        this.navigation_drawer.open  = true;
    }

    /**
     * Closes the drawer.
     */
    close(){
        this.navigation_drawer.open  = false;
    }

    /**
     * Refreshes the navigation drawer.
     */
    refreshNavigation(){
        var bc_item = PageManager.getBreadCrumbRoot();
        var page = bc_item.page;
        var arg = bc_item.args;
        // Update UI.            
        var activated_menu_item_list = this.navigation_drawer_elt.getElementsByClassName('mdc-list-item--activated');
        for(let i = 0; i < activated_menu_item_list.length; i++){
            activated_menu_item_list[i].classList.remove('mdc-list-item--activated');                
        }

        if(page.name === 'flight'){
            var flight_menu_item_list = this.flight_list_container.getElementsByTagName('a');
            for(let i = 0; i < flight_menu_item_list.length; i++){
                let curr_flight_menu_item = flight_menu_item_list[i];
                if(curr_flight_menu_item.getAttribute('flight') === arg){
                    curr_flight_menu_item.classList.add('mdc-list-item--activated');
                    break;
                }
            }
        }
        else{
            var page_menu_item_list = this.navigation_drawer_elt.getElementsByTagName('a');
            for(let i = 0; i < page_menu_item_list.length; i++){
                if(page_menu_item_list[i].getAttribute('page') === page.name){
                    page_menu_item_list[i].classList.add('mdc-list-item--activated');
                    break;
                }
            }
        }
    }

    /**
     * Raised when user click on drone page button.
     */
    onDroneButtonClick(){
        this.close();
        this.app.navigate('drone', [], true);
    }

    /**
     * Raised when user click on command page button.
     */
    onCommandButtonClick(){
        this.close();
        this.app.navigate('command', [], true);
    }

    /**
     * Raised when user click on ROS page button.
     */
    onROSButtonClick(){
        this.close();
        this.app.navigate('ros', [], true);
    }

    /**
     * Raised when user click on PX4 page button.
     */
    onPX4ButtonClick(){
        this.close();
        this.app.navigate('px4', [], true);
    }

    /**
     * Raised when user click on Parameters page button.
     */
    onParametersButtonClick(){
        this.close();
        this.app.navigate('parameters', [], true);
    }

    /**
     * Raised when user click on cameras page button.
     */
    onCamerasButtonClick(){
        this.close();
        this.app.navigate('cameras', [], true);
    }

    /**
     * Raised when user click on settings page button.
     */
    onSettingsButtonClick(){
        this.close();
        this.app.navigate('settings', [], true);
    }

    /**
     * Raised when user click on about page button.
     */
    onAboutButtonClick(){
        this.close();
        this.app.navigate('about', [], true);
    }

    /**
     * Updates list of flights in the menu.
     * 
     * @param {Array.<string>} iFlightList - Array of flights identifiers.
     */
    updateFlightList(iFlightList){
        var context = this;
        this.flight_list = iFlightList;

        this.flight_list.sort(function(f1, f2){
            var f1_date = Utils.computeFlightDate(f1);
            var f2_date = Utils.computeFlightDate(f2);

            if(f1_date && f2_date){
                return f2_date.getTime() - f1_date.getTime();
            }

            if(f1_date){
                return -1;
            }
            if(f2_date){
                return 1;
            }

            return 0;
        });

        this.flight_list_container.innerHTML = '';

        var template = '<i class="material-icons mdc-list-item__graphic">flight</i>';

        for(let i = 0; i < this.flight_list.length; i++){
            let curr_flight = this.flight_list[i];
            let curr_flight_date = Utils.computeFlightDate(curr_flight);
            let curr_flight_name = (curr_flight_date)?curr_flight_date.toLocaleString():curr_flight;

            let currElement = new Element('a', {
                class: 'mdc-list-item',
                html: template + curr_flight_name,
                events: {
                    onclick: function(){
                        context.onFlightLinkClick(curr_flight);
                    }
                }
            }).append(this.flight_list_container);

            currElement.getElt().setAttribute('flight', curr_flight);
        }
        this.open();
    }

    /**
     * Raised when user clicks on a flight item.
     * 
     * @param {string} iFlightId - Flight identifier
     */
    onFlightLinkClick(iFlightId){
        this.close();  
        this.app.navigate('flight', iFlightId, true);
    }

    /**
     * Update menu based on drone's armed state.
     * 
     * @param {boolean} iArmed - True if drone is armed.
     */
    setArmedMode(iArmed){
        if(iArmed){
            this.flight_list_container.setAttribute('hidden', 'true');
            this.cmd_button.setAttribute('hidden', 'true');
            this.ros_button.setAttribute('hidden', 'true');
            this.px4_button.setAttribute('hidden', 'true');
        }
        else{
            this.flight_list_container.removeAttribute('hidden');
            this.cmd_button.removeAttribute('hidden');
            this.ros_button.removeAttribute('hidden');
            this.px4_button.removeAttribute('hidden');
        }
    }
};

export default NavigationDrawer;
