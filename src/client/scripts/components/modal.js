/**
 * Module of helper class to create modal panels.
 * 
 * @requires module:components/element
 * 
 * @module components/modal
 * @author Romain Ducout
 */
import Element from 'components/element';

/**
 * Helper class to create modal panels.
 */
class Modal{
    /**
     * Creates an instance of Modal.
     * @param {Object} iOptions - Options object.
     */
    constructor(iOptions){
        /**
         * Modal options. 
         * @private
         * @property {string} title Title of modal.
         * @property {string} html HTML content of modal.
         * @property {Array} actions Array of actions for the modal's footer.
         * @member {Object} 
         * */
        this.options = iOptions;
        this.init();
    }

    /**
     * Initializes the modal.
     */
    init(){
        this.dialog_etl = document.querySelector('#as-dialog');

        this.title_elt = this.dialog_etl.querySelector('.mdc-dialog__header__title');
        this.body_elt = this.dialog_etl.querySelector('.mdc-dialog__body');
        this.footer_elt = this.dialog_etl.querySelector('.mdc-dialog__footer');
    }

    /**
     * Get element object of the modal.
     * 
     * @returns {HTMLElement} Element object of the modal.
     */
    getElt(){
        return this.dialog_etl;
    }

    /**
     * Shows the modal on screen.
     * 
     * @returns {Modal} This modal.
     */
    show(){
        var context = this;
        var dialog = new mdc.dialog.MDCDialog(this.dialog_etl);
        if(!dialog){ return; }
        
        if(this.options.title){
            this.title_elt.innerHTML = this.options.title;
            this.dialog_etl.classList.remove('as-dialog-no-title');
        }
        else{
            this.dialog_etl.classList.add('as-dialog-no-title');
        }
        this.body_elt.innerHTML = this.options.html;

        this.footer_elt.innerHTML = '';
        for(let i = 0; i < this.options.actions.length; i++){
            let curr_action = this.options.actions[i];
            
            let curr_class = 'mdc-button mdc-dialog__footer__button';
            if(curr_action.accept){
                curr_class += ' mdc-dialog__footer__button--accept';
            }
            if(curr_action.cancel){
                curr_class += ' mdc-dialog__footer__button--cancel';
            }

            new Element('button', {
                class: curr_class,
                html: curr_action.title,
                events: {
                    onclick: function(){
                        dialog.destroy();
                        if(curr_action.callback){
                            curr_action.callback(context.dialog_etl, dialog);
                        }
                    }
                }
            }).append(this.footer_elt);
        }
        
        dialog.show();
        
        return this;
    }
};

export default Modal;
