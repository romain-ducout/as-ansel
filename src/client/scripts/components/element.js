/**
 * Module of a class helper to create HTML element with options.
 * 
 * @module components/element
 * @author Romain Ducout
 */

 /**
 * Class helper to create HTML element with options.
 */
class Element{
    /**
     * Creates an instance of Element.
     * @param {string} iTag - Tag name.
     * @param {Object} iConfig - Configuration object of HTML element.
     * @param {string} iConfig.id - Element's ID.
     * @param {string} iConfig.class - Element's class.
     * @param {string} iConfig.html - Element's inner HTML.
     * @param {Object.<string, string>} iConfig.attributes - Element's HTML attributes.
     * @param {Object.<string, Array.<Function>>} iConfig.events - Events with callbacks for element.
     */
    constructor(iTag, iConfig){
        /**
         * DOM Element. 
         * @private
         * @member {HTMLElement} 
         * */
        this.elt = document.createElement(iTag);

        if(iConfig.id){
            this.elt.id = iConfig.id;
        }
        if(iConfig.class){
            this.elt.setAttribute('class', iConfig.class);
        }
        if(iConfig.html){
            this.elt.innerHTML = iConfig.html;
        }
        if(iConfig.attributes){
            for(let attr_name in iConfig.attributes){
                this.elt.setAttribute(attr_name, iConfig.attributes[attr_name]);
            }
        }

        if(iConfig.events){
            for(let event in iConfig.events){
                this.elt[event] = iConfig.events[event];
            }
        }
    }

    /**
     * Gets DOM Element of object.
     * 
     * @returns {HTMLElement} DOM element.
     */
    getElt(){
        return this.elt;
    }

    /**
     * Appends the Element to a DOM object
     * 
     * @param {HTMLElement} iParent - Parent to attach the element.
     * @returns {Element} This element.
     */
    append(iParent){
        iParent.appendChild(this.elt);
        return this;
    }

    /**
     * Inserts the Element inside an object.
     * 
     * @param {HTMLElement} iParent - Parent to attach the element.
     * @param {number} iPos - Position to insert the object
     * @returns {Element} This element.
     */
    insert(iParent, iPos){
        var before_elt = iParent.children[iPos];
        if(!before_elt){
            this.append(iParent);                
        }
        else{
            iParent.insertBefore(this.elt, before_elt);
        }
        return this;
    }
};

export default Element;
