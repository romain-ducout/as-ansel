
/**
 * Module of a modal panel to manage layers on map.
 * 
 * @requires module:components/modal
 * 
 * @module components/modal/map-layer-manager-modal/map-layer-manager-modal
 * @author Romain Ducout
 */
import Modal from 'components/modal';

import template from './map-layer-manager-modal.html';

/**
 * Modal panel to manage layers on map.
 * User can show / hide layers or delete them.
 * 
 * @extends module:components/modal~Modal
 */
class MapLayerManagerModal extends Modal{
    /**
     * Creates an instance of MapLayerManagerModal.
     * @param {module:components/map/map~Map} iMap - Map calling component.
     */
    constructor(iMap){
        super({
            title: null,
            html: template,
            actions:[]
        });

        this.options.actions = [
            {
                title: __('CANCEL')
            },
            {
                accept: true,
                title: __('DELETE'),
                callback: this.onDeleteButton.bind(this)
            }
        ];
        /**
         * Map component managed by the panel.
         * @private
         * @member {module:components/map/map~Map} 
         * */
        this.map = iMap;

        /**
         * List of selected layers' identifiers.
         * @private
         * @member {Array} 
         */
        this.selected_layer_list = [];
    }

    /**
     * Shows the modal.
     * 
     * @override
     */
    show(){
        super.show();        
        this.container = document.querySelector('.as-map-layer-manager-container');
        this.layer_list_container = document.querySelector('.mdc-list');
        
        this.initLayerList();
    }

    /**
     * Initializes the list of layers component.
     */
    initLayerList(){
        if(!this.map){ return; }
        var context = this;
        var layers = this.map.getLayersIdList();

        var layer_list_template = ``;
        for(let i = 0; i < layers.length; i++){
            let curr_layer_id = layers[i];
            layer_list_template += `
                <li class="mdc-list-item">
                    <label for="trailing-checkbox-blueberries">${curr_layer_id}</label>
                    <span class="mdc-list-item__meta">
                        <div class="mdc-checkbox" layerid="${curr_layer_id}">
                            <input type="checkbox" class="mdc-checkbox__native-control"/>
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </span>
                </li>
            `;
        }

        this.layer_list_container.innerHTML = layer_list_template;

        var checkboxes = this.container.querySelectorAll('.mdc-checkbox');
        for(let i = 0; i < checkboxes.length; i++){
            let curr_checkbox = checkboxes[i];
            let curr_checkbox_layer_id = curr_checkbox.getAttribute('layerid');
            mdc.checkbox.MDCCheckbox.attachTo(curr_checkbox);

            curr_checkbox.addEventListener('click', function(evt) {
                var ind = context.selected_layer_list.indexOf(curr_checkbox_layer_id);
                if(ind >= 0){
                    context.selected_layer_list.splice(ind, 1);
                }
                else{
                    context.selected_layer_list.push(curr_checkbox_layer_id.trim());
                }
            });
        };
    }

    /**
     * Method called when user validates the modal form.
     * Deletes all selected layers from the map.
     */
    onDeleteButton(){
        if(!this.map) { return; }
        for(let i = 0; i < this.selected_layer_list.length; i++){
            this.map.deleteLayer(this.selected_layer_list[i]);
        }
    }
};

export default MapLayerManagerModal;