
/**
 * Module of a modal panel to select logs on PX4 to deploy or delete.
 * 
 * @requires module:components/modal
 * 
 * @module components/modal/px4-deploy-log-modal/px4-deploy-log-modal
 * @author Romain Ducout
 */
import Modal from 'components/modal';

import template from './px4-deploy-log-modal.html';

/**
 * Modal panel to select logs on PX4 to deploy or delete.
 * 
 * @extends module:components/modal~Modal
 */
class PX4DeployLogModal extends Modal{
    /**
     * Creates an instance of PX4DeployLogModal.
     * @param {module:pages/px4/px4-page} iParent - Parent page opening the panel.
     * @param {Array} iLogList - List of log identifiers on PX4.
     */
    constructor(iParent, iLogList){
        super({
            title: null,
            html: template,
            actions:[]
        });

        this.options.actions = [
            {
                title: __('CANCEL')
            },
            {
                title: __('DELETE'),
                callback: this.onDeleteButton.bind(this)
            },
            {
                accept: true,
                title: __('DEPLOY'),
                callback: this.onDeployButton.bind(this)
            }
        ];

        /**
         * Parent page opening the panel.
         * @private
         * @member {module:pages/px4/px4-page} 
         * */
        this.parent = iParent;
        
        /**
         * List flight logs' identifiers to manage.
         * @private
         * @member {Array} 
         */
        this.flight_list = iLogList;
        
        /**
         * List of selected flight logs' identifiers.
         * @private
         * @member {Array} 
         */
        this.selected_flight_list = [];

        this.flight_list.sort(function(l1, l2){
            var d1 = new Date(l1.date).getTime();
            var d2 = new Date(l2.date).getTime();
            return (d2 - d1);
        });        
    }

    /**
     * Shows the modal.
     * 
     * @override
     */
    show(){
        super.show();        
        this.container = document.querySelector('.as-px4-deploy-log-container');
        this.flight_list_container = document.querySelector('.mdc-list');
        
        this.initFlightList();
    }

    /**
     * Initializes the list of flights' logs component.
     */
    initFlightList(){
        if(!this.flight_list){ return; }
        var context = this;;

        var log_list_elem_template = '';
        for(let i = 0; i < this.flight_list.length; i++){
            let curr_log = this.flight_list[i];
            log_list_elem_template += `
                <li class="mdc-list-item checkbox-list-ripple-surface ${curr_log.deployed?'disabled':''}">
                    <label for="trailing-checkbox-blueberries">${curr_log.name}</label>
                    <span class="mdc-list-item__meta">
                        <div class="mdc-checkbox" logid="${curr_log.id}">
                            <input type="checkbox" class="mdc-checkbox__native-control"/>
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </span>
                </li>
            `;
        }

        this.flight_list_container.innerHTML = log_list_elem_template;
    
        var checkboxes = this.container.querySelectorAll('.mdc-checkbox');
        for(let i = 0; i < checkboxes.length; i++){
            let curr_checkbox = checkboxes[i];
            let curr_checkbox_log_id = curr_checkbox.getAttribute('logid');
            mdc.checkbox.MDCCheckbox.attachTo(curr_checkbox);

            curr_checkbox.addEventListener('click', function(evt) {
                var ind = context.selected_flight_list.indexOf(curr_checkbox_log_id);
                if(ind >= 0){
                    context.selected_flight_list.splice(ind, 1);
                }
                else{
                    context.selected_flight_list.push(curr_checkbox_log_id.trim());
                }
            });
        };
    }

    /**
     * Method called when user validates the modal form with delete.
     * Deletes all selected logs from PX4.
     */
    onDeleteButton(){
        var context = this;
        if(this.selected_flight_list.length === 0){ return; }

        new Modal({
            title: __('MSG_WARNING_PX4_LOGS_DELETE_TITLE'),
            html: __('MSG_WARNING_PX4_LOGS_DELETE'),
            actions: [
                {
                    title: __('CANCEL')
                },
                {
                    accept: true,
                    title: __('VALIDATE'),
                    callback: function(){ 
                        context.parent.deleteLogList(context.selected_flight_list);                                            
                    }
                }
            ]
        }).show();
    }
    /**
     * Method called when user validates the modal form with deploy.
     * deploys all selected logs from PX4.
     */
    onDeployButton(){
        if(this.selected_flight_list.length === 0){ return; }
        this.parent.deleteLogList(this.selected_flight_list);
        
    }
};

export default PX4DeployLogModal;