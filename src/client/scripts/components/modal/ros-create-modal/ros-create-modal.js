
/**
 * Module of a modal panel to create ROS elements (topics, services or nodes).
 * 
 * @requires module:components/modal
 * @requires module:components/element
 * @requires module:utils/constants
 * @requires module:utils/utils
 * 
 * @module components/modal/ros-create-modal/ros-create-modal
 * @author Romain Ducout
 */
import Modal from 'components/modal';
import Element from 'components/element';
import Constants from 'utils/constants';
import Utils from 'utils/utils';

import template from './ros-create-modal.html';

/**
 * Modal panel to create ROS elements (topics, services or nodes).
 * 
 * @extends module:components/modal~Modal
 */
class RosCreateModal extends Modal{
    /**
     * Creates an instance of RosCreateModal.
     * @param {module:pages/ros/ros-page} iParent - Parent page opening the panel.
     */
    constructor(iParent){
        super({
            title: null,
            html: template,
            actions:[]
        });

        var context = this;
        this.options.actions = [
            {
                title: __('CLOSE')
            }
        ];

        /**
         * Currently tab name.
         * @private
         * @member {string} 
         * */
        this.current_panel = '';

        /**
         * Parent page opening the panel.
         * @private
         * @member {module:pages/ros/ros-page} 
         * */
        this.parent = iParent;
    }

    /**
     * Shows the modal.
     * 
     * @override
     */
    show(){
        super.show();
        this.container = document.querySelector('.as-ros-create-panel-container');

        new mdc.tabs.MDCTabBarScroller(this.container.querySelector('.mdc-tab-bar-scroller'));

        this.node_button = this.container.querySelector('.as-node-button');
        this.topic_button = this.container.querySelector('.as-topic-button');
        this.service_button = this.container.querySelector('.as-service-button');

        this.node_container = this.container.querySelector('.as-create-node-container');
        this.topic_container = this.container.querySelector('.as-create-topic-container');
        this.service_container = this.container.querySelector('.as-create-service-container');
        
        this.node_button.onclick = this.onNodeButtonClick.bind(this);
        this.topic_button.onclick = this.onTopicButtonClick.bind(this);
        this.service_button.onclick = this.onServiceButtonClick.bind(this);

        this.initNodeCreatePanel();
        this.initTopicCreatePanel();

        this.onNodeButtonClick();
    }

    /**
     * Initializes the node tab.
     */
    initNodeCreatePanel(){
        var context = this;
        var node_select_items_container = this.node_container.querySelector('.mdc-select__native-control');           

        for(let i = 0; i < Constants.ROS_NODE_LIST.length; i++){
            new Element('option', {
                html: Constants.ROS_NODE_LIST[i][0] + ' - ' + Constants.ROS_NODE_LIST[i][1],
                events: {
                    onclick: function(){
                    }
                }
            }).append(node_select_items_container);
        }
        this.node_select = new mdc.select.MDCSelect(this.node_container.querySelector('.mdc-select'));
        this.node_package_text = new mdc.textField.MDCTextField(this.node_container.querySelector('.as-node-create-package-input'));
        this.node_type_text = new mdc.textField.MDCTextField(this.node_container.querySelector('.as-node-create-type-input'));
        this.node_args_text = new mdc.textField.MDCTextField(this.node_container.querySelector('.as-node-create-args-input'));

        this.node_select.listen('MDCSelect:change', () => {
            context.node_package_text.value = Constants.ROS_NODE_LIST[context.node_select.selectedIndex][0];
            context.node_type_text.value = Constants.ROS_NODE_LIST[context.node_select.selectedIndex][1];                
        });
    }

    /**
     * Initializes the topics tab.
     */
    initTopicCreatePanel(){
        var context = this;
        var topic_select_items_container = this.topic_container.querySelector('.mdc-select__native-control');           

        for(let i = 0; i < Constants.ROS_TOPIC_LIST.length; i++){
            new Element('option', {
                html: Constants.ROS_TOPIC_LIST[i][0] + ' - ' + Constants.ROS_TOPIC_LIST[i][1],
                events: {
                    onclick: function(){
                    }
                }
            }).append(topic_select_items_container);
        }
        this.topic_select = new mdc.select.MDCSelect(this.topic_container.querySelector('.mdc-select'));
        this.topic_name_text = new mdc.textField.MDCTextField(this.topic_container.querySelector('.as-topic-create-name-input'));
        this.topic_type_text = new mdc.textField.MDCTextField(this.topic_container.querySelector('.as-topic-create-type-input'));
        this.topic_args_text = new mdc.textField.MDCTextField(this.topic_container.querySelector('.as-topic-create-args-input'));

        this.topic_select.listen('MDCSelect:change', () => {
            context.topic_name_text.value = Constants.ROS_TOPIC_LIST[context.topic_select.selectedIndex][0];
            context.topic_type_text.value = Constants.ROS_TOPIC_LIST[context.topic_select.selectedIndex][1];                
        });
    }

    /**
     * Switches to node tab on node button click.
     */
    onNodeButtonClick(){
        this.current_panel = 'node';
        this.node_container.removeAttribute('hidden');
        this.topic_container.setAttribute('hidden', true);
        this.service_container.setAttribute('hidden', true);
    }

    /**
     * Switches to topic tab on topic button click.
     */
    onTopicButtonClick(){
        this.current_panel = 'topic';
        this.node_container.setAttribute('hidden', true);
        this.topic_container.removeAttribute('hidden');
        this.service_container.setAttribute('hidden', true);
    }

    /**
     * Switches to service tab on service button click.
     */
    onServiceButtonClick(){
        this.current_panel = 'service';
        this.node_container.setAttribute('hidden', true);
        this.topic_container.setAttribute('hidden', true);
        this.service_container.removeAttribute('hidden');
    }

    /**
     * Called when create button is clicked.
     * Will crete the object of the currently displayed tab.
     */
    onCreate(){
        switch (this.current_panel) {
            case 'node':
                this.onCreateNode();
                break;
            case 'topic':
                this.onCreateTopic();
                break;
            case 'service':
                this.onCreateService();
                break;            
            default:
                break;
        }
    }

    /**
     * Gets parameters from panel to create a node.
     */
    onCreateNode(){
        var pkg = this.node_package_text.value;
        var type = this.node_type_text.value;    
        var args = this.node_args_text.value; 
        
        if(!pkg || pkg.length === 0){ return; }
        if(!type || type.length === 0){ return; }

        this.parent.createNode(pkg, type, args);
    }

    /**
     * Gets parameters from panel to create a topic.
     */
    onCreateTopic(){
        var name = this.topic_name_text.value;
        var type = this.topic_type_text.value;    
        var args = this.topic_args_text.value; 
        
        if(!name || name.length === 0){ return; }
        if(!type || type.length === 0){ return; }

        this.parent.createTopic(name, type, args);
    }

    /**
     * Gets parameters from panel to create a service.
     */
    onCreateService(){ 
        this.parent.createService(name, type, args);
    }
};

export default RosCreateModal;