/**
 * Module of a map view component to display a 3D / 2D map using Cesium.
 * 
 * @requires module:utils/constants
 * @requires module:utils/utils
 * @requires module:models/drone
 * @requires module:managers/event-manager
 * @requires module:components/modal/map-layer-manager-modal/map-layer-manager-modal
 * 
 * @module components/map/map
 * @author Romain Ducout
 */
import Constants from 'utils/constants';
import Utils from 'utils/utils';
import Drone from 'models/drone';
import EventManager from 'managers/event-manager';
import MapLayerManagerModal from 'components/modal/map-layer-manager-modal/map-layer-manager-modal';

import camera_image from 'assets/imgs/camera.svg';
import gps_marker_image from 'assets/imgs/gps_marker.png';
import drone_model from 'assets/models/drone_throne_colored.glb';

import template from './map.html';

/**
 * Map view component to display a 3D / 2D map using Cesium.
 * This map can shows a drone position as well as the content of a flight (images and markers).
 */
class Map {
    /**
     * Creates an instance of Map.
     * @param {HTMLElement} iContainer - DOM element where to insert the map.
     * @param {Object} iOptions - Options object.
     * @param {number} iOptions.mode Display mode (0: 2D, 1: 3D).
     * @param {Object} iOptions.events Map events.
     * @param {boolean} iOptions.view.drone True to display the drone.
     * @param {Object} iOptions.view.flight Flight view options.
     * @param {boolean} iOptions.view.flight.markers True to display GPS markers.
     * @param {boolean} iOptions.view.flight.altitude True to place objects according to altitude (false to project on ground).
     * @param {number} iOptions.view.flight.view_type Image view type to display.
     * @param {number} iOptions.view.flight.image_type Image type to display.
     */
    constructor(iContainer, iOptions){
        /**
         * DOM element where the map is inserted.
         * @private
         * @member {HTMLElement} 
         * */
        this.container = iContainer;
        /**
         * Map options.
         * @private
         * @member {Object} 
         * */
        this.options = iOptions;

        if(!this.options){
            this.options = {};
        }
        if(!this.options.view){
            this.options.view = {};
        }

        /**
         * Promise of map being loaded.
         * @private
         * @member {Promise} 
         * */
        this.load_promise = null;

        /**
         * Cesium map viewer.
         * @private
         * @member {Object} 
         * */
        this.viewer = null;

        /**
         * Flight to be displayed.
         * @private
         * @member {module:models/flight~Flight} 
         * */
        this.flight = null;
        
        /**
         * Event manager of the map.
         * @private
         * @member {module:managers/event-manager~EventManager} 
         * */
        this.event_manager = new EventManager(this.options.events);

        /**
         * List of Cesium entities for flight images.
         * @private
         * @member {Array} 
         * */
        this.images_entities = [];
        /**
         * List of Cesium entities for flight markers.
         * @private
         * @member {Array} 
         * */
        this.markers_entities = [];
        /**
         * List of Promise of loaded KML overlay.
         * @private
         * @member {Array} 
         * */
        this.kml_promises = [];
        /**
         * Cesium entity to represent the drone.
         * @private
         * @member {Object} 
         * */
        this.drone_entity = null;

        /**
         * Entity hovered by mouse by the user.
         * @private
         * @member {Object} 
         * */
        this.prev_hover_entity = null;
    }

    /**
     * Loads map resources before being shown.
     * 
     * @returns {Promise} loading promise for the map.
     */
    load(){
        var context = this;

        if(!this.load_promise){
            this.load_promise = new Promise(function(resolve, reject){
                Utils.getCesiumPromise()
                    .then(function(){
                        context.init();
                        resolve();
                    });
            });
        }

        return this.load_promise;
    }

    /**
     * Initializes the component.
     */
    init(){
        this.container.innerHTML = template;

        this.inner_container = this.container.querySelector('.as-map-inner-container');
        
        this.drone_zoom_button = this.container.querySelector('.as-map-drone-zoom-button');
        this.flight_zoom_button = this.container.querySelector('.as-map-flight-zoom-button');
        this.layers_button = this.container.querySelector('.as-map-layers-button');

        this.drone_zoom_button.onclick = this.onDroneZoomButtonClick.bind(this);
        this.flight_zoom_button.onclick = this.onFlightZoomButtonClick.bind(this);
        this.layers_button.onclick = this.onLayersButtonClick.bind(this);

        this.initViewer();
    }

    /**
     * Initializes the map viewer.
     */
    initViewer(){
        var onLineProvider = new Cesium.UrlTemplateImageryProvider({
            url : Constants.MAP_IMAGES_PROVIDERS[0]
        });
        
        this.viewer = new Cesium.Viewer(this.inner_container, {
            sceneMode: this.options.mode,
            imageryProvider: onLineProvider,
            requestRenderMode: true,
            animation: false,
            baseLayerPicker: false,
            fullscreenButton: false,
            vrButton: false,
            geocoder: false,
            homeButton: false,
            infoBox: false,
            sceneModePicker: false,
            selectionIndicator: false,
            timeline: false,
            navigationHelpButton: false,
            navigationInstructionsInitiallyVisible: false
        });

        var layers = this.viewer.scene.imageryLayers;
        layers.addImageryProvider(onLineProvider);

        var handler = new Cesium.ScreenSpaceEventHandler(this.getCanvas());

        if(Utils.isMobile()){
            handler.setInputAction(this.onViewerLeftClick.bind(this), Cesium.ScreenSpaceEventType.LEFT_CLICK);
        }
        else{
            handler.setInputAction(this.onViewerMouseMove.bind(this), Cesium.ScreenSpaceEventType.MOUSE_MOVE);
            handler.setInputAction(this.onViewerLeftClick.bind(this), Cesium.ScreenSpaceEventType.LEFT_CLICK);
        }

        this.popup_entity = this.viewer.entities.add({
            billboard : {
                width : 150,
                horizontalOrigin: Cesium.HorizontalOrigin.CENTER,
                verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
                pixelOffset : new Cesium.Cartesian2(0, -25),
                show: false
            }
        });

        this.ground_image_entity = this.viewer.entities.add({
            polygon : {
                material : Cesium.Color.BLUE.withAlpha(0.3),
                show: false
            }
        });
    }

    /**
     * Called when user clicks on drone zoom button.
     * Centers the map on the drone.
     */
    onDroneZoomButtonClick(){
        this.goToDrone();
    }
    /**
     * Called when user clicks on flight zoom button.
     * Centers the map on the flight.
     */
    onFlightZoomButtonClick(){
        this.goToFlight();
    }
    /**
     * Called when user clicks on layers button.
     * Open layers menu.
     */
    onLayersButtonClick(){
        var panel = new MapLayerManagerModal(this);
        panel.show();
    }

    /**
     * Shows the map.
     */
    show(){
        this.container.removeAttribute('hidden');
    }
    
    /**
     * Hides the map.
     */
    hide(){
        this.container.setAttribute('hidden', 'true');
        this.clearPopup();
    }

    /**
     * Gets the scene of the map.
     * 
     * @returns {Object} Scene of the map.
     */
    getScene(){
        if(!this.viewer){
            return null;
        }
        return this.viewer.scene;
    }

    /**
     * Gets the canvas of the map.
     * 
     * @returns {Object} Canvas of the map.
     */
    getCanvas(){
        if(!this.viewer){
            return null;
        }
        return this.viewer.scene.canvas;
    }

    /**
     * Hides the popup billboard.
     */
    clearPopup(){        
        this.getCanvas().style.cursor = '';
        this.prev_hover_entity = null;
        this.popup_entity.billboard.show = false;
        this.ground_image_entity.polygon.show = false;
    }

    /**
     * Creates a new layer from imput file.
     * 
     * @param {Resource | String | Document | Blob} iFile - A url, parsed KML document, or Blob containing binary KMZ data or a parsed KML document.
     */
    importLayer(iFile, iId){
        if(!iFile){ return; }
        if(!iId || iId.length === 0){ return; }
        if(!this.viewer){ return; }
        if(this.kml_promises[iId]){ return; }

        var context = this;

        var kml_promise = Cesium.KmlDataSource.load(iFile, {
            camera: this.viewer.camera,
            canvas: this.viewer.canvas
        });

        kml_promise.then(function(dataSource) {
            context.viewer.flyTo(dataSource.entities);
        });
        this.viewer.dataSources.add(kml_promise);

        this.kml_promises[iId] = kml_promise;

        this.layers_button.removeAttribute('hidden');
    }

    /**
     * Deletes a layer.
     * 
     * @param {String} iLayerId - Identifier of layer to delete.
     */
    deleteLayer(iLayerId){
        if(!this.viewer){ return; }
        if(!this.kml_promises[iLayerId]){ return; }

        var context = this;
        
        this.kml_promises[iLayerId].then(function(dataSource){
            context.viewer.dataSources.remove(dataSource, true);
        });
        delete this.kml_promises[iLayerId];

        if(this.getLayersIdList().length === 0){
            this.layers_button.setAttribute('hidden', 'true');
        }
    }

    /**
     * Returns an array of the imported layers' identifiers.
     * 
     * @returns {Array} List of layers' identifiers.
     */
    getLayersIdList(){
        var res = [];
        for(var id in this.kml_promises){
            res.push(id);
        }
        return res;
    }

    /**
     * Manager mouse move events.
     * Displays image and ground zone if mouse hover an image.
     * 
     * @param {Object} iEvent - Mouse move event.
     */
    onViewerMouseMove(iEvent){
        var picked_object = this.getScene().pick(iEvent.endPosition);
        if(!picked_object || !picked_object.id || !picked_object.id.properties){ 
            this.clearPopup();
            return; 
        }

        var properties = picked_object.id.properties.getValue();
        if(!properties || !properties.image){ 
            this.clearPopup();
            return; 
        }
        
        if(this.prev_hover_entity === picked_object.id){
            return;
        }
        
        var image = properties.image;
        this.getCanvas().style.cursor = 'pointer';

        if(this.getViewType() === 0){
            var dist = Cesium.Cartesian3.distance(this.viewer.camera.position, picked_object.id.position.getValue());
            let thumbmailURL = image.getImageUrl(150);
            
            this.popup_entity.position = picked_object.id.position;
            this.popup_entity.billboard.image = thumbmailURL;
            this.popup_entity.billboard.show = true;

            if(this.getMode() === Cesium.SceneMode.SCENE3D){
                this.popup_entity.billboard.eyeOffset = new Cesium.Cartesian3(0.0, 0.0, -dist * 0.5);
            }
            else{
                this.popup_entity.billboard.eyeOffset = new Cesium.Cartesian3(0.0, 0.0, -50);
            }
        }

        var coords = image.getBoundingCoordinates();
        this.ground_image_entity.polygon.show = true;
        this.ground_image_entity.polygon.hierarchy = Cesium.Cartesian3.fromDegreesArray([
            coords.tl.longitude, coords.tl.latitude,
            coords.tr.longitude, coords.tr.latitude,
            coords.br.longitude, coords.br.latitude,
            coords.bl.longitude, coords.bl.latitude
        ]);

        this.prev_hover_entity = picked_object.id;
    }

    /**
     * Manager mouse click events.
     * Raises event if clicks on an image.
     * 
     * @param {Object} iEvent - Mouse click event.
     */
    onViewerLeftClick(iEvent){
        var picked_object = this.getScene().pick(iEvent.position);
        if(!Cesium.defined(picked_object)){ return; }

        var properties = picked_object.id.properties.getValue();
        if(!properties || !properties.image){ return; }

        this.event_manager.raise('onImageClick', [properties.image]); 
    }

    /**
     * Centers view on the current flight.
     */
    goToFlight(){
        if(!this.flight) { return; }
        this.viewer.zoomTo(this.images_entities);
    }

    /**
     * Centers view on the drone on map.
     */
    goToDrone() {
        this.viewer.zoomTo(this.drone_entity, new Cesium.HeadingPitchRange(0, -Math.PI*0.25, 200));
    };

    /**
     * Removes the flight elements from the map.
     */
    clearFlight(){
        for(var i = 0; i < this.images_entities.length; i++){
            this.viewer.entities.remove(this.images_entities[i]);
        }
        this.images_entities = [];

        for(var i = 0; i < this.markers_entities.length; i++){
            this.viewer.entities.remove(this.markers_entities[i]);
        }
        this.markers_entities = [];
    }

    /**
     * Retrieves map view options.
     * 
     * @returns {Object} Map view options.
     */
    getViewOptions(){
        return this.options.view;
    }

    /**
     * Applies map view options.
     * 
     * @param {Object} iOptions - Map options to set.
     */
    setViewOptions(iOptions){
        this.options.view = iOptions;
    }

    /**
     * Sets current flight to display.
     * 
     * @param {Flight} iFlight - Flight to display
     */
    setFlight(iFlight){
        this.flight = iFlight;
    }

    /**
     * Checks if the drone is enabled on map.
     * 
     * @returns {boolean} True if drone enabled on map.
     */
    getDroneEnabled(){
        if(!this.options){ return false; }
        if(!this.options.view){ return false; }
        return this.options.view.drone;
    }

    /**
     * Gets image type to display.
     * 
     * @returns {number} Image type to display.
     */
    getImageType(){
        if(!this.options){ return 0; }
        if(!this.options.view){ return 0; }
        if(!this.options.view.flight){ return 0; }
        return this.options.view.flight.image_type;
    }

    /**
     * Checks if altitude is enabled on map.
     * If altitude is enabled, markers are positionned relative to their altitude.
     * If altitude is not enabled, markers are projected on ground.
     * 
     * @returns {boolean} True if altitude is enabled.
     */
    getAltitudeEnabled(){
        if(!this.options){ return false; }
        if(!this.options.view){ return false; }
        if(!this.options.view.flight){ return false; }
        return this.options.view.flight.altitude;
    }

    /**
     * Checks if markers are enabled on map.
     * 
     * @returns {boolean} True if markers are enabled.
     */
    getMarkersEnabled(){
        if(!this.options){ return false; }
        if(!this.options.view){ return false; }
        if(!this.options.view.flight){ return false; }
        return this.options.view.flight.markers;
    }

    /**
     * Gets view type used to display images.
     * 
     * @returns {number} View type.
     */
    getViewType(){
        if(!this.options){ return 0; }
        if(!this.options.view){ return 0; }
        if(!this.options.view.flight){ return 0; }
        return this.options.view.flight.view_type;
    }

    /**
     * Gets view mode 2D / 3D.
     * 
     * @returns {number} View mode 2D / 3D of map.
     */
    getMode(){
        if(!this.options){ return false; }
        return this.options.mode;
    }

    /**
     * Sets view mode 2D / 3D.
     * 
     * @param {number} iMode - New view mode.
     */
    setMode(iMode){
        if(!this.options){ return; }
        if(!this.options.mode == iMode){ return; }

        this.options.mode = iMode;

        if(this.getScene()){
            if(this.getMode() === Cesium.SceneMode.SCENE3D){
                this.getScene().morphTo3D(0);
            }
            else{
                this.getScene().morphTo2D(0);
            }
        }
    }

    /**
     * Toggle 2D / 3D mode.
     * 
     */
    toggleMode(){
        if(this.getMode() === Cesium.SceneMode.SCENE3D){
            this.setMode(Cesium.SceneMode.SCENE2D);
        }
        else{
            this.setMode(Cesium.SceneMode.SCENE3D);
        }
    }


    /**
     * Refreshes map elements: drone and flight.
     */
    refresh(){
        var context = this;

        this.clearFlight();

        this.refreshButtons();
        
        if(this.flight){
            this.refreshImages();
            this.refreshMarkers();
        }
        if(this.getDroneEnabled()){
            this.refreshDrone();
        }
    }

    /**
     * Refreshes the action buttons.
     */
    refreshButtons(){
        if(!this.drone_zoom_button) { return; }
        if(!this.flight_zoom_button) { return; }
        this.drone_zoom_button.setAttribute('hidden', 'true');
        this.flight_zoom_button.setAttribute('hidden', 'true');
        
        if(this.flight){
            this.flight_zoom_button.removeAttribute('hidden');
        }
        if(this.getDroneEnabled()){
            this.drone_zoom_button.removeAttribute('hidden');
        }
    }

    /**
     * Refreshes the images of the current flight.
     */
    refreshImages(){
        if(!this.viewer){ return; }
        if(!this.flight){ return; }
        var images = this.flight.getImages(this.getImageType());
        if(!images){ return; }

        for(let i = 0; i < images.length; i++){
            let curr_image = images[i];
            if(!curr_image){ continue; }

            if(this.getViewType() === 0){
                let currBillboard = this.viewer.entities.add({
                    position: Cesium.Cartesian3.fromDegrees(curr_image.gps.longitude, curr_image.gps.latitude, this.getAltitudeEnabled()?curr_image.gps.altitude:0),
                    properties: {
                        image: curr_image
                    },
                    billboard : {   
                        image : camera_image,                        
                        width : 25,
                        height : 25,
                        horizontalOrigin: Cesium.HorizontalOrigin.CENTER,
                        verticalOrigin: Cesium.VerticalOrigin.BOTTOM
                    }
                });
                this.images_entities.push(currBillboard);
            }
            else {
                let thumbmailURL = curr_image.getImageUrl(75);

                let currBillboard = this.viewer.entities.add({
                    position: Cesium.Cartesian3.fromDegrees(curr_image.gps.longitude, curr_image.gps.latitude, this.getAltitudeEnabled()?curr_image.gps.altitude:0),
                    properties: {
                        image: curr_image
                    },
                    billboard : {
                        image : thumbmailURL,
                        width : 75,
                        horizontalOrigin: Cesium.HorizontalOrigin.CENTER,
                        verticalOrigin: Cesium.VerticalOrigin.BOTTOM
                    }
                });
                
                this.images_entities.push(currBillboard);
            }
        }
    }

    /**
     * Refreshes the markers of the current flight.
     */
    refreshMarkers(){
        if(!this.viewer){ return; }
        if(!this.flight){ return; }
        if(!this.getMarkersEnabled()){ return; }
        var images = this.flight.getImages(this.getImageType());
        if(!images){ return; }
        var image_markers = this.flight.getMarkers();
        if(!image_markers){ return; }

        var marker_max_dist = 5;

        // step 1: compute coordinates for each markers.
        var markers_coords = [];
        for(let i = 0; i < images.length; i++){
            let curr_image = images[i];
            if(!curr_image){ continue; }
            let curr_image_markers = image_markers[curr_image.id];
            if(!curr_image_markers){ continue; }

            for(let j = 0; j < curr_image_markers.length; j++){
                let curr_image_marker = curr_image_markers[j];
                if(!curr_image_marker){ continue; }

                markers_coords.push(curr_image.getCoordinates(curr_image_marker.x, curr_image_marker.y));
            }
        }

        // step 2: place each markers in clusters.
        var clusters = [];
        while(markers_coords.length > 0){
            let curr_coords = markers_coords.pop();

            let curr_found_cluster = false;
            for(let i = 0; i < clusters.length; i++){
                let curr_cluster = clusters[i];
                for(let j = 0; j < curr_cluster.length; j++){
                    let curr_cluster_coords = curr_cluster[j];

                    let curr_dist = Utils.distVincenty(
                        curr_coords.latitude, curr_coords.longitude,
                        curr_cluster_coords.latitude, curr_cluster_coords.longitude
                    );

                    if(curr_dist < marker_max_dist){
                        curr_found_cluster = true;
                        curr_cluster.push(curr_coords);
                        break;
                    }
                }
            }

            if(!curr_found_cluster){
                clusters.push([curr_coords]);
            }
        }

        //step 3: compute estimated position for each clusters.
        var markers = [];
        for(let i = 0; i < clusters.length; i++){
            let curr_cluster = clusters[i];
            let curr_sum_latitude = 0;
            let curr_sum_longitude = 0;
            for(let j = 0; j < curr_cluster.length; j++){
                curr_sum_latitude += curr_cluster[j].latitude;
                curr_sum_longitude += curr_cluster[j].longitude;                    
            }
            markers.push({
                latitude: curr_sum_latitude / curr_cluster.length,
                longitude: curr_sum_longitude / curr_cluster.length
            })
        }
        
        //step 3: draw each estimated marker positions. 
        for(let i = 0; i < markers.length; i++){
            let curr_marker = markers[i];
            if(!curr_marker){ continue; }

            let currBillboard = this.viewer.entities.add({
                position: Cesium.Cartesian3.fromDegrees(curr_marker.longitude, curr_marker.latitude, 0),
                billboard : {   
                    image : gps_marker_image,                            
                    width : 20,
                    height : 25,
                    horizontalOrigin: Cesium.HorizontalOrigin.CENTER,
                    verticalOrigin: Cesium.VerticalOrigin.BOTTOM
                }
            });
            this.markers_entities.push(currBillboard);
        }
    }

    /**
     * Refreshes the drone on the map.
     */
    refreshDrone() {
        if(!this.viewer){ return; }
        if(!Drone){ return; }
        if(!Drone.data){ return; }
        if(!Drone.data.gps){ return; }

        if(!this.drone_entity) {
            this.setupDrone();
        }
        
        var position = Cesium.Cartesian3.fromDegrees(Drone.data.gps.longitude, Drone.data.gps.latitude, Drone.data.gps.altitude);
        var heading = Cesium.Math.toRadians(Drone.data.gps.direction);
        
        var pitch = 0;
        var roll = 0;

        if(Drone.data.attitude){
            pitch = Drone.data.attitude.pitch;
            roll = Drone.data.attitude.roll;
            heading = Drone.data.attitude.yaw - Math.PI * 0.5;
        }

        var hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
        var orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);

        this.drone_entity.position = position;
        this.drone_entity.orientation = orientation;
    }

    /**
     * Initializes drone asset.
     */
    setupDrone() {
        if(!this.viewer){ return; }
        this.drone_entity = this.viewer.entities.add({
            name : 'drone',
            position : Cesium.Cartesian3.fromDegrees(0, 0, 0),
            model : {
                uri : drone_model,
                scale: 0.005,
                minimumPixelSize : 100
            }
        });
    }
};

export default Map;

