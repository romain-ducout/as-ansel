/**
 * Module of Ansel's PX4 page.
 * 
 * @requires module:pages/page
 * @requires module:managers/request-manager
 * @requires module:managers/message-manager
 * @requires module:components/element
 * @requires module:components/modal
 * @requires module:utils/utils
 * @requires module:utils/constants
 * @requires module:models/job
 * @requires module:components/modal/px4-deploy-log-modal/px4-deploy-log-modal
 * 
 * @module pages/px4/px4-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import RequestManager from 'managers/request-manager';
import MessageManager from 'managers/message-manager';
import Element from 'components/element';
import Modal from 'components/modal';
import Utils from 'utils/utils';
import Constants from 'utils/constants';
import Job from 'models/job';
import PX4DeployLogModal from 'components/modal/px4-deploy-log-modal/px4-deploy-log-modal';

import html from './px4-page.html';
import css from './px4-page.css';

/**
 * Ansel's PX4 page.
 * This page gives access to PX4's logs.
 * 
 * @extends module:pages/page~Page
 */
class PX4Page extends Page{
    /**
     * Creates an instance of PX4Page.
     */
    constructor(){
        super();
        this.name = 'px4';
        this.clear();
    }

    /**
     * Clear view to no log selected.
     */
    clear(){
        /**
         * List of logs on server.
         * @private
         * @member {Array} 
         * */
        this.log_list = [];
        /**
         * Currently displayed log.
         * @private
         * @member {Object} 
         * */
        this.current_log = null;
        /**
         * Currently selected log tab.
         * @private
         * @member {Array} 
         * */
        this.current_tab = 'map';

        /**
         * Current log data.
         * @private
         * @member {Object} 
         * */
        this.data = null;
        /**
         * Current log parameters.
         * @private
         * @member {Object} 
         * */
        this.params = null;

        /**
         * Indicates if a given graph tab has already been drawn.
         * @private
         * @member {Object.<string, boolean>} 
         * */
        this.drawn_graph_list = {};

        /**
         * Map object to display flight from logs.
         * @private
         * @member {Object} 
         * */
        this.map = null;
    }

    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);
        this.right_drawer = this.getContainer().querySelector('.as-right-drawer');

        this.log_actions_container = this.getContainer().querySelector('.as-px4-toolbar-log-actions-container');
        
        this.empty_container = this.getContainer().querySelector('.as-empty-container');
        this.graph_container = this.getContainer().querySelector('.as-px4-graph-container');
        this.tab_container = this.getContainer().querySelector('.as-px4-tab-container');

        this.deploy_button = this.getContainer().querySelector('.as-px4-page-actions-deploy-button');
        this.open_button = this.getContainer().querySelector('.as-px4-page-actions-open-button');
        this.download_log_button = this.getContainer().querySelector('.as-px4-page-actions-download-log-button');
        this.delete_log_button = this.getContainer().querySelector('.as-px4-page-actions-delete-log-button');
        
        this.log_list_container = this.getContainer().querySelector('.as-log-list-container');

        this.deploy_button.onclick = this.onDeployButtonClick.bind(this);
        this.open_button.onclick = this.onOpenButtonClick.bind(this);
        this.download_log_button.onclick = this.onDownloadLogButtonClick.bind(this);
        this.delete_log_button.onclick = this.onDeleteLogButtonClick.bind(this);

        this.getContainer().querySelector('.as-px4-map-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-flight-mode-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-gps-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-motors-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-attitude-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-imu-mag-baro-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-battery-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-gp-setpoints-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-lp-setpoints-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-gv-setpoints-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-ekf-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-log-timing-button').onclick = this.onTabClick.bind(this);
        this.getContainer().querySelector('.as-px4-parameters-button').onclick = this.onTabClick.bind(this);
    }

    /**
     * Gets the tab container for a given tab identifier.
     * 
     * @param {string} iTabId - Tab id.
     * @returns {HTMLElement} Tab container element.
     */
    getTabContainer(iTabId){
        return this.tab_container.querySelector('.as-px4-' + iTabId + '-tab');
    }

    /**
     * Gets the childrens of a tab container for a given tab identifier.
     * 
     * @param {string} iTabId - Tab id.
     * @returns {HTMLElement} Children of a tab container element.
     */
    getTabContainerList(iTabId){
        return this.tab_container.children;
    }

    /**
     * Gets the tab button for a given tab identifier.
     * 
     * @param {string} iTabId - Tab id.
     * @returns {HTMLElement} Tab button element.
     */
    getTabButtonList(iTabId){
        return this.tab_container.children;
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @override
     */
    onShow(){
        this.refresh();
    }

    /**
     * When user clicks on a tab button.
     * Displays the corresponding tab0
     * 
     * @param {Object} iEvent - Event object.
     */
    onTabClick(iEvent){
        this.displayTab(iEvent.currentTarget.getAttribute('tabid'));
    }

    /**
     * Refreshes the title of the page to correspond to opened log.
     */
    refreshTitle(){
        var title = __('PAGE_TITLE_PX4_LOGS');
        if(this.current_log){
            title = this.current_log.name;
        }
        this.setTitle(title);
    }

    /**
     * Displays a tab from its identifier.
     * 
     * @param {any} iTabId - Tab identifier to display.
     */
    displayTab(iTabId){
        var tab_list = this.getTabContainerList();
        for(let i = 0; i < tab_list.length; i++){
            tab_list[i].setAttribute('hidden', true);
        }

        var new_tab = this.getTabContainer(iTabId);
        if(!new_tab){ return; }
        this.current_tab = iTabId;
        new_tab.removeAttribute('hidden');

        if(!this.drawn_graph_list[iTabId]){
            this.drawGraph(iTabId);
            this.drawn_graph_list[iTabId] = true;
        }
        var graph_container_list = this.tab_container.getElementsByClassName('graph-container');
        for(let i = 0; i < graph_container_list.length; i++){
            Plotly.Plots.resize(graph_container_list[i]);
        }
    }

    /**
     * Called when user clicks on the open log button.
     */
    onOpenButtonClick(){
        this.right_drawer.classList.toggle('active');            
    }

    /**
     * Called when user clicks on the deploy log button.
     */
    onDeployButtonClick(){
        var context = this;
        this.enableSpinner(true);

        var job = new Job('px4', {
            cmd: 'logs-list'
        });

        job.start(
            function(iJob){
                context.enableSpinner(false); 

                var logs = iJob.result;
                if(!logs || logs.length === 0){ return; }
                               
                var panel = new PX4DeployLogModal(context, logs);
                panel.show();
            },
            function(iJob){
                context.enableSpinner(false);
            }
        );
    }

    /**
     * Called when user clicks on the download log button.
     */
    onDownloadLogButtonClick(){
        if(!this.current_log || !this.current_log.urls){ return; }
        Utils.downloadFile(this.current_log.urls.data);
    }

    /**
     * Called when user clicks on the delete log button.
     */
    onDeleteLogButtonClick(){
        var context = this;

        new Modal({
            title: __('MSG_WARNING_LOGS_DELETE_TITLE'),
            html: __('MSG_WARNING_LOGS_DELETE'),
            actions: [
                {
                    title: __('CANCEL')
                },
                {
                    accept: true,
                    title: __('VALIDATE'),
                    callback: function(){
                        RequestManager.delete('api/logs/' + context.current_log.id, {type: 'json'})
                            .then(res => {
                                context.enableSpinner(false);
                                context.refresh();
                            })
                            .catch(err => {
                                context.enableSpinner(false);
                            });                                                 
                    }
                }
            ]
        }).show();
    }

    /**
     * Requests the server to delete a list of logs.
     * 
     * @param {Array} iLogList - List of logs to delete.
     */
    deleteLogList(iLogList){
        var context = this;

        var job = new Job('px4', {
            logs: iLogList,
            cmd: 'logs-delete'
        });

        this.enableSpinner(true);
        job.start(
            function(iJob){
                context.enableSpinner(false);
            },
            function(iJob){
                context.enableSpinner(false);
            },
            function(iJob){
                context.setSpinnerProgress(iJob.progress);
                context.setSpinnerMessage(iJob.message);
            }
        );
    }

    /**
     * Requests the server to deploy a list of logs.
     * 
     * @param {Array} iLogList - List of logs to deploy.
     */
    deployLogList(iLogList){
        var context = this;

        var job = new Job('px4', {
            logs: iLogList,
            cmd: 'logs-deploy'
        });

        this.enableSpinner(true);
        job.start(
            function(iJob){
                context.enableSpinner(false);
                context.refresh();
            },
            function(iJob){
                context.enableSpinner(false);
            },
            function(iJob){
                context.setSpinnerProgress(iJob.progress);
                context.setSpinnerMessage(iJob.message);
            }
        );
    }

    /**
     * Refreshes user interface.
     */
    refresh(){
        this.clear();

        this.graph_container.setAttribute('hidden', true);
        this.empty_container.setAttribute('hidden', true);
        this.log_actions_container.setAttribute('hidden', true);

        this.refreshTitle();
        this.loadDeployedLogList();
    }

    /**
     * Loads list of deployed logs on server.
     */
    loadDeployedLogList(){
        var context = this;
        context.enableSpinner(true);
        Utils.getPlotlyPromise()
            .then(function(){
                RequestManager.get('api/logs', {type: 'json'})
                    .then(res => {
                        context.enableSpinner(false);
                        context.updateDeployedLogList(res);
                    })
                    .catch(err => {
                        context.enableSpinner(false);
                    });
            });
    }
    /**
     * Updates deployed log list drawer.
     * 
     * @param {Array} iLogList - List of ligs deployed.
     */
    updateDeployedLogList(iLogList){
        var context = this;
        this.log_list = iLogList;

        var drawer_menu = this.getContainer().querySelector('.mdc-drawer__content');
        drawer_menu.innerHTML = '';

        if(iLogList.length === 0){
            this.empty_container.removeAttribute('hidden');
            return;
        }
        this.empty_container.setAttribute('hidden', true);

        var template = '<i class="material-icons mdc-list-item__graphic">timeline</i>'

        for(let i = 0; i < this.log_list.length; i++){
            let curr_log = this.log_list[i];

            let currElement = new Element('a', {
                class: 'mdc-list-item',
                html: template + curr_log.name,
                events: {
                    onclick: function(){
                        context.onDeployedLogClick(curr_log);
                    }
                }
            }).append(drawer_menu);
        }

        this.right_drawer.classList.add('active');
    }
    
    /**
     * Called when user clicks on the deployed log drawer button.
     */
    onDeployedLogClick(iLog){
        this.right_drawer.classList.remove('active');  
        this.loadLog(iLog);
    }

    /**
     * Loads a given log.
     * 
     * @param {Object} iLog - Log object.
     */
    loadLog(iLog){
        var context = this;
        if(!iLog.deployed) { return; }
        if(!iLog.urls || !iLog.urls.data || !iLog.urls.params) { return; }

        this.current_log = iLog;
        this.drawn_graph_list = {};
        this.refreshTitle();
        this.clearGraphs();

        var onError = function(){
            context.enableSpinner(false);
        }
        var onEnded = function(){
            context.enableSpinner(false);

            context.log_actions_container.removeAttribute('hidden');
            context.graph_container.removeAttribute('hidden');
            new mdc.tabs.MDCTabBarScroller(context.getContainer().querySelector('.mdc-tab-bar-scroller'));

            context.enableSpinner(false);
            context.displayTab(context.current_tab);
        }

        this.enableSpinner(true);
        RequestManager.get(iLog.urls.data, {})
            .then(res => {
                context.data = context.convertCSVToData(res);

                RequestManager.get(iLog.urls.params, {})
                    .then(res => {
                        context.params = context.convertTXTToParams(res);
                        onEnded();
                    })
                    .catch(err => {
                        onEnded();
                    });
            })
            .catch(err => {
                onError();
            });
    }

    /**
     * Converts a text parameters to JSON parameters.
     * 
     * @param {string} iTxt - Log parameters as string.
     * @returns {Object} JSON of the parameters of a log.
     */
    convertTXTToParams(iTxt){
        var res = {};

        try {
            var lines = iTxt.match(/[^\r\n]+/g);

            for(let i = 0; i < lines.length; i++){
                let curr_line = lines[i];
                let curr_split = curr_line.replace( /\s\s+/g, ' ' ).split(/\s/g);
                res[curr_split[0]] = curr_split[1];
            }                
        } catch (error) {
            
        }
        return res;
    }

    /**
     * Converts a CSV file to JSON
     * 
     * @param {string} iCSV - CSV as text.
     * @returns {Object} JSON of a log.
     */
    convertCSVToData(iCSV){
        data = [];
        try {
            var data = Utils.convertCSVToJSON(iCSV);

            for(let ind in data){
                data[ind].shift();
                if(ind === 'VER_FwGit'){ continue; }
                data[ind] = data[ind].map(function(x){
                    if(x.length === 0){
                        return null;
                    }
                    var res = parseFloat(x);
                    if(isNaN(res)){
                        return x;
                    }
                    return res;
                });
            }

            var first_time = data['TIME_StartTime'][0];    
            data.index = data['TIME_StartTime'].map(x => (x - first_time) * 0.000001);
            
        } catch (error) {                
        }
        return data;
    }

    /**
     * Draw a graph in a DOM element.
     * 
     * @param {HTMLElement} iTarget - Location of the graph to draw.
     * @param {Array} iData - Array of data array to display.
     * @param {Array} iTitles - Titles of each data to display.
     */
    drawGraphData(iTarget, iData, iTitles){
        var datas = [];
        for(let i = 0; i < iData.length; i++){
            if(iData[i].length < 1){continue;}
            let curr_data = {
                total: 0,
                count: 0,
                mean: 0,
                std: 0,
                min: null,
                p1: 0,
                p2: 0,
                p3: 0,
                max: null
            }

            let curr_ordered =  iData[i].slice().sort(function(a, b){ return a - b;});

            for(let j = 0; j < curr_ordered.length; j++){
                let curr_value = curr_ordered[j];
                if(isNaN(curr_value)) { continue; }
                curr_data.count += 1;
                curr_data.total += curr_value;

                curr_data.min = (!curr_data.min || (curr_value < curr_data.min))?curr_value: curr_data.min;
                curr_data.max = (!curr_data.max || (curr_value > curr_data.max))?curr_value: curr_data.max;
            }
            if(curr_data.count !== 0){
                curr_data.mean = curr_data.total / curr_data.count;
            }

            curr_data.p1 = curr_ordered[Math.round(curr_data.count * 0.25)];
            curr_data.p2 = curr_ordered[Math.round(curr_data.count * 0.5)];
            curr_data.p3 = curr_ordered[Math.round(curr_data.count * 0.75)];

            let total_dist = 0;
            for(let j = 0; j < curr_ordered.length; j++){
                let curr_value = curr_ordered[j];
                if(isNaN(curr_value)) { continue; }
                total_dist += Math.pow(Math.abs(curr_data.mean - curr_value), 2.0);
            }
            curr_data.std = Math.sqrt(total_dist / (curr_data.count - 1));

            datas.push(curr_data);
        }

        let template_headers = ``;
        for(let i = 0; i < iData.length; i++){
            template_headers += `<th>${iTitles[i]}</th>`;
        }

        let generate_row = function(iTitle, iId, iData){
            let template = `<tr><th>${iTitle}</th>`;
            for(let i = 0; i < iData.length; i++){
                template += `<td>${iData[i][iId].toFixed(5)}</td>`;
            }
            template += `</tr>`;
            return template;
        }

        let template = `
            <table>
                <thead>
                    <tr>
                        <th></th>
                        ${template_headers}
                    </tr>
                </thead>    
                <tbody>
                    ${generate_row('count', 'count', datas)}
                    ${generate_row('mean', 'mean', datas)}
                    ${generate_row('std', 'std', datas)}
                    ${generate_row('min', 'min', datas)}
                    ${generate_row('%25', 'p1', datas)}
                    ${generate_row('%50', 'p2', datas)}
                    ${generate_row('%75', 'p3', datas)}
                    ${generate_row('max', 'max', datas)}
                </tbody>
            </table>
        `;

        iTarget.innerHTML = template;
    }

    /**
     * Clears all the graph tab views.
     */
    clearGraphs(){
        var tab_list = this.getTabContainerList();
        for(let i = 0; i < tab_list.length; i++){
            tab_list[i].innerHTML = '';
        }
    }

    /**
     * Draw a given graph tab.
     * 
     * @param {string} iTab - Identifier of tab to draw.
     */
    drawGraph(iTab){
        switch (iTab) {
            case 'map': this.displayMapGraph(); break;
            case 'flight-mode': this.displayFlightModeGraph(); break;
            case 'gps': this.displayGPSGraph(); break;
            case 'motors': this.displayMotorsGraph(); break;
            case 'attitude': this.displayAttitudeGraph(); break;
            case 'battery': this.displayBatteryGraph(); break;
            case 'imu-mag-baro': this.displayIMUMagBaroGraph(); break;
            case 'gp-setpoints': this.displayGPSetpointsGraph(); break;
            case 'lp-setpoints': this.displayLPSetpointsGraph(); break;
            case 'gv-setpoints': this.displayGVSetpointsGraph(); break;
            case 'ekf': this.displayEKFGraph(); break;
            case 'log-timing': this.displayLogTimingGraph(); break;
            case 'parameters': this.displayParametersGraph(); break;
        }
    }

    /**
     * Draws a graph page made of a set of plots.
     * 
     * @param {string} iTab - Identifier of the tab.
     * @param {Object} iGraphList - List of graphs to display.
     */
    drawPlotPage(iTab, iGraphList){
        var tab_container = this.getTabContainer(iTab);

        var template = '';
        for(let i = 0; i < iGraphList.length; i++){
            template += '<div class="graph-container"></div><div class="data-container"></div>';
        }
        tab_container.innerHTML = template;

        var graph_container_list = tab_container.getElementsByClassName('graph-container');
        var data_container_list = tab_container.getElementsByClassName('data-container');

        for(let i = 0; i < iGraphList.length; i++){
            let curr_graph = iGraphList[i];

            var curr_layout = curr_graph.layout;
            var curr_data = curr_graph.data;
            curr_layout.showlegend = true;

            if(!curr_layout.height){
                curr_layout.height =  500;
            }
            if(!curr_layout.margin){
                curr_layout.margin = {
                    autoexpand: true,
                    l: 70,
                    r: 30,
                    t: 10
                };
                if(curr_layout.title){
                    curr_layout.margin.t = 50;
                }
            }
            
            Plotly.plot(graph_container_list[i], curr_data, curr_layout);
            
            if(curr_graph.describe){
                let curr_describe_data = [];
                let curr_describe_names = [];
                for(let j = 0; j < curr_graph.data.length; j++){
                    curr_describe_data.push(curr_graph.data[j].y);
                    curr_describe_names.push(curr_graph.data[j].name);
                }

                this.drawGraphData(data_container_list[i], curr_describe_data, curr_describe_names);
            }
        }
    }

    /**
     * Draws GPS graph tab.
     */
    displayGPSGraph(){
        this.drawPlotPage('gps', [{
            describe: true,
            layout: {
                title: 'GPS Status',
                xaxis: {
                    title: 'Time [s]'
                }
            },
            data: [
                {name: 'GPS_Fix', y: this.data['GPS_Fix'], x: this.data.index},
                {name: 'GPS_EPH', y: this.data['GPS_EPH'], x: this.data.index},
                {name: 'GPS_EPV', y: this.data['GPS_EPV'], x: this.data.index},
                {name: 'GPS_nSat', y: this.data['GPS_nSat'], x: this.data.index}
            ]
        }]);
    }
    /**
     * Draws motors graph tab.
     */
    displayMotorsGraph(){
        var yaw_balance = [];
        var pitch_balance = [];
        var roll_balance = [];
        for(let i = 0; i < this.data.index.length; i++){
            yaw_balance.push(this.data['OUT0_Out0'][i] + this.data['OUT0_Out1'][i] - this.data['OUT0_Out2'][i] - this.data['OUT0_Out3'][i]);
            pitch_balance.push(this.data['OUT0_Out0'][i] + this.data['OUT0_Out2'][i] - this.data['OUT0_Out1'][i] - this.data['OUT0_Out3'][i]);
            roll_balance.push(this.data['OUT0_Out0'][i] + this.data['OUT0_Out3'][i] - this.data['OUT0_Out1'][i] - this.data['OUT0_Out2'][i]);
        }

        this.drawPlotPage('motors', [
            {
                describe: true,
                layout: {
                    title: 'Motors',
                    yaxis: {
                        title: '[PWM]'
                    },
                    xaxis: {
                        title: 'Time [s]'
                    }
                },
                data: [
                    {name: 'OUT0_Out0', y: this.data['OUT0_Out0'], x: this.data.index},
                    {name: 'OUT0_Out1', y: this.data['OUT0_Out1'], x: this.data.index},
                    {name: 'OUT0_Out2', y: this.data['OUT0_Out2'], x: this.data.index},
                    {name: 'OUT0_Out3', y: this.data['OUT0_Out3'], x: this.data.index}
                ]
            },
            {
                describe: true,
                layout: {
                    title: 'Motors balance'
                },
                data: [
                    { name: 'Yaw Balance', type: 'violin', y: yaw_balance },
                    { name: 'Pitch Balance', type: 'violin', y: pitch_balance },
                    { name: 'Roll Balance', type: 'violin', y: roll_balance }
                ]
            }
        ]);
    }

    /**
     * Draws attitude graph tab.
     */
    displayAttitudeGraph(){
        this.drawPlotPage('attitude', [
            {
                layout: {
                    yaxis: { title: 'Roll [deg]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'ATT_Yaw', y: this.data['ATT_Yaw'], x: this.data.index},
                    {name: 'ATSP_YawSP', y: this.data['ATSP_YawSP'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: 'Pitch [deg]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'ATT_Pitch', y: this.data['ATT_Pitch'], x: this.data.index},
                    {name: 'ATSP_PitchSP', y: this.data['ATSP_PitchSP'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: 'Yaw [deg]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'ATT_Roll', y: this.data['ATT_Roll'], x: this.data.index},
                    {name: 'ATSP_RollSP', y: this.data['ATSP_RollSP'], x: this.data.index}
                ]
            },
        ]);
    }

    /**
     * Draws battery graph tab.
     */
    displayBatteryGraph(){
        this.drawPlotPage('battery', [
            {
                layout: {
                    title: 'Battery Voltage',
                    yaxis: { title: 'Voltage [V]' },
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'BATT_V', y: this.data['BATT_V'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'STAT_BatRem', y: this.data['STAT_BatRem'], x: this.data.index}
                ]
            }
        ]);
    }

    /**
     * Draws IMU / Barometer / Magnetometer graph tab.
     */
    displayIMUMagBaroGraph(){
        var computeRollingMean = function(iData, iWindow){
            var res = [];
            var rolling_count = 0;
            for(let i = 0; i < iData.length; i++){
                if(isNaN(iData[i])) { continue; }
                rolling_count += iData[i];

                if(i >= iWindow){
                    res.push(rolling_count / (iWindow + 1));
                    rolling_count -= iData[i - iWindow + 1];
                }
                else{
                    res.push(null);
                }
            }
            return res;
        };

        this.drawPlotPage('imu-mag-baro', [
            {
                layout: {
                    yaxis: { title: '[M/s^2]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_AccX', y: this.data['IMU_AccX'], x: this.data.index},
                    {name: 'IMU1_AccX', y: this.data['IMU1_AccX'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: '[M/s^2]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_AccY', y: this.data['IMU_AccY'], x: this.data.index},
                    {name: 'IMU1_AccY', y: this.data['IMU1_AccY'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: '[M/s^2]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_AccZ', y: this.data['IMU_AccZ'], x: this.data.index},
                    {name: 'IMU1_AccZ', y: this.data['IMU1_AccZ'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: '[rad/s]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_GyroX', y: this.data['IMU_GyroX'], x: this.data.index},
                    {name: 'IMU1_GyroX', y: this.data['IMU1_GyroX'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: '[rad/s]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_GyroY', y: this.data['IMU_GyroY'], x: this.data.index},
                    {name: 'IMU1_GyroY', y: this.data['IMU1_GyroY'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: '[rad/s]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_GyroZ', y: this.data['IMU_GyroZ'], x: this.data.index},
                    {name: 'IMU1_GyroZ', y: this.data['IMU1_GyroZ'], x: this.data.index}
                ]
            },                
            {
                layout: {
                    yaxis: { title: '[G]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_MagX', y: this.data['IMU_MagX'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: '[G]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_MagY', y: this.data['IMU_MagY'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: '[G]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'IMU_MagZ', y: this.data['IMU_MagZ'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: '[m]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'SENS_BaroAlt', y: this.data['SENS_BaroAlt'], x: this.data.index},
                    {
                        name: 'Rolling Mean (over 100 samples)', 
                        y: computeRollingMean(this.data['SENS_BaroAlt'], 100), 
                        x: this.data.index
                    }
                ]
            },
            {
                layout: {
                    yaxis: { title: '[°C]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'SENS_BaroTemp', y: this.data['SENS_BaroTemp'], x: this.data.index}
                ]
            },
            {
                layout: {
                    yaxis: { title: 'Pressure [mBar]'},
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'SENS_BaroPres', y: this.data['SENS_BaroPres'], x: this.data.index},
                    {
                        name: 'Rolling Mean (over 100 samples)', 
                        y: computeRollingMean(this.data['SENS_BaroPres'], 100), 
                        x: this.data.index
                    }
                ]
            }
        ]);
    }

    /**
     * Draws GP set points graph tab.
     */
    displayGPSetpointsGraph(){
        this.drawPlotPage('gp-setpoints', [
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'GPOS_Lat', y: this.data['GPOS_Lat'], x: this.data.index},
                    {name: 'GPS_Lat', y: this.data['GPS_Lat'], x: this.data.index},
                    {name: 'GPSP_Lat', y: this.data['GPSP_Lat'], x: this.data.index},
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'GPOS_Lon', y: this.data['GPOS_Lon'], x: this.data.index},
                    {name: 'GPS_Lon', y: this.data['GPS_Lon'], x: this.data.index},
                    {name: 'GPSP_Lon', y: this.data['GPSP_Lon'], x: this.data.index},
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'GPOS_Alt', y: this.data['GPOS_Alt'], x: this.data.index},
                    {name: 'GPS_Alt', y: this.data['GPS_Alt'], x: this.data.index},
                    {name: 'GPSP_Alt', y: this.data['GPSP_Alt'], x: this.data.index},
                ]
            }
        ]);
    }
    /**
     * Draws LP set points graph tab.
     */
    displayLPSetpointsGraph(){
        this.drawPlotPage('lp-setpoints', [
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'LPOS_X', y: this.data['LPOS_X'], x: this.data.index},
                    {name: 'LPSP_X', y: this.data['LPSP_X'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'LPOS_Y', y: this.data['LPOS_Y'], x: this.data.index},
                    {name: 'LPSP_Y', y: this.data['LPSP_Y'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'LPOS_Z', y: this.data['LPOS_Z'], x: this.data.index},
                    {name: 'LPSP_Z', y: this.data['LPSP_Z'], x: this.data.index}
                ]
            }
        ]);
    }
    /**
     * Draws GV set points graph tab.
     */
    displayGVSetpointsGraph(){
        this.drawPlotPage('gv-setpoints', [
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'GPOS_VelN', y: this.data['GPOS_VelN'], x: this.data.index},
                    {name: 'GPS_VelN', y: this.data['GPS_VelN'], x: this.data.index},
                    {name: 'GVSP_VX', y: this.data['GVSP_VX'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'GPOS_VelE', y: this.data['GPOS_VelE'], x: this.data.index},
                    {name: 'GPS_VelE', y: this.data['GPS_VelE'], x: this.data.index},
                    {name: 'GVSP_VY', y: this.data['GVSP_VY'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'GPOS_VelD', y: this.data['GPOS_VelD'], x: this.data.index},
                    {name: 'GPS_VelD', y: this.data['GPS_VelD'], x: this.data.index},
                    {name: 'GVSP_VZ', y: this.data['GVSP_VZ'], x: this.data.index}
                ]
            }
        ]);
    }

    /**
     * Draws EKF graph tab.
     */
    displayEKFGraph(){
        var bitfield = function(x, range){
            var res = [];
            for(let i = 0; i < range; i++){
                res.push(x % 2);
                x = Math.floor(x / 2);
            }
            return res;
        }

        var EST0_fHealth_name_list = [
            'velHealth',
            'posHealth',
            'hgtHealth',
            'gyrOffsEx',
            'onGround',
            'staticMode',
            'compass',
            'airspeed'
        ];
        var EST0_fNaN_name_list = [
            'angNaN',
            'summedDelVelNaN',
            'KHNaN',
            'KHPNaN',
            'PNaN',
            'covarianceNaN',
            'kalmanGainsNaN',
            'statesNaN'
        ];
        var EST0_fTOut_name_list = [
            'velTO',
            'posTO',
            'hghtTO',
            'IMUTO'
        ];

        var EST0_fHealth = [];
        var EST0_fNaN = [];
        for(let i = 0; i < 8; i++){
            EST0_fHealth.push({name: EST0_fHealth_name_list[i], y: [], x: this.data.index});
            EST0_fNaN.push({name: EST0_fNaN_name_list[i], y: [], x: this.data.index});
        }

        for(let i = 0; i < this.data['EST0_fHealth'].length; i++){
            var curr_bitfield = bitfield(this.data['EST0_fHealth'][i], 8);

            for(let j = 0; j < 8; j++){
                EST0_fHealth[j].y.push(curr_bitfield[j]?j+0.5:j);
            }
        }
        for(let i = 0; i < this.data['EST0_fNaN'].length; i++){
            var curr_bitfield = bitfield(this.data['EST0_fNaN'][i], 8);

            for(let j = 0; j < 8; j++){
                EST0_fNaN[j].y.push(curr_bitfield[j]?j+0.5:j);
            }
        }

        var EST0_fTOut = [];
        for(let i = 0; i < 4; i++){
            EST0_fTOut.push({name: EST0_fTOut_name_list[i], y: [], x: this.data.index});
        }
        for(let i = 0; i < this.data['EST0_fTOut'].length; i++){
            var curr_bitfield = bitfield(this.data['EST0_fTOut'][i], 4);

            for(let j = 0; j < 4; j++){
                EST0_fTOut[j].y.push(curr_bitfield[j]?j+0.5:j);
            }
        }


        this.drawPlotPage('ekf', [
            {
                layout: {
                    xaxis: { title: 'Time [s]' },
                    yaxis: { 
                        range: [0, 8],
                        tickvals: [0,1,2,3,4,5,6,7],
                        ticktext: EST0_fHealth_name_list
                    }
                },
                data: EST0_fHealth
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' },
                    yaxis: { 
                        range: [0, 8],
                        tickvals: [0,1,2,3,4,5,6,7],
                        ticktext: EST0_fNaN_name_list
                    }
                },
                data: EST0_fNaN
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' },
                    yaxis: { 
                        range: [0, 4],
                        tickvals: [0,1,2,3],
                        ticktext: EST0_fTOut_name_list
                    }
                },
                data: EST0_fTOut
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'Delta Angle Bias X', y: this.data['EST0_s10'], x: this.data.index},
                    {name: 'Delta Angle Bias Y', y: this.data['EST0_s11'], x: this.data.index},
                    {name: 'Delta Angle Bias Z', y: this.data['EST1_s12'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'Accel Bias', y: this.data['EST1_s13'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'Earth Magnetic Field North', y: this.data['EST1_s16'], x: this.data.index},
                    {name: 'Earth Magnetic Field East', y: this.data['EST1_s17'], x: this.data.index},
                    {name: 'Earth Magnetic Field Down', y: this.data['EST1_s18'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'Body Magnetic Field X', y: this.data['EST1_s19'], x: this.data.index},
                    {name: 'Body Magnetic Field Y', y: this.data['EST1_s20'], x: this.data.index},
                    {name: 'Body Magnetic Field Z', y: this.data['EST1_s21'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'Glitch Position Test Ratio', y: this.data['EKFG_posTestRatio'], x: this.data.index}
                ]
            }
        ]);
    }

    /**
     * Draws Log timing graph tab.
     */
    displayLogTimingGraph(){
        var dts = [];
        
        for(let i = 0; i < this.data.index.length - 1; i++){
            let value = this.data.index[i + 1] - this.data.index[i];
            if(value > 1000){ continue; }
            dts.push(value);
        }

        this.drawPlotPage('log-timing', [
            {
                describe: true,
                layout: {
                    title: 'Timing Statistics',
                    xaxis: { title: 'Time [s]' }
                },
                data: [{ type: 'violin', name: '', y: dts }]
            }
        ]);
    }

    /**
     * Draws parameters graph tab.
     */
    displayParametersGraph(){
        if(!this.params){ return; }
        var tab_container = this.getContainer().querySelector('.as-px4-parameters-tab');

        tab_container.innerHTML = '<div class="table-container"></div>';
        var table_container_list = tab_container.getElementsByClassName('table-container');

        var color_list = [];
        var data_list = [[],[]];
        for(let i in this.params){
            color_list.push((i%2 === 0)?'lightgrey':'white');
            data_list[0].push(i);
            data_list[1].push(this.params[i]);
        }
            
        var data = [{
            type: 'table',
            header: {
                values: [['Parameter'], ['Value']],
                align: 'center',
                line: {width: 1, color: 'black'},
                fill: {color: 'rgb(78, 161, 213)'}
            },
            cells: {
                values: data_list,
                align: 'center',
                line: {color: 'black', width: 1},
                fill: {color: [color_list]}
            }
        }];

        Plotly.plot(table_container_list[0], data, {
            margin: {
                autoexpand: true,
                t: 10
            }
        });
    }

    /**
     * Draws flight mode graph tab.
     */
    displayFlightModeGraph(){
        var STAT_ArmState = [];
        var STAT_BatWarn = [];
        var STAT_FailsafeState = [];
        var STAT_Landed = [];
        for(let i = 0; i < this.data.index.length - 1; i++){
            STAT_ArmState.push(this.data['STAT_ArmState'][i] * 0.25);
            STAT_BatWarn.push(this.data['STAT_BatWarn'][i] * 0.5 + 1);
            STAT_FailsafeState.push(this.data['STAT_FailsafeState'][i] * 0.5 + 2);
            STAT_Landed.push(this.data['STAT_Landed'][i] * 0.5 + 3);
        }
        this.drawPlotPage('flight-mode', [
            {
                layout: {
                    title: 'Flight Modes',
                    xaxis: { title: 'Time [s]' },
                    yaxis: { 
                        range: [0, 11],
                        tickvals: [0,1,2,3,4,5,6,7,8,9,10,11],
                        ticktext: [
                            'MANUAL', 'ALTCTL', 'POSCTL',
                            'AUTO_MISSION', 'AUTO_LOITER', 'AUTO_RTL',
                            'ACRO', 'OFFBOARD', 'LAND',
                            'EMERGENCYLAND', 'SIMPLE_RTL'
                        ],
                        title: 'Time [s]' 
                    }
                },
                data: [{name: 'STAT_MainState', y: this.data['STAT_MainState'], x: this.data.index}]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' },
                    yaxis: { 
                        range: [0, 3],
                        tickvals: [0,1,2,3],
                        ticktext: ['Arm State (1 / 4)', 'BatWarn', 'FailsafeState', 'AUTO_Landed'],
                        title: 'Time [s]' 
                    }
                },
                data: [
                    {name: 'STAT_ArmState', y: STAT_ArmState, x: this.data.index},
                    {name: 'STAT_BatWarn', y: STAT_BatWarn, x: this.data.index},
                    {name: 'STAT_FailsafeState', y: STAT_FailsafeState, x: this.data.index},
                    {name: 'STAT_Landed', y: STAT_Landed, x: this.data.index},
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' },
                    yaxis: { 
                        range: [0, 15],
                        tickvals: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
                        ticktext: [
                            'MANUAL',
                            'ALTCTL',
                            'POSCTL',
                            'AUTO_MISSION',
                            'AUTO_LOITER',
                            'AUTO_RTL',
                            'AUTO_RCRECOVER',
                            'AUTO_RTGS',
                            'AUTO_LANDENGFAIL',
                            'AUTO_LANDGPSFAIL',
                            'ACRO',
                            'LAND',
                            'DESCEND',
                            'TERMINATION',
                            'OFFBOARD',
                            'SIMPLE_RTL'
                        ],
                        title: 'Time [s]' 
                    }
                },
                data: [
                    {name: 'GPSP_NavState', y: this.data['GPSP_NavState'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'MISS_seq_curt', y: this.data['MISS_seq_curt'], x: this.data.index}
                ]
            },
            {
                layout: {
                    xaxis: { title: 'Time [s]' }
                },
                data: [
                    {name: 'MAN_thrust', y: this.data['MAN_thrust'], x: this.data.index},
                    {name: 'MAN_pitch', y: this.data['MAN_pitch'], x: this.data.index},
                    {name: 'MAN_roll', y: this.data['MAN_roll'], x: this.data.index},
                    {name: 'MAN_yaw', y: this.data['MAN_yaw'], x: this.data.index}
                ]
            }
        ]);
    }

    /**
     * Draws map graph tab.
     */
    displayMapGraph(){
        var context = this;
        var precision = 3;
        var tab_container = this.getContainer().querySelector('.as-px4-map-tab');

        if(this.data['GPS_Lat'].length === 0 || this.data['GPS_Lon'].length === 0){
            MessageManager.showError(__('MSG_ERROR_PX4_NO_GPS_DATA'));
            return;
        }
        var description_template = ``;

        if(this.params){ 
            var system_id = parseInt(this.params['SYS_AUTOSTART']);
            var system_name = Constants.UAS_SYSTEM_NAMES[system_id];

            var description_template = `${system_name}, Flight: ${new Date(this.data['GPS_GPSTime'][0] / 1000)}`;
            if(this.params['TEST_DEV']){
                var system_prefix = this.params['TEST_DEV'].substring(0, 3);
                var system_number = this.params['TEST_DEV'].substr(3);

                if( system_prefix === '624'){
                    description_template = `${system_name} ${parseInt(system_number)}, Flight: ${new Date(this.data['GPS_GPSTime'][0] / 1000)}`;
                }
            } 
        }

        var firmware_template = `Firmware Version: ${this.data['VER_FwGit'][0]}`;
        
        var distances = this.conputeDistances();
        
        var distances_template = `
            <li class="mdc-list-item">Total Distance Covered: ${distances.total_distance.toFixed(precision)}km (Planned: ${distances.nominal_distance.toFixed(precision)}km)</li>
            <li class="mdc-list-item">Total Ascent: ${distances.total_climb.toFixed(precision)}m (Planned ${distances.nominal_climb.toFixed(precision)}m)</li>
            <li class="mdc-list-item">Total Descent: ${distances.total_descent.toFixed(precision)}m (Planned ${distances.nominal_descent.toFixed(precision)}m)</li>
        `;

        tab_container.innerHTML = `
            <div class="map-container"></div>
            <div class="as-fab-container">
                <button class="mdc-fab material-icons mdc-ripple-upgraded as-px4-map-tab-info-button">
                    <span class="mdc-fab__icon">info</span>
                </button>
                <div class="mdc-menu-anchor">
                    <div class="mdc-menu as-px4-map-tab-info-menu mdc-list--dense" tabindex="-1" style="transform: scale(0.9, 0.9);">
                        <ul class="mdc-menu__items mdc-list" role="menu"">
                            <li class="mdc-list-item">${description_template}</li>
                            <li class="mdc-list-item">${firmware_template}</li>
                            <li class="mdc-list-divider" role="separator"></li>
                            ${distances_template}
                        </ul>
                    </div>
                </div>
            </div> 
        `;

        var button_info_elt = tab_container.querySelector('.as-px4-map-tab-info-button');
        var menu_info_elt = tab_container.querySelector('.as-px4-map-tab-info-menu');
        var menu_info = new mdc.menu.MDCMenu(menu_info_elt);

        button_info_elt.onclick = function(){
            menu_info.open = !menu_info.open;
        }

        if(this.data['GPS_Lat'][0] < 0.01 && this.data['GPS_Lon'][0] < 0.01){
            this.plotMapIndoor(tab_container.querySelector('.map-container'));
        }
        else{
            this.plotMapOutdoor(tab_container.querySelector('.map-container'));
        }
    }

    /**
     * Draws a map for an indoor flight.
     * 
     * @param {HTMLElement} iTarget - Element to display the map.
     */
    plotMapIndoor(iTarget){
        Plotly.newPlot(iTarget, [{
            x: this.data['GPS_Lon'],
            y: this.data['GPS_Lat'],
            mode: 'markers',
            type: 'scatter'
        }]);
    }

    /**
     * Draws a map for an outdoor flight.
     * 
     * @param {HTMLElement} iTarget - Element to display the map.
     */
    plotMapOutdoor(iTarget){
        var context = this;
        this.map = new mapboxgl.Map({
            container: iTarget,
            style: Constants.MAPBOX_STYLE_LIST[0]
        });

        var bounds = new mapboxgl.LngLatBounds();
        var feature_list = [];
        for(let i = 0; i < this.data.index.length; i++){
            bounds.extend(new mapboxgl.LngLat(this.data['GPS_Lon'][i], this.data['GPS_Lat'][i]));
            feature_list.push({
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [this.data['GPS_Lon'][i], this.data['GPS_Lat'][i]]
                }
            });
        }
        
        setTimeout(() => {
            context.map.addLayer({
                id: 'positions',
                type: 'circle',
                source: {
                    type: 'geojson',
                    data: {
                        type: 'FeatureCollection',
                        features: feature_list
                    }
                },
                "paint": {
                    "circle-radius": 3,
                    "circle-color": "#007cbf"
                }
            });

            context.map.fitBounds(bounds,{
                speed: 10000,
                padding: 100
            });
            
        }, 100);
    }

    /**
     * Computes flight distances.
     * - total_distance: Effective total distance of the flight.
     * - nominal_distance: Theorical distance of flight.
     * - total_climb: Effective altitude gain of the flight.
     * - total_descent: Effective altitude loss of the flight.
     * - nominal_climb: Theorical altitude gain of the flight.
     * - nominal_descent: Theorical altitude loss of the flight.
     * 
     * @returns {Object} Flight distances.
     */
    conputeDistances(){
        var res = {
            total_distance: 0,
            nominal_distance: 0,
            total_climb: 0,
            total_descent: 0,
            nominal_climb: 0,
            nominal_descent: 0
        };

        try {
            var last_ind = this.data.index.length - 1;

            // List indexes of waypoints.
            var waypoint_times = [];
            for(let i = 1; i < this.data.index.length; i++){
                if(this.data['MISS_seq_curt'][i - 1] !== this.data['MISS_seq_curt'][i]){
                    waypoint_times.push(i);
                }
            }
            
            // Compute positions for each waypoints (includes first and last positions).
            var waypoint_latitudes = [this.data['GPS_Lat'][0]];
            var waypoint_longitudes = [this.data['GPS_Lon'][0]];
            var waypoint_altitudes = [this.data['GPOS_Alt'][0]];
            for(let i = 0; i < waypoint_times.length; i++){
                waypoint_latitudes.push(this.data['GPSP_Lat'][waypoint_times[i]]);
                waypoint_longitudes.push(this.data['GPSP_Lon'][waypoint_times[i]]);
                waypoint_altitudes.push(this.data['GPSP_Alt'][waypoint_times[i]]);
            }
            waypoint_latitudes.push(this.data['GPS_Lat'][last_ind]);
            waypoint_longitudes.push(this.data['GPS_Lon'][last_ind]);
            waypoint_altitudes.push(this.data['GPOS_Alt'][last_ind]);

            // Compute nominal data.
            res.nominal_distance = 0;
            res.nominal_climb = 0;
            res.nominal_descent = 0;
            for(let i = 1; i < waypoint_latitudes.length; i++){
                let curr_dist = Utils.distVincenty(
                    waypoint_latitudes[i - 1], 
                    waypoint_longitudes[i - 1],
                    waypoint_latitudes[i], 
                    waypoint_longitudes[i]
                );
                if(!isNaN(curr_dist)){
                    res.nominal_distance += curr_dist;
                }
                let curr_alt_diff = waypoint_latitudes[i] - waypoint_latitudes[i - 1];
                if(curr_alt_diff > 0){
                    res.nominal_climb += curr_alt_diff;
                }
                else{
                    res.nominal_descent += curr_alt_diff;
                }
            }

            // Compute total data on all points.
            res.total_distance = 0;
            res.total_climb = 0;
            res.total_descent = 0;
            for(let i = 1; i < this.data.index.length; i++){
                let curr_dist = Utils.distVincenty(
                    this.data['GPOS_Lat'][i - 1], 
                    this.data['GPOS_Lon'][i - 1],
                    this.data['GPOS_Lat'][i], 
                    this.data['GPOS_Lon'][i]
                );
                if(!isNaN(curr_dist)){
                    res.total_distance += curr_dist;
                }
                let curr_alt_diff = this.data['GPOS_Alt'][i] - this.data['GPOS_Alt'][i - 1];
                if(curr_alt_diff > 0){
                    res.total_climb += curr_alt_diff;
                }
                else{
                    res.total_descent += curr_alt_diff;
                }
            }

            res.nominal_distance *= 0.001;
            res.total_distance *= 0.001;
        } catch (error) {  
            res = {
                total_distance: 0,
                nominal_distance: 0,
                total_climb: 0,
                total_descent: 0,
                nominal_climb: 0,
                nominal_descent: 0
            };  
        }
        return res;
    }
};

export default PX4Page;