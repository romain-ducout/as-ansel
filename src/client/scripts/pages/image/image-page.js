/**
 * Module of Ansel's image page.
 * 
 * @requires module:pages/page
 * @requires module:managers/request-manager
 * @requires module:components/element
 * @requires module:utils/utils
 * @requires module:models/job
 * 
 * @module pages/image/image-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import RequestManager from 'managers/request-manager';
import Element from 'components/element';
import Utils from 'utils/utils';
import Job from 'models/job';

import html from './image-page.html';
import css from './image-page.css';

/**
 * Height of an image item in drawer.
 * 
 * @private
 * @type {number}
 */
var DRAWER_ITEM_HEIGHT = 200;

/**
 * Ansel's image page.
 * This page shows a given image taken by the drone.
 * 
 * @extends module:pages/page~Page
 */
class ImagePage extends Page{
    /**
     * Creates an instance of ImagePage.
     */
    constructor(){
        super();
        this.name = 'image';

        /**
         * Current flight displayed in the page.
         * @private
         * @member {module:models/flight~Flight} 
         * */
        this.flight = null;
        /**
         * Current image displayed in the page.
         * @private
         * @member {module:models/image~Image} 
         * */
        this.image = null;
        /**
         * Indicates if GPS markers should be displayed.
         * @private
         * @member {boolean} 
         * */
        this.enable_markers = true;

        /**
         * Images of flight to be displayed in drawer.
         * @private
         * @member {Object.<string, HTMLElement>} 
         * */
        this.drawer_image_elt_list = {};
    }

    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.resetControls();
        this.initTemplate(html);

        this.initObjects();
        this.initCommands();
        this.initEvents();

        this.setImageType(0);
    }

    /**
     * Resets controls values.
     */
    resetControls(){
        this.dragging = false;
        this.zooming = false;
        this.prev_drag_pos = {x: 0, y: 0};
        this.prev_zoom_dist = 0;
        this.transform = {
            scale: 1,
            translate: {
                x: 0,
                y: 0
            }
        };
    }

    /**
     * Initializes the page's members.
     */
    initObjects(){
        this.center_container = this.getContainer().querySelector('.as-page-center-container');
        this.input_elt = this.getContainer().querySelector('.as-flight-image-input');
        this.flight_image_elt = this.getContainer().querySelector('.as-flight-image');
        this.flight_image_container_elt = this.getContainer().querySelector('.as-flight-image-container');
        this.flight_marker_container_elt = this.getContainer().querySelector('.as-flight-marker-container');

        this.back_button = this.getContainer().querySelector('.as-image-page-actions-back-button');
        this.download_button = this.getContainer().querySelector('.as-image-page-actions-download-button');
        this.info_button = this.getContainer().querySelector('.as-image-page-actions-info-button');
        this.views_button = this.getContainer().querySelector('.as-flight-page-actions-eye-button');
        this.fullscreen_button = this.getContainer().querySelector('.as-image-page-actions-fullscreen-button');
        this.restore_button = this.getContainer().querySelector('.as-image-page-actions-restore-button');
        this.camera_button = this.getContainer().querySelector('.as-image-page-actions-camera-button');
        this.palette_button = this.getContainer().querySelector('.as-image-page-actions-palette-button');
        
        this.color_button = this.getContainer().querySelector('.as-image-type-color-button');
        this.thermal1_button = this.getContainer().querySelector('.as-image-type-thermal1-button');
        this.thermal2_button = this.getContainer().querySelector('.as-image-type-thermal2-button');
        
        this.color_palette_button = this.getContainer().querySelector('.as-image-type-color-palette-button');
        this.thermal1_palette_button = this.getContainer().querySelector('.as-image-type-thermal1-palette-button');
        this.thermal2_palette_button = this.getContainer().querySelector('.as-image-type-thermal2-palette-button');
        
        this.markers_button = this.getContainer().querySelector('.as-image-page-actions-view-markers-button');

        this.image_drawer_menu = this.getContainer().querySelector('.as-right-drawer-images');
        this.info_drawer_menu = this.getContainer().querySelector('.as-right-drawer-info');
        this.views_menu = this.getContainer().querySelector('.as-image-page-menu-views');
        this.palette_menu = this.getContainer().querySelector('.as-image-page-menu-palette');

        this.actions_container = this.getContainer().querySelector('.as-image-type-actions-container');
    }

    /**
     * Initializes the page's commands.
     */
    initCommands(){            
        this.back_button.onclick = this.onBackButtonClick.bind(this);
        this.download_button.onclick = this.onDownloadButtonClick.bind(this);
        this.info_button.onclick = this.onInfoButtonClick.bind(this);
        this.views_button.onclick = this.onViewsButtonClick.bind(this);
        this.fullscreen_button.onclick = this.onFullscreenButtonClick.bind(this);  
        this.restore_button.onclick = this.onRestoreButtonClick.bind(this); 
        this.markers_button.onclick = this.onMarkersButtonClick.bind(this);
        this.camera_button.onclick = this.onCameraButtonClick.bind(this);
        this.palette_button.onclick = this.onPaletteButtonClick.bind(this);

        this.color_button.onclick = this.onColorButtonClick.bind(this);
        this.thermal1_button.onclick = this.onThermal1ButtonClick.bind(this);
        this.thermal2_button.onclick = this.onThermal2ButtonClick.bind(this);

        this.color_palette_button.onclick = this.onColorButtonClick.bind(this);
        this.thermal1_palette_button.onclick = this.onThermal1ButtonClick.bind(this);
        this.thermal2_palette_button.onclick = this.onThermal2ButtonClick.bind(this);
    }

    /**
     * Initializes the page's events.
     */
    initEvents(){
        this.input_elt.addEventListener('wheel', this.onMouseWheel.bind(this));
        this.input_elt.addEventListener('DOMMouseScroll', this.onMouseWheel.bind(this));
        
        this.input_elt.addEventListener('mousedown', this.onMouseDown.bind(this));
        this.input_elt.addEventListener('mousemove', this.onMouseMove.bind(this));
        
        this.input_elt.addEventListener('touchstart', this.onTouchStart.bind(this));
        this.input_elt.addEventListener('touchend', this.onTouchEnd.bind(this));
        this.input_elt.addEventListener('touchmove', this.onTouchMove.bind(this));

        this.image_drawer_menu.addEventListener('scroll', this.onImageDrawerScroll.bind(this));
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @param {Object} iArgs - Arguments for the page.
     * @param {string} iArgs.flight - Flight of the image to show.
     * @param {string} iArgs.image - Image to show.
     * @override
     */    
    onShow(iArgs){
        var context = this;
        this.flight = iArgs.flight;
        this.image = iArgs.image;

        this.mouseup_handler = this.onMouseUp.bind(this);
        this.keyup_handler = this.onKeyUp.bind(this);
        document.addEventListener('mouseup', this.mouseup_handler);
        document.addEventListener('keyup', this.keyup_handler);

        this.flight_image_container_elt.setAttribute('hidden', 'true');

        this.resetControls();

        this.updateActiveFlight();
        this.updateActiveImage();

        this.refreshImageTransform();
        this.refreshUI();
        this.refreshDrawerImages();
    }

    /**
     * Method called when the page is being hidden.
     * 
     * @override
     */
    onHide(){
        document.removeEventListener('mouseup', this.mouseup_handler);
        document.removeEventListener('keyup', this.keyup_handler);      
    }

    /**
     * Updates page to display current image.
     */
    updateActiveImage(){
        var context = this;
        Utils.getApp().enableSpinner(true);
        this.setTitle(this.image.ind);

        var large_image_loaded = false;
        var image_url_small = this.image.getImageUrl(500);
        var image_url_large = this.image.getImageUrl();

        var image_to_load = this.image;

        this.actions_container.setAttribute('hidden', 'true');
        this.palette_button.setAttribute('hidden', 'true');
        this.info_drawer_menu.innerHTML = '';

        this.image.load(true)
            .then(function(){
                if(image_to_load !== context.image){ return; }
                // Update image.        
                context.image_promise = new Promise(function(resolve, reject){
                    RequestManager.get(image_url_small, {type: 'arraybuffer'})
                        .then(res => {
                            if(large_image_loaded){ return; }
                            if(image_to_load !== context.image){ return; }
        
                            context.displayImage(new Blob([new Uint8Array(res)], {type: 'image/jpeg'} ));
                            Utils.getApp().enableSpinner(false);
                        })
                        .catch(err => {
                            Utils.getApp().enableSpinner(false);
                        });
                    RequestManager.get(image_url_large, {type: 'arraybuffer'})
                        .then(res => {
                            if(image_to_load !== context.image){ return; }
                            large_image_loaded = true;
        
                            var blob = new Blob([new Uint8Array(res)], {type: 'image/jpeg'} );
        
                            resolve(blob)
                            context.displayImage(blob);
                            Utils.getApp().enableSpinner(false);
                        })
                        .catch(err => {
                            Utils.getApp().enableSpinner(false);
                        });
                });
        
                // Update image elements (markers, ...).
                context.updateImageMarkers();
        
                // Update drawer menu with selected image.
                for(let curr_image_id in context.drawer_image_elt_list){
                    if(curr_image_id === context.image.id){
                        context.drawer_image_elt_list[curr_image_id].classList.add('selected');
                    }
                    else{
                        context.drawer_image_elt_list[curr_image_id].classList.remove('selected');
                    }
                }
        
                context.image_drawer_menu.scrollTo(0, 200 * context.image.ind);   
        
                context.updateImageInfo();
                context.updateImageTypeButtons();
            })
            .catch(function(){                
            });
    }

    /**
     * Updates GPS markers on image if any.
     */
    updateImageMarkers(){
        this.flight_marker_container_elt.innerHTML = '';

        var markers = this.flight.getMarkers();
        if(markers && markers[this.image.id]){
            var image_markers = markers[this.image.id];
            for(let i = 0; i < image_markers.length; i++){
                let curr_marker = image_markers[i];

                this.flight_marker_container_elt.innerHTML += `
                    <img 
                        class="as-flight-marker" 
                        style="top:${curr_marker.y * 100}%; left:${curr_marker.x * 100}%" 
                        src="assets/imgs/gps_marker.png"
                    />
                `;
            }
        }
    }

    /**
     * Update page with current flight.
     */
    updateActiveFlight(){
        var context = this;

        var markers = this.flight.getMarkers();

        this.image_drawer_menu.innerHTML = '';
        this.drawer_image_elt_list = {};

        for(let i = 0; i < this.flight.images[this.image.type].length; i++){
            let curr_image = this.flight.images[this.image.type][i];
            let curr_template = '';
            if(curr_image){

                let marker_template = ``;
                if(markers && markers[curr_image.id] && markers[curr_image.id].length > 0){
                    marker_template += `<img class="as-icon-menu-item-marker" src="assets/imgs/gps_marker.png"/>`
                }

                let curr_template = `
                    ${marker_template}
                    <img class="as-image-menu-item-image mdc-elevation--z5" src="" title="${i}"/>
                `;

                let currElement = new Element('div', {
                    class: 'as-image-menu-item',
                    html: curr_template,
                    events: {
                        onclick: function(){
                            context.image = curr_image;
                            context.updateActiveImage();
                        }
                    }
                }).append(this.image_drawer_menu);

                this.drawer_image_elt_list[curr_image.id] = currElement.elt;
            }
            else{
                new Element('div', {
                    class: 'as-image-menu-item',
                    html: `
                        <div class="as-image-menu-item-error mdc-elevation--z5" src="" title="${i}">${__('IMAGE_NOT_AVAILABLE')}</div>
                    `
                }).append(this.image_drawer_menu);
            }
        }
    }

    /**
     * Refresh image drawer when user scrolls.
     */
    onImageDrawerScroll(){
        this.refreshDrawerImages();
    }

    /**
     * Updates the image type buttons depending on image types available in current flight.
     */
    updateImageTypeButtons(){
        var color_image = this.flight.getImage(0, this.image.ind);
        var thermal1_image = this.flight.getImage(1, this.image.ind);
        var thermal2_image = this.flight.getImage(2, this.image.ind);

        var color_count = 0;            
        if(color_image){
            color_count++;
        }
        if(thermal1_image){
            color_count++;
        }
        if(thermal2_image){
            color_count++;
        }

        this.actions_container = this.getContainer().querySelector('.as-image-type-actions-container');
        this.actions_container.setAttribute('hidden', 'true');
        this.palette_button.setAttribute('hidden', 'true');

        if(color_count >= 2){
            this.actions_container.removeAttribute('hidden');
            this.palette_button.removeAttribute('hidden');
            
            if(color_image){
                this.color_button.setAttribute('src', this.flight.images[0][this.image.ind].getImageUrl(150));
                this.color_button.removeAttribute('hidden');
                this.color_palette_button.removeAttribute('hidden');                    
            }
            else{
                this.color_button.setAttribute('hidden', 'true');
                this.color_palette_button.setAttribute('hidden', 'true');
            }

            if(thermal1_image){
                this.thermal1_button.setAttribute('src', this.flight.images[1][this.image.ind].getImageUrl(150));
                this.thermal1_button.removeAttribute('hidden');  
                this.thermal1_palette_button.removeAttribute('hidden');                
            }
            else{
                this.thermal1_button.setAttribute('hidden', 'true'); 
                this.thermal1_palette_button.setAttribute('hidden', 'true');                
            }

            if(thermal2_image){
                this.thermal2_button.setAttribute('src', this.flight.images[2][this.image.ind].getImageUrl(150));
                this.thermal2_button.removeAttribute('hidden');   
                this.thermal2_palette_button.removeAttribute('hidden');                
            }
            else{
                this.thermal2_button.setAttribute('hidden', 'true');  
                this.thermal2_palette_button.setAttribute('hidden', 'true');                
            }
        }
    }

    /**
     * Updates image information drawer.
     */
    updateImageInfo(){
        this.info_drawer_menu.innerHTML = '';

        var convertLatLng = function(iVal){
            return Math.floor(parseFloat(iVal) * 1000) / 1000;                    
        };                

        var item_list = [
            [__('NAME'), this.image.id],
            [__('LATITUDE'), this.image.gps.latitude, convertLatLng],
            [__('LONGITUDE'), this.image.gps.longitude, convertLatLng],
            [__('ALTITUDE'), this.image.gps.altitude, convertLatLng],

            [__('EXIF_IMAGE_MAKE'), this.image.exif.image.Make],
            [__('EXIF_IMAGE_MODEL'), this.image.exif.image.Model],
            [__('EXIF_IMAGE_ORIENTATION'), this.image.exif.image.Orientation],
            [__('EXIF_IMAGE_RESOLUTION_X'), this.image.exif.image.XResolution],
            [__('EXIF_IMAGE_RESOLUTION_Y'), this.image.exif.image.YResolution],
            [__('EXIF_IMAGE_SOFTWARE'), this.image.exif.image.Software]
        ];

        var html = '';
        for(let i = 0; i < item_list.length; i++){
            let value = item_list[i][1];
            if(item_list[i][2]){
                value = item_list[i][2](value);
            }
            html += '<li class="mdc-list-item"><b>' + item_list[i][0] + ':&nbsp;</b> ' + value + '</li>' 
        }

        this.info_drawer_menu.innerHTML += `<ul class="mdc-list mdc-list--non-interactive">${html}</ul>`;
    }

    /**
     * Displays a downloaded image.
     * 
     * @param {Blob} iBlob - Blob image to display.
     */
    displayImage(iBlob){
        var urlCreator = window.URL || window.webkitURL;
        var blob_url = urlCreator.createObjectURL( iBlob );
        this.flight_image_elt.setAttribute('src', blob_url);
        this.flight_image_container_elt.removeAttribute('hidden');
    }

    /**
     * Called when user clicks on the back button.
     */
    onBackButtonClick(){
        this.back();
    }

    /**
     * Called when user clicks on the download image button.
     */
    onDownloadButtonClick(){
        this.image_promise.then((blob) => {
            Utils.downloadBlobAsFile(blob, this.image.id);
        });
    }

    /**
     * Called when user clicks on the views button.
     */
    onViewsButtonClick(){
        var menu = new mdc.menu.MDCMenu(this.views_menu);
        menu.open = !menu.open;            
    }

    /**
     * Called when user clicks on the full screen button.
     */
    onFullscreenButtonClick(){
        var container = this.getContainer().querySelector('.as-image-viewer');
        Utils.setFullScreen(container);
    }

    /**
     * Called when user clicks on the restore image view button.
     * Will reset the position of the image to original zoom/translation.
     */
    onRestoreButtonClick(){
        this.prev_drag_pos = {x: 0, y: 0};
        this.transform = {
            scale: 1,
            translate: {
                x: 0,
                y: 0
            }
        };

        this.refreshImageTransform();
    }

    /**
     * Called when user clicks on the color image button.
     */
    onColorButtonClick(){
        this.setImageType(0);
    }
    /**
     * Called when user clicks on the thermal B&W image button.
     */
    onThermal1ButtonClick(){
        this.setImageType(1);
    }
    /**
     * Called when user clicks on the thermal jet button.
     */
    onThermal2ButtonClick(){
        this.setImageType(2);
    }

    /**
     * Called when user clicks on the toggle markers view button.
     */
    onMarkersButtonClick(){
        this.enable_markers = !this.enable_markers;
        this.refreshUI()
    }

    /**
     * Called when user clicks on the image drawer button.
     */
    onCameraButtonClick(){
        this.info_drawer_menu.classList.remove('active');     
        this.image_drawer_menu.classList.toggle('active');

        if(this.image_drawer_menu.classList.contains('active')){
            this.center_container.classList.add('right-collapsed');
        }
        else{
            this.center_container.classList.remove('right-collapsed');
        }
    }

    /**
     * Called when user clicks on the palette button.
     * This allows to choose image type on smaller screens.
     */
    onPaletteButtonClick(){
        var menu = new mdc.menu.MDCMenu(this.palette_menu);
        menu.open = !menu.open;   
    }

    /**
     * Called when user clicks on the info drawer button.
     */
    onInfoButtonClick(){
        this.image_drawer_menu.classList.remove('active');     
        this.info_drawer_menu.classList.toggle('active');

        if(this.info_drawer_menu.classList.contains('active')){
            this.center_container.classList.add('right-collapsed');
        }
        else{
            this.center_container.classList.remove('right-collapsed');
        }
    }

    /**
     * Displays a given image type.
     * 
     * @param {number} iType - Image type to display.
     */
    setImageType(iType){
        if(!this.flight){ return; }
        if(!this.image){ return; }

        var new_image = this.flight.getImage(iType, this.image.ind);
        if(!new_image){ return; }

        this.image = new_image;
        this.updateActiveFlight();
        this.updateActiveImage();
        this.refreshDrawerImages();
    }

    /**
     * Called when user raises key up event.
     * - UP / DOWN: navigate throught the images of the current flight.
     * - LEFT / RIGHT: changes the image type.
     * 
     * @param {Object} iEvent - Key up event.
     */
    onKeyUp(iEvent){
        var event = iEvent || window.event;

        var image_ind = this.image.ind;
        var image_type = this.image.type;
        var reload = false;

        if(event.keyCode == '38') { // UP
            image_ind--;
        }
        else if(event.keyCode == '40') { // DOWN
            image_ind++;
        }
        else if(event.keyCode == '37') { // LEFT
            image_type = (image_type - 1 + 3) % 3;
            reload = true;
        }
        else if(event.keyCode == '39') { // RIGHT
            image_type = (image_type + 1 + 3) % 3;
            reload = true;
        }
        else{
            return;
        }

        var new_image = this.flight.getImage(image_type, image_ind);
        if(new_image){
            this.image = new_image;
            if(reload){
                this.updateActiveFlight();    
            }
            this.updateActiveImage();
        }

        this.image_drawer_menu.scrollTo(0, 200 * this.image.ind);
        this.refreshDrawerImages();
    }

    /**
     * Refreshes image CSS transformation according to transform value.
     */
    refreshImageTransform(){
        this.flight_image_container_elt.style.transform = `
            translate(${this.transform.translate.x}px, ${this.transform.translate.y}px) 
            scale(${this.transform.scale},${this.transform.scale})`;
    }

    /**
     * Refreshes user interface.
     */
    refreshUI(){
        if(this.enable_markers){
            this.markers_button.classList.add('mdc-list-item--activated');
            this.flight_marker_container_elt.removeAttribute('hidden');
        }
        else {
            this.markers_button.classList.remove('mdc-list-item--activated');
            this.flight_marker_container_elt.setAttribute('hidden', true);
        }
    }

    /**
     * Refreshes images drawer element.
     */
    refreshDrawerImages(){
        var scroll = this.image_drawer_menu.scrollTop;
        var height = this.image_drawer_menu.clientHeight

        var first_image_ind = Math.floor(scroll / DRAWER_ITEM_HEIGHT);
        var last_image_ind = Math.ceil((scroll + height) / DRAWER_ITEM_HEIGHT);

        for(let i = first_image_ind; i < this.image_drawer_menu.children.length && i < last_image_ind; i++){
            let curr_image_elt = this.image_drawer_menu.children[i].getElementsByClassName('as-image-menu-item-image')[0];
            if(!curr_image_elt){ continue; }
            let curr_image_src = curr_image_elt.getAttribute('src');
            if(curr_image_src && curr_image_src.length > 0){ continue; }
            let curr_image = this.flight.getImage(this.image.type, i);
            if(!curr_image){ continue; }
            curr_image_elt.setAttribute('src', curr_image.getImageUrl(250)); 
        }
    }

    /**
     * Called when user raise a mouse down event.
     * Starts dragging.
     * 
     * @param {Object} iEvent - Mouse down event.
     */
    onMouseDown(iEvent){ 
        var x = iEvent.pageX - this.flight_image_container_elt.offsetLeft;
        var y = iEvent.pageY - this.flight_image_container_elt.offsetTop;
        this.startDrag(x, y);
    }
    /**
     * Called when user raise a mouse up event.
     * Stops dragging.
     * 
     * @param {Object} iEvent - Mouse up event.
     */
    onMouseUp(iEvent){
        this.endDrag();
    }
    /**
     * Called when user raise a mouse move event.
     * Performs drag according to mouse move since last call.
     * 
     * @param {Object} iEvent - Mouse mode event.
     */
    onMouseMove(iEvent){
        var x = iEvent.pageX - this.flight_image_container_elt.offsetLeft;
        var y = iEvent.pageY - this.flight_image_container_elt.offsetTop;
        this.drag(x, y);
    }
    /**
     * Called when user raise a mouse wheel event.
     * Performs a zoom.
     * 
     * @param {Object} iEvent - Mouse wheel event.
     */
    onMouseWheel(iEvent){
        var x = iEvent.pageX - this.flight_image_container_elt.offsetLeft;
        var y = iEvent.pageY - this.flight_image_container_elt.offsetTop;

        var delta = iEvent.wheelDelta ? iEvent.wheelDelta/40 : iEvent.detail ? -iEvent.detail : 0;
        var scale = this.transform.scale * Math.pow(1.1, delta);

        this.zoom(x, y, scale);
    }

    /**
     * Called when user raise a touch start event.
     * Starts drag / zoom.
     * 
     * @param {Object} iEvent - Touch start event.
     */
    onTouchStart(iEvent){ 
        if(iEvent.touches.length === 1){
            var t1 = iEvent.touches[0];
            var x = t1.pageX - this.flight_image_container_elt.offsetLeft;
            var y = t1.pageY - this.flight_image_container_elt.offsetTop;
            this.startDrag(x, y);
        }
        else if(iEvent.touches.length === 2){
            var t1 = iEvent.touches[0];
            var t2 = iEvent.touches[1];

            var x1 = t1.pageX - this.flight_image_container_elt.offsetLeft;
            var y1 = t1.pageY - this.flight_image_container_elt.offsetTop;
            var x2 = t2.pageX - this.flight_image_container_elt.offsetLeft;
            var y2 = t2.pageY - this.flight_image_container_elt.offsetTop;

            this.zooming = true;     
            this.prev_zoom_dist = Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2);
        }
    }
    /**
     * Called when user raise a touch end event.
     * Ends drag / zoom.
     * 
     * @param {Object} iEvent - Touch end event.
     */
    onTouchEnd(iEvent){
        this.zooming = false;
        if(iEvent.touches.length === 0){
            this.endDrag();
        }
        else if(iEvent.touches.length === 1){
            var t1 = iEvent.touches[0];
            var x = t1.pageX - this.flight_image_container_elt.offsetLeft;
            var y = t1.pageY - this.flight_image_container_elt.offsetTop;
            this.startDrag(x, y);
        }
    }
    /**
     * Called when user raise a touch move event.
     * Performs drag / zoom.
     * 
     * @param {Object} iEvent - Touch move event.
     */
    onTouchMove(iEvent){
        if(iEvent.touches.length === 1){
            var x = iEvent.touches[0].pageX - this.flight_image_container_elt.offsetLeft;
            var y = iEvent.touches[0].pageY - this.flight_image_container_elt.offsetTop;
            this.drag(x, y);
        }
        else if(iEvent.touches.length === 2){
            var t1 = iEvent.touches[0];
            var t2 = iEvent.touches[1];

            var x1 = t1.pageX - this.flight_image_container_elt.offsetLeft;
            var y1 = t1.pageY - this.flight_image_container_elt.offsetTop;
            var x2 = t2.pageX - this.flight_image_container_elt.offsetLeft;
            var y2 = t2.pageY - this.flight_image_container_elt.offsetTop;

            var zoom_dist = Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2);  
            this.zoom((x1 + x2) * 0.5, (y1 + y2) * 0.5, this.transform.scale * (zoom_dist / this.prev_zoom_dist));

            this.prev_zoom_dist = zoom_dist;  
        }
    }

    /**
     * Starts drag at a given position.
     * 
     * @param {number} x - X position.
     * @param {number} y - Y position.
     */
    startDrag(x, y){
        document.body.style.cursor = 'move'; 
        this.prev_drag_pos = {x: x, y: y};
        this.dragging = true;            
    }

    /**
     * Ends drag.
     */
    endDrag(){
        document.body.style.cursor = '';
        this.dragging = false;            
    }

    /**
     * Performs drag according to given position and previous position.
     * 
     * @param {number} x - X position.
     * @param {number} y - Y position.
     */
    drag(x, y){
        if(!this.dragging){ return; }
        this.transform.translate.x += x - this.prev_drag_pos.x;
        this.transform.translate.y += y - this.prev_drag_pos.y;

        this.refreshImageTransform();

        this.prev_drag_pos = {x: x, y: y};
        this.refreshImageTransform();
    }

    /**
     * Performs a zoom on the image.
     * 
     * @param {number} x - X position.
     * @param {number} y - Y position.
     * @param {number} scale - Scale factor.
     */
    zoom(x, y, scale){
        if(scale > 64){ scale = 64; }
        if(scale < 0.25){ scale = 0.25; }

        var width = this.flight_image_container_elt.clientWidth;
        var height = this.flight_image_container_elt.clientHeight;

        // Compute translation to keep point under mouse.
        var image_x = (x - (width * 0.5) - this.transform.translate.x) / this.transform.scale;
        var image_y = (y - (height * 0.5) - this.transform.translate.y) / this.transform.scale;

        var image_x_new = (x - this.transform.translate.x - (width * 0.5)) / scale;
        var image_y_new = (y - this.transform.translate.y - (height * 0.5)) / scale;

        this.transform.translate.x += (image_x_new -image_x) * scale;
        this.transform.translate.y += (image_y_new -image_y) * scale;

        this.transform.scale = scale;
        this.refreshImageTransform();
    }
};

export default ImagePage;