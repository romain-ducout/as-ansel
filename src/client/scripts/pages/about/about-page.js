/**
 * Module of Ansel's about page.
 * 
 * @requires module:pages/page
 * 
 * @module pages/about/about-page
 * @author Romain Ducout
 */
import Page from 'pages/page';

import html from './about-page.html';
import css from './about-page.css';

/**
 * Ansel's about page.
 * This page shows information about Ansel and links to other Aerosense's interests.
 * 
 * @extends module:pages/page~Page
 */
class AboutPage extends Page{
    /**
     * Creates an instance of AboutPage.
     */
    constructor(){
        super();
        this.name = 'about';
    }

    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);
        this.qgc_card = this.getContainer().querySelector('.as-about-qgc-card');
        this.cloud_card = this.getContainer().querySelector('.as-about-cloud-card');

        this.qgc_card.onclick = this.onQGCCardClick.bind(this);
        this.cloud_card.onclick = this.onCloudCardClick.bind(this);
    }

    /**
     * Called when user clicks on QGC Card.
     * Open link to QGC page.
     */
    onQGCCardClick(){
        var win = window.open('http://www.aerosense.co.jp/aerobostation', '_blank');
        win.focus();
    }

    /**
     * Called when user clicks on Cloud Card.
     * Open link to Aerobo Cloud page.
     */
    onCloudCardClick(){
        var win = window.open('https://aerobocloud.com/', '_blank');
        win.focus();
    }
};

export default AboutPage;