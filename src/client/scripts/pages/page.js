/**
 * Abstract page class module for Ansel pages.
 * 
 * @requires module:managers/request-manager
 * @requires module:components/element
 * @requires module:utils/utils
 * 
 * @module pages/page
 * @author Romain Ducout
 */
import RequestManager from 'managers/request-manager';
import Element from 'components/element';
import Utils from 'utils/utils';

/**
 * Abstract page class for Ansel pages.
 */
class Page{
    /**
     * Creates an instance of Page.
     */
    constructor(){ 
        /**
         * Page name identifier. 
         * @protected
         * @member {string} 
         * */
        this.name = '';
        /**
         * Indicates if the page is active.
         * @protected
         * @member {boolean} 
         * */
        this.shown = false;
        /**
         * DOM element container of page.
         * @protected
         * @member {HTMLElement} 
         * */
        this.container = null;          
    }

    /**
     * Sets the title of the page.
     * Title is being displayed in the topb bar.
     * 
     * @param {string} iTitle - title to give to the page.
     */
    setTitle(iTitle){
        var title_elt = this.getContainer().querySelector('.mdc-toolbar__title');  
        if(!title_elt){ return; }
        title_elt.innerHTML = iTitle;
    }

    /**
     * Gets name of the page.
     * 
     * @returns {string} Name of the page.
     */
    getName(){
        return this.name;
    }

    /**
     * Inits the page's HTML via a string templage.
     * 
     * @param {string} iTemplate - Template to use.
     */
    initTemplate(iTemplate){
        var root = Utils.getApp().getRootElt();  

        var container = new Element('div', {
            id: 'as-' + this.name + '-page-container',
            class: 'as-page-container',
            html: iTemplate,
            attributes: {
                hidden: 'True'
            }
        }).append(root);
        this.container = container.elt;

        // try to initializes menu button
        this.menu_button = this.container.querySelector('.as-page-actions-menu-button');
        if(this.menu_button){
            this.menu_button.onclick = this.onMenuButtonClick.bind(this);
        }
    }

    /**
     * Abstract method that initializes the page when seen the first time.
     * This method is called by page manager, do not call this.
     * 
     * @abstract
     */
    init(){

    }

    /**
     * Abstract method called when the page is being shown on screen.
     * 
     * @abstract
     */
    onShow(){

    }
    /**
     * Abstract method called when the page is being hidden.
     * 
     * @abstract
     */
    onHide(){

    }

    /**
     * Navigates to different page.
     * 
     * @param {string} iPageName - Page to navigate to.
     * @param {Object} iArgs - Arguments to give to the page.
     */
    navigate(iPageName, iArgs){
        Utils.getApp().navigate(iPageName, iArgs);
    }

    /**
     * Goes back to previous page.
     */
    back(){
        Utils.getApp().back();
    }

    /**
     * Shows/hides spinner on screen.
     * 
     * @param {boolean} iEnable - True to show spinner, false to hide it.
     * @param {string} iMessage - Message to show bellow the spinner.
     */
    enableSpinner(iEnable, iMessage){
        Utils.getApp().enableSpinner(iEnable, iMessage);
    }

    /**
     * Sets the spinner at a given progress.
     * 
     * @param {number} iProgress - Progress of the spinner.
     * @param {string} iMessage - Message to display bellow the spinner.
     */
    setSpinnerProgress(iProgress, iMessage){  
        Utils.getApp().enableSpinner(iProgress, iMessage);
    }

    /**
     * Sets the message of the spinner.
     * 
     * @param {string} iMessage - Message to display bellow the spinner.
     */
    setSpinnerMessage(iMessage){  
        Utils.getApp().enableSpinner(iMessage);
    }

    /**
     * Hides the page (called by page manager).
     */
    hide(){
        var container = this.getContainer();
        container.setAttribute('hidden', true);
        this.shown = false;

        this.onHide();
    }

    /**
     * Shows the page (called by page manager).
     * 
     * @param {Object} iArgs - Argument for the page.
     */
    show(iArgs){
        var container = this.getContainer();
        container.removeAttribute('hidden');
        this.shown = true;

        this.onShow(iArgs);
    }

    /**
     * Gets the container DOM element of the page.
     * 
     * @returns {HTMLElement} - Container of the page
     */
    getContainer(){
        return this.container;
    }

    /**
     * Opens naviagetion drawer.
     */
    openDrawer(){
        var drawerElt = this.getContainer().querySelector('.mdc-drawer');
        if(!drawerElt){
            drawerElt = document.querySelector('#as-navigation-drawer');
        }
        var drawer = new mdc.drawer.MDCTemporaryDrawer(drawerElt);
        drawer.open = true;
    }

    /**
     * Closes the navigation drawer.
     */
    closeDrawer(){
        var drawerElt = this.getContainer().querySelector('.mdc-drawer');
        if(!drawerElt){
            drawerElt = document.querySelector('#as-navigation-drawer');
        }
        var drawer = new mdc.drawer.MDCTemporaryDrawer(drawerElt);
        drawer.open = false;
    }

    /**
     * Called when user clicks on menu button.
     */
    onMenuButtonClick(){
        this.openDrawer();
    }
};
export default Page;