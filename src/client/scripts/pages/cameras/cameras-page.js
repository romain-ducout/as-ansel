/**
 * Module of Ansel's cameras page.
 * 
 * @requires module:pages/page
 * @requires module:managers/message-manager
 * @requires module:managers/request-manager
 * @requires module:utils/utils
 * 
 * @module pages/cameras/cameras-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import MessageManager from 'managers/message-manager';
import RequestManager from 'managers/request-manager';
import Utils from 'utils/utils';

import html from './cameras-page.html';
import css from './cameras-page.css';


/**
 * Types of cameras that can be controlled by drone.
 * - NONE: No camera.
 * - QX30: QX30 photo camera.
 * - BOSON: Boson thermal camera.
 * 
 * @readonly
 * @private
 * @enum {number}
 */
var CAMERA_TYPES = {
    NONE: -1,
    QX30: 0,
    BOSON: 1
};

/**
 * Ansel's cameras page.
 * This page shows controls for the different cameras that can be used on drone.
 * 
 * @extends module:pages/page~Page
 */
class CamerasPage extends Page{
    /**
     * Creates an instance of CamerasPage.
     */
    constructor(){
        super();
        this.name = 'cameras';

        /**
         * Currently displayed camera controls.
         * @private
         * @member {CAMERA_TYPES} 
         * */
        this.current_camera = CAMERA_TYPES.NONE;
        /**
         * Thermal camera data.
         * @private
         * @member {Object} 
         * */
        this.thermal_camera = {};
    }


    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);
        this.center_container = this.getContainer().querySelector('.as-page-center-container');
        this.right_drawer_cameras = this.getContainer().querySelector('.as-right-drawer-cameras');
        this.right_drawer_settings = this.getContainer().querySelector('.as-right-drawer-settings');

        this.camera_qx30_container = this.getContainer().querySelector('.as-cameras-qx30-container');
        this.camera_boson_container = this.getContainer().querySelector('.as-cameras-boson-container');

        this.camera_qx30_actions_toolbar = this.getContainer().querySelector('.as-cameras-qx30-actions-toolbar');
        this.camera_boson_actions_toolbar= this.getContainer().querySelector('.as-cameras-boson-actions-toolbar');            

        this.camera_qx30_button = this.getContainer().querySelector('.as-cameras-qx30-button');
        this.camera_boson_button = this.getContainer().querySelector('.as-cameras-boson-button');

        this.initThermalUI();

        this.camera_qx30_button.onclick = this.onCameraQX30ButtonClick.bind(this);
        this.camera_boson_button.onclick = this.onCameraBosonButtonClick.bind(this);

        var socket = io(); 
        socket.on('thermal', this.onThermalUpdate.bind(this));
    }

    /**
     * Initializes the UI of for the thermal camera.
     */
    initThermalUI(){
        this.thermal_image = this.getContainer().querySelector('.as-thermal-image');
        
        this.start_thermal_button = this.getContainer().querySelector('.as-cameras-page-actions-start-thermal-button');
        this.stop_thermal_button = this.getContainer().querySelector('.as-cameras-page-actions-stop-thermal-button');
        this.settings_button = this.getContainer().querySelector('.as-cameras-page-actions-settings-button');
        this.thermal_gray_button = this.getContainer().querySelector('.as-cameras-thermal1-button');
        this.thermal_jet_button = this.getContainer().querySelector('.as-cameras-thermal2-button');
        this.cameras_button = this.getContainer().querySelector('.as-cameras-page-actions-cameras-button');
        
        this.thermal_quality_low_button = this.getContainer().querySelector('.as-cameras-q1-button');
        this.thermal_quality_medium_button = this.getContainer().querySelector('.as-cameras-q2-button');
        this.thermal_quality_high_button = this.getContainer().querySelector('.as-cameras-q3-button');
        
        this.start_thermal_button.onclick = this.onStartThermalButtonClick.bind(this);
        this.stop_thermal_button.onclick = this.onStopThermalButtonClick.bind(this);
        this.settings_button.onclick = this.onSettingsButtonClick.bind(this);
        this.thermal_gray_button.onclick = this.onThermalGrayButtonClick.bind(this);
        this.thermal_jet_button.onclick = this.onThermalJetButtonClick.bind(this);
        this.cameras_button.onclick = this.onCamerasButtonClick.bind(this);
        this.thermal_quality_low_button.onclick = this.onThermalQualityLowButtonClick.bind(this);
        this.thermal_quality_medium_button.onclick = this.onThermalQualityMediumButtonClick.bind(this);
        this.thermal_quality_high_button.onclick = this.onThermalQualityHighButtonClick.bind(this);
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @override
     */
    onShow(){
        if(this.current_camera === CAMERA_TYPES.NONE){
            this.openCamerasDrawer();
        }
        Utils.getApp().enableSpinner(true);

        var context = this;
        RequestManager.get('api/cameras/thermal', {type: 'json'})
            .then(res => {
                context.thermal_camera = res;
                context.updateUI();
                Utils.getApp().enableSpinner(false);
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_CAMERA_INFO'));
            });  
        this.updateUI();
    }

    /**
     * Method called when the page is being hidden.
     * 
     * @override
     */
    onHide(){
        this.stopThermalCamera();
    }

    /**
     * Called when user clicks on the settings button.
     */
    onSettingsButtonClick(){
        this.toggleSettingsDrawer();
    }

    /**
     * Called when user clicks on QX30 camera button.
     * Selects camera QX30 view.
     */
    onCameraQX30ButtonClick(){
        this.closeCamerasDrawer();
        this.current_camera = CAMERA_TYPES.QX30;
        this.updateUI();
    }

    /**
     * Called when user clicks on Boson camera button.
     * Selects thermal camera Boson view.
     */
    onCameraBosonButtonClick(){
        this.closeCamerasDrawer();
        this.current_camera = CAMERA_TYPES.BOSON;
        this.updateUI();
    }

    /**
     * Called when user clicks on start thermal camera button.
     * Starts camera to reccord stream.
     */
    onStartThermalButtonClick(){
        this.startThermalCamera();
    }

    /**
     * Called when user clicks on stop thermal camera button.
     * Stops camera to reccord stream.
     */
    onStopThermalButtonClick(){
        this.stopThermalCamera();
    }

    /**
     * Called when user clicks on gray view for thermal button.
     * Display thermal stream in gray color.
     */
    onThermalGrayButtonClick(){
        this.setThermalCamera({
            format: 0
        });
    }

    /**
     * Called when user clicks on thermal jet view for thermal button.
     * Display thermal stream in jet color.
     */
    onThermalJetButtonClick(){
        this.setThermalCamera({
            format: 1
        });
    }

    /**
     * Called when user clicks on low quality for thermal button.
     * Display thermal stream in low quality.
     */
    onThermalQualityLowButtonClick(){
        this.setThermalCamera({
            quality: 0
        });
    }

    /**
     * Called when user clicks on medium quality for thermal button.
     * Display thermal stream in medium quality.
     */
    onThermalQualityMediumButtonClick(){
        this.setThermalCamera({
            quality: 1
        });
    }

    /**
     * Called when user clicks on high quality for thermal button.
     * Display thermal stream in high quality.
     */
    onThermalQualityHighButtonClick(){
        this.setThermalCamera({
            quality: 2
        });
    }

    /**
     * Called when user clicks on camera button.
     * Displays drawer to select camera.
     */
    onCamerasButtonClick(){
        this.toggleCamerasDrawer()
    }

    /**
     * Requests server to start thermal camera.
     */
    startThermalCamera(){
        var context = this;
        RequestManager.get('api/cameras/thermal/start', {type: 'json'})
            .then(res => {
                context.thermal_camera = res;
                context.updateUI();
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_CAMERA_START'));
            });            
    }

    /**
     * Requests server to stop thermal camera.
     */
    stopThermalCamera(){            
        var context = this;
        RequestManager.get('api/cameras/thermal/stop', {type: 'json'})
            .then(res => {
                context.thermal_camera = res;
                context.updateUI();
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_CAMERA_STOP'));
            });
    }

    /**
     * Sets thermal camera parameters.
     * 
     * @param {Object} iParams - Camera parameters.
     */
    setThermalCamera(iParams){
        var context = this;
        RequestManager.post(
            'api/cameras/thermal', 
            {
                headers: {
                    'Content-Type': 'application/json'
                },
                data: iParams,
                type: 'json'
            }
        )
            .then(res => {
                context.thermal_camera = res;
                context.updateUI();
            })
            .catch(err => {
            });
    }

    /**
     * Method called when received a thermal camera update.
     * This is a buffer for an image.
     * 
     * @param {Object} iMessage - Thermal camera message.
     */
    onThermalUpdate(iMessage){
        var blob = new Blob([iMessage], {type: "image/jpeg"});
        var url = (URL || webkitURL).createObjectURL(blob);
    
        this.thermal_image.src = url;
    }

    /**
     * Updates UI for selected camera.
     */
    updateUI(){
        this.camera_qx30_button.classList.remove('selected');
        this.camera_boson_button.classList.remove('selected');
        this.camera_qx30_container.setAttribute('hidden', 'true');
        this.camera_boson_container.setAttribute('hidden', 'true');
        this.camera_qx30_actions_toolbar.setAttribute('hidden', 'true');
        this.camera_boson_actions_toolbar.setAttribute('hidden', 'true');

        if(this.current_camera === CAMERA_TYPES.QX30){
            this.camera_qx30_button.classList.add('selected');
            this.camera_qx30_container.removeAttribute('hidden');
            this.camera_qx30_actions_toolbar.removeAttribute('hidden');
            this.setTitle('SONY QX30');
            this.updateQX30UI();
        }
        if(this.current_camera === CAMERA_TYPES.BOSON){
            this.camera_boson_button.classList.add('selected');
            this.camera_boson_container.removeAttribute('hidden');
            this.camera_boson_actions_toolbar.removeAttribute('hidden');
            this.setTitle('FLIR BOSON');
            this.updateThermalUI();
        }
    }

    /**
     * Updates QX30 camera view.
     */
    updateQX30UI(){
        
    }

    /**
     * Updates thermal camera view.
     */
    updateThermalUI(){
        this.start_thermal_button.setAttribute('hidden', 'true');
        this.stop_thermal_button.setAttribute('hidden', 'true');
        this.thermal_image.setAttribute('hidden', 'true');

        if(this.thermal_camera.active){
            this.stop_thermal_button.removeAttribute('hidden', 'true');
            this.thermal_image.removeAttribute('hidden', 'true');
        }
        else{
            this.start_thermal_button.removeAttribute('hidden', 'true');
        }
        
        this.thermal_gray_button.classList.remove('mdc-list-item--activated');
        this.thermal_jet_button.classList.remove('mdc-list-item--activated');
        if(this.thermal_camera.format === 0){
            this.thermal_gray_button.classList.add('mdc-list-item--activated');
        }
        else if(this.thermal_camera.format === 1){
            this.thermal_jet_button.classList.add('mdc-list-item--activated');
        }

        this.thermal_quality_low_button.classList.remove('mdc-list-item--activated');
        this.thermal_quality_medium_button.classList.remove('mdc-list-item--activated');
        this.thermal_quality_high_button.classList.remove('mdc-list-item--activated');
        if(this.thermal_camera.quality === 0){
            this.thermal_quality_low_button.classList.add('mdc-list-item--activated');
        }
        else if(this.thermal_camera.quality === 1){
            this.thermal_quality_medium_button.classList.add('mdc-list-item--activated');
        }
        else if(this.thermal_camera.quality === 2){
            this.thermal_quality_high_button.classList.add('mdc-list-item--activated');
        }
    }

    /**
     * Closes cameras drawer.
     */
    closeCamerasDrawer(){
        this.right_drawer_cameras.classList.remove('active');
        this.updateLayout();
    }
    /**
     * Opens cameras drawer.
     */
    openCamerasDrawer(){
        this.right_drawer_settings.classList.remove('active');
        this.right_drawer_cameras.classList.add('active');
        this.updateLayout();
    }
    /**
     * Toggle cameras drawer visibility.
     */
    toggleCamerasDrawer(){
        this.right_drawer_settings.classList.remove('active');
        this.right_drawer_cameras.classList.toggle('active');
        this.updateLayout();
    }
    /**
     * Closes settings drawer.
     */
    closeSettingsDrawer(){
        this.right_drawer_settings.classList.remove('active');
        this.updateLayout();
    }
    /**
     * Opens settings drawer.
     */
    openSettingsDrawer(){
        this.right_drawer_cameras.classList.remove('active');
        this.right_drawer_settings.classList.add('active');
        this.updateLayout();
    }
    /**
     * Toggle settings drawer visibility.
     */
    toggleSettingsDrawer(){
        this.right_drawer_cameras.classList.remove('active');
        this.right_drawer_settings.classList.toggle('active');
        this.updateLayout();
    }

    /**
     * Updates page layout.
     */
    updateLayout(){
        if(
            this.right_drawer_cameras.classList.contains('active') ||
            this.right_drawer_settings.classList.contains('active')
        ){
            this.center_container.classList.add('right-collapsed');
        }
        else{
            this.center_container.classList.remove('right-collapsed');
        }
    }
};

export default CamerasPage;