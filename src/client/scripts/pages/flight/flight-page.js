/**
 * Module of Ansel's flight page.
 * 
 * @requires module:pages/page
 * @requires module:components/map/map
 * @requires module:components/modal
 * @requires module:models/flight
 * @requires module:managers/message-manager
 * @requires module:managers/settings-manager
 * @requires module:utils/utils
 * 
 * @module pages/flight/flight-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import Map from 'components/map/map';
import Modal from 'components/modal';
import Flight from 'models/flight';
import MessageManager from 'managers/message-manager';
import SettingsManager from 'managers/settings-manager';
import Utils from 'utils/utils';

import html from './flight-page.html';
import css from './flight-page.css';

/**
 * Ansel's flight page.
 * This page shows a given flight on a map.
 * 
 * @extends module:pages/page~Page
 */
class FlightPage extends Page{
    /**
     * Creates an instance of FlightPage.
     */
    constructor(){
        super();
        this.name = 'flight';
        
        /**
         * Map displayed.
         * @private
         * @member {module:components/map/map~Map}
         * */
        this.map = null;
        /**
         * Currently displayed flight.
         * @private
         * @member {module:models/flight~Flight}
         * */
        this.current_flight = null;

        /**
         * Map view options.
         * @private
         * @property {boolean} drone True to display the drone.
         * @property {Object} flight Flight view options.
         * @property {boolean} flight.markers True to display GPS markers.
         * @property {boolean} flight.altitude True to place objects according to altitude (false to project on ground).
         * @property {number} flight.view_type Image view type to display.
         * @property {number} flight.image_type Image type to display.
         * @member {Object}
         * */
        this.view_options = {
            drone: false,
            flight: {
                markers: true,
                altitude: true,
                view_type: 0,
                image_type: 0,
            }
        };
    }

    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);

        this.download_button = this.getContainer().querySelector('.as-flight-page-actions-download-button');
        this.delete_button = this.getContainer().querySelector('.as-flight-page-actions-delete-button');
        this.altitude_button = this.getContainer().querySelector('.as-flight-page-actions-altitude-button');
        this.import_layers_button = this.getContainer().querySelector('.as-flight-page-actions-layers-button');
        this.search_button = this.getContainer().querySelector('.as-flight-page-actions-search-button');
        this.map_button = this.getContainer().querySelector('.as-flight-page-actions-map-button');       
                    
        this.more_button = this.getContainer().querySelector('.as-flight-page-actions-more-button');
        this.views_button = this.getContainer().querySelector('.as-flight-page-actions-views-button');

        this.color_button = this.getContainer().querySelector('.as-image-type-color-button');
        this.thermal1_button = this.getContainer().querySelector('.as-image-type-thermal1-button');
        this.thermal2_button = this.getContainer().querySelector('.as-image-type-thermal2-button');  
        this.markers_button = this.getContainer().querySelector('.as-map-page-actions-view-markers-button');

        this.view_default_button = this.getContainer().querySelector('.as-flight-page-actions-view-default-button');
        this.view_thumbnail_button = this.getContainer().querySelector('.as-flight-page-actions-view-thumbnail-button');
        
        this.download_button.onclick = this.onDownloadButtonClick.bind(this);
        this.delete_button.onclick = this.onDeleteButtonClick.bind(this);
        this.altitude_button.onclick = this.onAltitudeButtonClick.bind(this);
        this.import_layers_button.onclick = this.onImportLayerButtonClick.bind(this);
        this.search_button.onclick = this.onSearchButtonClick.bind(this);
        this.map_button.onclick = this.onMapButtonClick.bind(this);

        this.more_button.onclick = this.onMoreButtonClick.bind(this);
        this.views_button.onclick = this.onViewsButtonClick.bind(this);
        this.markers_button.onclick = this.onMarkersButtonClick.bind(this);

        this.color_button.onclick = this.onColorButtonClick.bind(this);
        this.thermal1_button.onclick = this.onThermal1ButtonClick.bind(this);
        this.thermal2_button.onclick = this.onThermal2ButtonClick.bind(this);

        this.view_default_button.onclick = this.onViewDefaultButtonClick.bind(this);
        this.view_thumbnail_button.onclick = this.onViewThumbnailButtonClick.bind(this);
        
        this.initMap();
    }

    /**
     * Initializes the map component of the page.
     * 
     */
    initMap(){
        var context = this;

        this.map = new Map(this.getContainer().querySelector('.as-flight-map-container'), {
            mode: Cesium.SceneMode.SCENE3D,
            events: {
                onImageClick(iImage){
                    context.navigate('image', {
                        flight: context.current_flight, 
                        image: iImage
                    });
                }
            },
            view: this.view_options
        });

        SettingsManager.getSettings()
            .then(function(iSettings){
                var mode = Cesium.SceneMode.SCENE3D;
                if(iSettings.app.mode === 0) {
                    mode = Cesium.SceneMode.SCENE2D;
                }

                context.map.setMode(mode);
            });
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @param {string} iFlightId - Identifier of the flight to display.
     * @override
     */
    onShow(iFlightId){
        var context = this;

        this.map.load()
            .then(function(){
                context.map.setFlight(context.current_flight);
                context.map.refresh();
                context.map.show();  
                context.map.goToFlight();              
            })
            .catch(function(){                
            }); 
        this.loadFlight(iFlightId);  
    }

    /**
     * Method called when the page is being hidden.
     * 
     * @override
     */
    onHide(){
        this.map.hide();
    }

    /**
     * Loads a flight in the map.
     * 
     * @param {string} iFlightId - Identifier of the flight to load.
     */
    loadFlight(iFlightId){
        var context = this;
        if(!iFlightId){ return; }
        
        this.setCurrentFlight(null);

        Utils.getApp().enableSpinner(true);
        var flight = new Flight(iFlightId); 

        flight.load(function(){
            context.setCurrentFlight(flight);
        })
            .then(() => {
                context.setCurrentFlight(flight);
                Utils.getApp().enableSpinner(false);
            })
            .catch((err) => {
                MessageManager.showError(__("MSG_ERROR_FLIGHT_LOADING"));
                context.setCurrentFlight(null);
                Utils.getApp().enableSpinner(false);
            });
    }

    /**
     * Called when user clicks on more button.
     * This displays additional commands in drop-down menu.
     */
    onMoreButtonClick(){
        var menu_elt = this.getContainer().querySelector('.as-flight-page-menu');
        var menu = new mdc.menu.MDCMenu(menu_elt);
        menu.open = !menu.open;
    }

    /**
     * Called when user clicks on views button.
     * This display view commands in drop-down menu.
     */
    onViewsButtonClick(){
        var menu_elt = this.getContainer().querySelector('.as-flight-page-menu-views');
        var menu = new mdc.menu.MDCMenu(menu_elt);
        menu.open = !menu.open;
    }

    /**
     * Called when user clicks on map button.
     * This toggles the map to 2D and 3D.
     */
    onMapButtonClick(){
       this.map.toggleMode();
       this.map.goToFlight();
    }

    /**
     * Called when user clicks on download flight button.
     * This downloads all the images of the flight in a zip archive.
     */
    onDownloadButtonClick(){
        var context = this;
        if(!this.current_flight) { return; }
        Utils.getApp().enableSpinner(true);

        this.current_flight.download(
            function(){
                Utils.getApp().enableSpinner(false);
            },
            function(iError){
                Utils.getApp().enableSpinner(false);
            },
            function(iProgress){
                Utils.getApp().setSpinnerProgress(iProgress);
            }
        );
    }

    /**
     * Called when user clicks on deletes button.
     * Deletes the flight from the drone.
     * Displays a warning popup to the user first.
     */
    onDeleteButtonClick(){
        var context = this;
        if(!this.current_flight) { return; }

        new Modal({
            title: __('MSG_WARNING_FLIGHT_DELETE_TITLE'),
            html: `${__('MSG_WARNING_FLIGHT_DELETE_1')}<br/>${__('MSG_WARNING_FLIGHT_DELETE_2')}`,
            actions: [
                {
                    title: __('CANCEL')
                },
                {
                    accept: true,
                    title: __('VALIDATE'),
                    callback: function(){
                        context.enableSpinner(true);
            
                        context.current_flight.delete()
                            .then(flights => {
                                context.flight_list = [];
                                context.current_flight = null;
                                context.setCurrentFlight(null);
                                Utils.getApp().navigate('drone');
                                Utils.getApp().load();
                            })
                            .catch(err => {
                                context.enableSpinner(false);
                            }) 
                    }
                }
            ]
        }).show();                     
    }

    /**
     * Called when user clicks on toggle altitude button.
     * If altitude is enabled, markers are positionned relative to their altitude.
     * If altitude is not enabled, markers are projected on ground.
     */
    onAltitudeButtonClick(){
        this.view_options.flight.altitude = !this.view_options.flight.altitude;
        this.map.setViewOptions(this.view_options);
        this.map.refresh();
        this.refreshUI();
    }

    /**
     * Called when user clicks on import KML button.
     * Open file chooser and add selected KML to map.
     */
    onImportLayerButtonClick(){
        var context = this;
        Utils.openFileChooser()
            .then(function(files){
                if(files.length < 1){ return; }
                context.map.importLayer(files[0], files[0].name);
            });
    }

    /**
     * Called when user clicks on search button.
     * Parses the image to find markers.
     */
    onSearchButtonClick(){
        var context = this;
        if(!this.current_flight) { return; }
        Utils.getApp().enableSpinner(true);

        this.current_flight.search(
            function(){
                Utils.getApp().enableSpinner(false);
            },
            function(iError){
                Utils.getApp().enableSpinner(false);
            },
            function(iProgress){
                Utils.getApp().setSpinnerProgress(iProgress);
            }
        );
    }

    /**
     * Called when user clicks on default view button.
     * Default view displays images as icons.
     */
    onViewDefaultButtonClick(){
        this.setMapViewType(0);
    }
    /**
     * Called when user clicks on thumbnail view button.
     * Thumbnail view displays thumbnail as icons.
     */
    onViewThumbnailButtonClick(){
        this.setMapViewType(1);
    }

    /**
     * Called when user clicks on color button.
     * Displays colors images of the flight.
     */
    onColorButtonClick(){
        this.setImageType(0);
    }
    /**
     * Called when user clicks on thermal B&W button.
     * Displays thermal B&W images of the flight.
     */
    onThermal1ButtonClick(){
        this.setImageType(1);
    }
    /**
     * Called when user clicks on thermal Jet button.
     * Displays thermal Jet images of the flight.
     */
    onThermal2ButtonClick(){
        this.setImageType(2);
    }

    /**
     * Called when user clicks on toggle marker button.
     * Displays colors images of the flight.
     */
    onMarkersButtonClick(){
        this.view_options.flight.markers = !this.view_options.flight.markers;
        this.map.setViewOptions(this.view_options);
        this.map.refresh();
        this.refreshUI();            
    }

    /**
     * Sets a given map view type.
     * 
     * @param {number} iType - Map view type
     */
    setMapViewType(iType){
        if(!this.current_flight){ return; }

        this.view_options.flight.view_type = iType;
        this.map.setViewOptions(this.view_options);
        this.map.refresh();

        this.refreshUI();
    }

    /**
     * Sets a given image type to display.
     * 
     * @param {number} iType - Image type to display.
     */
    setImageType(iType){
        if(!this.current_flight){ return; }

        if(!this.current_flight.getImages(iType) || !this.current_flight.getImages(iType).length > 0){ 
            return;
        }

        this.view_options.flight.image_type = iType;
        this.map.setViewOptions(this.view_options);
        this.map.refresh();

        this.refreshUI();
    }

    /**
     * Displays a given loaded flight.
     * 
     * @param {Flight} iFlight - Flight to display.
     */
    setCurrentFlight(iFlight){
        this.current_flight = iFlight;
        this.view_options.flight.image_type = 0;
        this.map.setFlight(iFlight);
        this.map.refresh();
        this.map.goToFlight();

        if(this.current_flight){
            this.setTitle(this.current_flight.getTitle());
        }
        
        this.refreshUI();
    }

    /**
     * Refresh user interface elements.
     */
    refreshUI(){
        if(!this.current_flight){
            return;
        }
        var drawer_flights_content_elt = this.getContainer().querySelector('.as-flight-list-container');

        if(this.current_flight.getImages(0) && this.current_flight.getImages(0).length > 0){ 
            this.color_button.classList.remove('disabled');
        }
        else{
            this.color_button.classList.add('disabled');
        }       
        
        if(this.current_flight.getImages(1) && this.current_flight.getImages(1).length > 0){
            this.thermal1_button.classList.remove('disabled');
        }
        else{
            this.thermal1_button.classList.add('disabled');
        }       
        
        if(this.current_flight.getImages(2) && this.current_flight.getImages(2).length > 0){
            this.thermal2_button.classList.remove('disabled');
        }
        else{
            this.thermal2_button.classList.add('disabled');
        }

        // Image types buttons
        var image_type = this.view_options.flight.image_type;
        this.color_button.classList.remove('mdc-list-item--activated');
        this.thermal1_button.classList.remove('mdc-list-item--activated');
        this.thermal2_button.classList.remove('mdc-list-item--activated');

        if(image_type === 0){
            this.color_button.classList.add('mdc-list-item--activated');
        }
        else if(image_type === 1){
            this.thermal1_button.classList.add('mdc-list-item--activated');
        }
        else if(image_type === 2){
            this.thermal2_button.classList.add('mdc-list-item--activated');
        }


        // View types buttons
        var view_type = this.view_options.flight.view_type;
        this.view_default_button.classList.remove('mdc-list-item--activated');
        this.view_thumbnail_button.classList.remove('mdc-list-item--activated');

        if(view_type === 0){
            this.view_default_button.classList.add('mdc-list-item--activated');
        }
        else if(view_type === 1){
            this.view_thumbnail_button.classList.add('mdc-list-item--activated');
        }

        // Altitude toggle button
        if(this.view_options.flight.altitude){
            this.altitude_button.classList.add('mdc-list-item--activated');
        }
        else {
            this.altitude_button.classList.remove('mdc-list-item--activated');
        }

        // Markers toggle button
        if(this.view_options.flight.markers){
            this.markers_button.classList.add('mdc-list-item--activated');
        }
        else {
            this.markers_button.classList.remove('mdc-list-item--activated');
        }
    }
};

export default FlightPage;
