/**
 * Module of Ansel's command page.
 * 
 * @requires module:pages/page
 * @requires module:managers/request-manager
 * @requires module:components/element
 * @requires module:models/job
 * 
 * @module pages/command/command-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import RequestManager from 'managers/request-manager';
import Element from 'components/element';
import Job from 'models/job';

import html from './command-page.html';
import css from './command-page.css';

/**
 * Ansel's command page (Need developer access).
 * Page to execute UNIX commands on drone.
 * 
 * @extends module:pages/page~Page
 */
class CommandPage extends Page{
    /**
     * Creates an instance of CommandPage.
     */
    constructor(){
        super();
        this.name = 'command';

        /**
         * List of commands.
         * @private
         * @member {Array.<string>} 
         * */
        this.command_list = [];
        /**
         * Indicate if waiting a command's response.
         * @private
         * @member {boolean} 
         * */
        this.is_waiting = false;

        /**
         * Command history.
         * @private
         * @member {Array.<string>} 
         * */
        this.command_history_list = [];
        /**
         * Current indice of command history.
         * @private
         * @member {number} 
         * */
        this.command_history_ind = 0;
    }

    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);

        var input = this.getCommandInputElt();
        input.onkeyup = this.onKeyUp.bind(this);
    }

    /**
     * Called when user raise key up event from keyboard.
     * - ENTER: valid current message.
     * - UP: go up in history.
     * - DOWN: go down in history.
     * 
     * @param {Object} iEvent Key up event.
     */
    onKeyUp(iEvent){
        var input = this.getCommandInputElt();

        // ENTER
        if (iEvent.keyCode == 13) {
            this.onTextValidate();
        }
        // UP
        else if (iEvent.keyCode == 38) {
            if(this.command_history_ind - 1 >= 0 && this.command_history_ind - 1 < this.command_history_list.length){
                this.command_history_ind = this.command_history_ind - 1;
                input.value = this.command_history_list[this.command_history_ind];
                input.focus();
            }
        }
        // DOWN
        else if (iEvent.keyCode == 40) {
            if(this.command_history_ind + 1 >= 0 && this.command_history_ind + 1 < this.command_history_list.length){
                this.command_history_ind = this.command_history_ind + 1;
                input.value = this.command_history_list[this.command_history_ind];
                input.focus();
            }
            else{
                input.value = '';
                input.focus();
            }
        }
    }

    /**
     * Gets command input element.
     * 
     * @returns {HTMLElement} Command input element.
     */
    getCommandInputElt(){
        return this.getContainer().querySelector('.as-command-prompt-input');
    }
    /**
     * Gets command input container element.
     * 
     * @returns {HTMLElement} Command input container element.
     */
    getCommandInputContainerElt(){
        return this.getContainer().querySelector('.as-command-prompt-container');
    }
    /**
     * Gets prompt element.
     * 
     * @returns {HTMLElement} Prompt element.
     */
    getPrompt(){
        return this.getContainer().querySelector('.as-command-viewer-container');
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @override
     */
    onShow(){
        this.getCommandInputElt().focus();
    }

    /**
     * Add a command line entry to the console.
     * 
     * @param {string} iEntry - Command line entry.
     */
    addConsoleCommandEntry(iEntry){
        if(!iEntry || iEntry.length === 0){ return; }
        var prompt = this.getPrompt();

        let currElement = new Element('div', {
            class: 'as-command-viewer-line command',
            html: iEntry.replace(/(\r\n|\n|\r)/g,"<br />")
        }).append(prompt);
    }

    /**
     * Add a response line entry to the console.
     * 
     * @param {string} iEntry - Response line entry.
     */
    addConsoleEntry(iEntry){
        if(!iEntry || iEntry.length === 0){ return; }
        var prompt = this.getPrompt();

        let currElement = new Element('div', {
            class: 'as-command-viewer-line',
            html: iEntry.replace(/(\r\n|\n|\r)/g,"<br />")
        }).append(prompt);
    }

    /**
     * Add an error line entry to the console.
     * 
     * @param {string} iEntry - Error line entry.
     */
    addConsoleErrorEntry(iEntry){
        if(!iEntry || iEntry.length === 0){ return; }
        var prompt = this.getPrompt();

        let currElement = new Element('div', {
            class: 'as-command-viewer-line error',
            html: iEntry.replace(/(\r\n|\n|\r)/g,"<br />")
        }).append(prompt);
    }

    /**
     * Add list of entries to the console.
     * 
     * @param {Array} iEntryList - List of entries.
     */
    addConsoleEntryList(iEntryList){
        for(let i = 0; i < iEntryList.length; i++){
            let curr_code = iEntryList[i][0];
            let curr_text = iEntryList[i][1];
            if(curr_code === 0){
                this.addConsoleEntry(curr_text);
            }
            else{
                this.addConsoleErrorEntry(curr_text);
            }
        }
    }

    /**
     * Called when user validates his input.
     */
    onTextValidate(){
        var input = this.getCommandInputElt();
        if(input.value.length === 0){ return; }
        this.command_list.push(input.value);

        if(this.command_history_list[this.command_history_list.length - 1] !== input.value){
            this.command_history_list.push(input.value);
            this.command_history_ind = this.command_history_list.length;
        }

        input.value = '';

        if(!this.is_waiting){
            this.launchNextCommand();
        }
    }

    /**
     * Launches a command typed by the user.
     */
    launchNextCommand(){
        var context = this;
        var input = this.getCommandInputElt();
        var container = this.getCommandInputContainerElt();
        var prompt = this.getPrompt();

        if(this.command_list.length === 0){
            this.is_waiting = false;
            container.classList.remove("waiting");
            input.focus();

            var container = this.getContainer().querySelector(".as-command-container-inner");
            container.scrollTop = container.scrollHeight;
            return;
        }
        var cmd = this.command_list[0];
        this.command_list.splice(0, 1);

        this.is_waiting = true;
        container.classList.add("waiting");

        if(cmd.trim() === 'clear'){
            prompt.innerHTML = '';
            context.launchNextCommand();
            return;
        }
        this.addConsoleCommandEntry(cmd);

        var job = new Job('commands', {
            cmd: cmd,
            cwd: this.cwd
        });

        job.start(
            function(iJob){
                context.addConsoleEntryList(iJob.result.data);
                context.launchNextCommand();
            },
            function(iJob){
                context.addConsoleEntryList(iJob.result.data);
                context.launchNextCommand();
            },
            function(iJob){
                context.addConsoleEntryList(iJob.result.data);
            }
        );
    }
};

export default CommandPage;