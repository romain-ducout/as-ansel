/**
 * Module of Ansel's drone page.
 * 
 * @requires module:pages/page
 * @requires module:managers/settings-manager
 * @requires module:components/element
 * @requires module:components/map/map
 * @requires module:models/drone
 * @requires module:utils/constants
 * @requires module:utils/utils
 * 
 * @module pages/drone/drone-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import SettingsManager from 'managers/settings-manager';
import Element from 'components/element';
import Map from 'components/map/map';
import Drone from 'models/drone';
import Constants from 'utils/constants';
import Utils from 'utils/utils';

import html from './drone-page.html';
import css from './drone-page.css';

/**
 * Ansel's drone page.
 * This page shows the status of the drone and its position on a map.
 * 
 * @extends module:pages/page~Page
 */
class DronePage extends Page{
    /**
     * Creates an instance of DronePage.
     */
    constructor(){
        super();
        this.name = 'drone';

        /**
         * Indicates if graph has been initialized.
         * @private
         * @member {boolean}
         * */
        this.graph_init = false;

        /**
         * Map displayed.
         * @private
         * @member {module:components/map/map~Map}
         * */
        this.map = null;
        /**
         * Indicates if page has to be refreshed.
         * @private
         * @member {boolean}
         * */
        this.animating = false;
        /**
         * Latest image taken by drone.
         * @private
         * @member {string}
         * */
        this.latest_image = null;
    }    
    
    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);

        this.image_button = this.getContainer().querySelector('.as-drone-page-actions-image-button');
        this.info_button = this.getContainer().querySelector('.as-drone-page-actions-info-button');
        this.clear_images_button = this.getContainer().querySelector('.as-drone-page-actions-clear-images-button');
        this.graph_button = this.getContainer().querySelector('.as-drone-page-actions-graph-button');
        this.map_button = this.getContainer().querySelector('.as-drone-page-actions-map-button');
        this.import_layers_button = this.getContainer().querySelector('.as-drone-page-actions-layers-button');

        this.drone_info_drawer = this.getContainer().querySelector('.as-right-drawer-info');
        this.drone_image_drawer = this.getContainer().querySelector('.as-right-drawer-image');
        this.drone_graph_drawer = this.getContainer().querySelector('.as-bottom-drawer');

        this.drone_images_container = this.getContainer().querySelector('.as-drone-images-container');
        this.drone_graph_container = this.getContainer().querySelector('.as-drone-graph-container');
        
        this.drone_info_model = this.getContainer().querySelector('.as-drone-info-model');
        this.drone_info_armed = this.getContainer().querySelector('.as-drone-info-armed');
        this.drone_info_position = this.getContainer().querySelector('.as-drone-info-position');
        this.drone_info_latitude = this.getContainer().querySelector('.as-drone-info-latitude');
        this.drone_info_longitude = this.getContainer().querySelector('.as-drone-info-longitude');
        this.drone_info_altitude = this.getContainer().querySelector('.as-drone-info-altitude');
        this.drone_info_camera_state = this.getContainer().querySelector('.as-drone-info-camera-state');
        this.drone_info_camera_image_count = this.getContainer().querySelector('.as-drone-info-camera-image-count');

        this.image_button.onclick = this.onImageButtonClick.bind(this);
        this.info_button.onclick = this.onInfoButtonClick.bind(this);
        this.clear_images_button.onclick = this.onClearImagesButtonClick.bind(this);
        this.graph_button.onclick = this.onGraphButtonClick.bind(this);
        this.map_button.onclick = this.onMapButtonClick.bind(this);
        this.import_layers_button.onclick = this.onImportLayerButtonClick.bind(this);

        Drone.connect(this.onDroneChange.bind(this));
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @override
     */
    onShow(){
        var context = this;
        this.animate();
        this.onDroneChange();
        this.showCurrentMap();
    }

    /**
     * Displays the currently selected map.
     */
    showCurrentMap(){
        var context = this;

        if(!this.map){
            SettingsManager.getSettings()
                .then(function(iSettings){
                    var mode = Cesium.SceneMode.SCENE3D;
                    if(iSettings.app.mode === 0) {
                        mode = Cesium.SceneMode.SCENE2D;
                    }

                    context.map =  new Map(context.getContainer().querySelector('.as-drone-map-container'), {
                        mode: mode,
                        view: {
                            drone: true
                        }
                    });
                    context.showCurrentMap();
                });
            return;
        }

        this.map.load()
            .then(function(){
                context.map.goToDrone(); 
                context.map.show();               
            })
            .catch(function(){                
            }); 
    }

    /**
     * Method called when the page is being hidden.
     * 
     * @override
     */
    onHide(){
        this.animating = false;
        this.map.hide();
    }

    /**
     * Called when user clicks on image button.
     * Displays drawer that displays imagestaken by drone.
     */
    onImageButtonClick(){
        this.drone_info_drawer.classList.remove('active');            
        this.drone_image_drawer.classList.toggle('active');
    }

    /**
     * Called when user clicks on info button.
     * Display drone information drawer.
     */
    onInfoButtonClick(){
        this.drone_image_drawer.classList.remove('active');
        this.drone_info_drawer.classList.toggle('active');
    }

    /**
     * Called when user clicks on graph button.
     * Display graph drawer.
     */
    onGraphButtonClick(){
        this.drone_graph_drawer.classList.toggle('active');
        this.initGraph();
    }

    /**
     * Called when user clicks on map button.
     * Toggle map view 2D / 3D.
     */
    onMapButtonClick(){
        this.map.toggleMode();
    }

    /**
     * Called when user clicks on clear images button.
     * Clear the list of images displayed in image drawer.
     */
    onClearImagesButtonClick(){
        this.drone_images_container.innerHTML = '';
    }

    /**
     * Called when user clicks on import KML button.
     * Open file chooser and add selected KML to map.
     */
    onImportLayerButtonClick(){
        var context = this;
        Utils.openFileChooser()
            .then(function(files){
                if(files.length < 1){ return; }
                context.map.importLayer(files[0], files[0].name);
            });
    }

    /**
     * Called every 50ms while page is displayed.
     * Used to refresh the position of the drone on the map.
     */
    animate(){
        var context = this;
        this.animating = true;

        var animate = function(){
            if(!context.animating){
                return;
            }
            
            if(context.map){
                context.map.refresh();
            }

            setTimeout(() => {
                animate();
            }, 50);
        }

        animate();
    }

    /**
     * Called each time the drone's status changed.
     */
    onDroneChange(){
        if(!this.shown) { return; }
        this.updateInfoView();
        this.updateImageView();
        this.updateGraphView();

        if(Drone.data){
            this.setTitle(Drone.data.model);
        }
    }

    /**
     * Updates information drawer view.
     */
    updateInfoView(){
        var drone_data = Drone.getData();

        this.drone_info_model.innerHTML = __('UNAVALAIBLE');
        this.drone_info_armed.innerHTML = __('UNAVALAIBLE');
        this.drone_info_position.innerHTML = __('UNAVALAIBLE');
        this.drone_info_latitude.innerHTML = __('UNAVALAIBLE');
        this.drone_info_longitude.innerHTML = __('UNAVALAIBLE');
        this.drone_info_altitude.innerHTML = __('UNAVALAIBLE');
        this.drone_info_camera_state.innerHTML = __('UNAVALAIBLE');
        this.drone_info_camera_image_count.innerHTML = __('UNAVALAIBLE');

        if(!drone_data) { return; }

        this.drone_info_model.innerHTML = drone_data.model;
        this.drone_info_armed.innerHTML = drone_data.armed;

        if(drone_data.mission){
            this.drone_info_position.innerHTML = `${drone_data.mission.current} / ${drone_data.mission.total}`;
        }
        if(drone_data.gps){
            this.drone_info_latitude.innerHTML = drone_data.gps.latitude;
            this.drone_info_longitude.innerHTML = drone_data.gps.longitude;
            this.drone_info_altitude.innerHTML = `${drone_data.gps.altitude}m`;
        }
        if(drone_data.camera){
            this.drone_info_camera_state.innerHTML = drone_data.camera.camera_state;
            this.drone_info_camera_image_count.innerHTML = drone_data.camera.count;
        }
    }
    /**
     * Updates image drawer view.
     */
    updateImageView(){ 
        var drone_data = Drone.getData();
        if(!drone_data){ return; }
        if(!drone_data.flight || !drone_data.flight.file || !drone_data.flight.name){ return; }
        if(drone_data.flight.file === this.latest_image){ return; }           

        var image_url = `/api/flights/${drone_data.flight.name}/images/${drone_data.flight.file}/image?width=270`;
        var image_template = `
            <img class="as-image-menu-item mdc-elevation--z5" src="${image_url}"></img>
        `;
        let currElement = new Element('img', {
            class: 'as-image-menu-item mdc-elevation--z5',
            attributes: {
                src: image_url
            }
        }).insert(this.drone_images_container, 0);

        if(this.drone_images_container.children.length > 20){
            var last_children = this.drone_images_container.children[this.drone_images_container.children.length - 1];
            this.drone_images_container.removeChild(last_children);
        }
        
        this.latest_image = drone_data.flight.file;                
    }

    /**
     * Initializes graph view.
     */
    initGraph(){
        if(this.graph_init) { return; }
        var context = this;
        Utils.getPlotlyPromise()
            .then(function(){
                var data = [
                    {y: [], x: [], mode: 'lines', name: 'latitude'},
                    {y: [], x: [], mode: 'lines', name: 'longitude'},
                    {y: [], x: [], mode: 'lines', name: 'altitude'},
                    {y: [], x: [], mode: 'lines', name: 'heading'}
                ];
                Plotly.plot(context.drone_graph_container, data, {
                    height: 300,
                    margin:{
                        autoexpand: true,
                        l: 70, r: 30,
                        t: 10, b: 50
                    }
                });
                context.graph_init = true;
            });
    }

    /**
     * Updates graph drawer view.
     */
    updateGraphView(){
        var drone_data = Drone.getData();
        if(!drone_data) { return; }
        if(!drone_data.gps) { return; }
        if(!this.graph_init) { return; }

        Plotly.Plots.resize(this.drone_graph_container);

        var time = new Date().time;

        var data_y = [
            [drone_data.gps.latitude],
            [drone_data.gps.longitude],
            [drone_data.gps.altitude],
            [drone_data.gps.direction]
        ];
        var data_x = [[time],[time],[time],[time]];
        var update = {
            x: data_x,
            y: data_y
        }

        Plotly.extendTraces(this.drone_graph_container, update, [0,1,2,3]);
    }
};

export default DronePage;