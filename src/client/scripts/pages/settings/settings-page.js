/**
 * Module of Ansel's settings page.
 * 
 * @requires module:pages/page
 * @requires module:managers/request-manager
 * @requires module:managers/message-manager
 * @requires module:managers/settings-manager
 * @requires module:utils/utils
 * @requires module:utils/constants
 * 
 * @module pages/settings/settings-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import RequestManager from 'managers/request-manager';
import MessageManager from 'managers/message-manager';
import SettingsManager from 'managers/settings-manager';
import Utils from 'utils/utils';
import Constants from 'utils/constants';

import html from './settings-page.html';
import css from './settings-page.css';

/**
 * Ansel's settings page.
 * This page allows user to manage the application's settings.
 * 
 * @extends module:pages/page~Page
 */
class SettingsPage extends Page{
    /**
     * Creates an instance of SettingsPage.
     */
    constructor(){
        super();
        this.name = 'settings';

        /**
         * Current settings value. 
         * @private
         * @member {Object} 
         * */
        this.settings = null;
    }

    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);
        this.refresh_button = this.getContainer().querySelector('.as-settings-page-actions-refresh-button');

        this.language_select_field = new mdc.select.MDCSelect(this.container.querySelector('.as-language-setting-field'));
        this.mode_select_field = new mdc.select.MDCSelect(this.container.querySelector('.as-mode-setting-field'));
        
        this.connect_aerobo_button = this.container.querySelector('.as-settings-page-actions-apply-aerobo-button');
        this.apply_wifi_button = this.container.querySelector('.as-settings-page-actions-apply-wifi-button');

        this.wifi_ssid_field = new mdc.textField.MDCTextField(this.container.querySelector('.as-wifi-ssid-setting-field'));
        this.wifi_wpa_passphrase_field = new mdc.textField.MDCTextField(this.container.querySelector('.as-wifi-wpa-passphrase-setting-field'));

        this.language_select_field.selectedIndex = 0;
        this.mode_select_field.selectedIndex = 0;

        this.refresh_button.onclick = this.onRefreshButtonClick.bind(this);
        
        this.apply_wifi_button.onclick = this.onApplyWifiButtonClick.bind(this);
        this.connect_aerobo_button.onclick = this.onConnectAeroboButtonClick.bind(this);

        this.language_select_field.listen('change', this.onLanguageChange.bind(this));
        this.mode_select_field.listen('change', this.onModeChange.bind(this));
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @override
     */
    onShow(){
        this.refreshSettings();
    }

    /**
     * Called when user clicks on save WIFI settings button.
     */
    onApplyWifiButtonClick(){
        this.saveWifiSettings();
    }

    /**
     * Called when user clicks on connect to Aerobo Cloud button.
     */
    onConnectAeroboButtonClick(){

    }

    /**
     * Called when user clicks on the refresh button.
     * Reloads the settings from server.
     */
    onRefreshButtonClick(){
        this.refreshSettings();
    }

    /**
     * Reloads settings from server and update UI components.
     */
    refreshSettings(){
        SettingsManager.getSettings()
            .then((iSettings) => {
                this.settings = iSettings;
                // App settings
                var lang_ind = Constants.SETTINGS_LANGUAGES.indexOf(this.settings.app.language);
                this.language_select_field.selectedIndex = lang_ind >= 0 ? lang_ind : 0;
                this.mode_select_field.selectedIndex = this.settings.app.mode;

                // Wifi settings
                this.wifi_ssid_field.value = this.settings.wifi.ssid;
                this.wifi_wpa_passphrase_field.value = this.settings.wifi.wpa_passphrase;
    
                this.wifi_ssid_field.input_.addEventListener('change', this.checkValidity.bind(this));
                this.wifi_wpa_passphrase_field.input_.addEventListener('change', this.checkValidity.bind(this));
                this.wifi_ssid_field.input_.addEventListener('keyup', this.checkValidity.bind(this));
                this.wifi_wpa_passphrase_field.input_.addEventListener('keyup', this.checkValidity.bind(this));
                
                this.checkValidity();
            })
            .catch(() => {});
    }

    /**
     * Save WIFI settings on server.
     */
    saveWifiSettings(){
        var context = this;
        this.enableSpinner(true);

        this.settings.wifi.ssid = this.wifi_ssid_field.value;
        this.settings.wifi.wpa_passphrase = this.wifi_wpa_passphrase_field.value;

        SettingsManager.saveSettings(this.settings)
            .then(res => {
                MessageManager.showInfo(__('MSG_SUCCESS_SAVE_SETTINGS'));
                context.enableSpinner(false);
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_SAVE_SETTINGS'));
                context.enableSpinner(false);
            });      
    }

    /**
     * Save application settings on server.
     */
    saveAppSettings(){    
        var context = this;        
        this.settings.app.language = Constants.SETTINGS_LANGUAGES[this.language_select_field.selectedIndex]; 
        this.settings.app.mode = this.mode_select_field.selectedIndex;    

        SettingsManager.saveSettings(this.settings)
            .then(res => {
                MessageManager.showInfo(__('MSG_SUCCESS_SAVE_SETTINGS'));
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_SAVE_SETTINGS'));
            });    
    }
    
    /**
     * Checks the validity of the different fields.
     * - WIFI pass phrase must be larger than 8 characters.
     */
    checkValidity(){
        this.apply_wifi_button.disabled = false;
        this.wifi_wpa_passphrase_field.root_.classList.remove('invalid');
        if(this.wifi_wpa_passphrase_field.value.length < 8){
            this.apply_wifi_button.disabled = true;
            this.wifi_wpa_passphrase_field.root_.classList.add('invalid');
        }
    }

    /**
     * Called when language setting is changed.
     * Save the setting on server.
     * 
     * @param {Object} iEvent - Change event.
     */
    onLanguageChange(iEvent){
        this.saveAppSettings();
    }

    /**
     * Called when 2D / 3D mode setting is changed.
     * Save the setting on server.
     * 
     * @param {Object} iEvent - Change event.
     */
    onModeChange(iEvent){
        this.saveAppSettings();
    }
};

export default SettingsPage;