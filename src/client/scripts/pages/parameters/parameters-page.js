/**
 * Module of Ansel's Parameters page.
 * 
 * @requires module:pages/page
 * @requires module:managers/request-manager
 * @requires module:managers/message-manager
 * @requires module:utils/utils
 * @requires module:utils/constants
 * @requires module:models/job
 * 
 * @module pages/parameters/parameters-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import MessageManager from 'managers/message-manager';
import Job from 'models/job';
import Utils from 'utils/utils';

import html from './parameters-page.html';
import css from './parameters-page.css';

/**
 * Ansel's Parameters page.
 * This page gives access to drone parameters in read and write.
 * 
 * @extends module:pages/page~Page
 */
class ParametersPage extends Page{
    /**
     * Creates an instance of PX4Page.
     */
    constructor(){
        super();
        this.name = 'parameters';

        /**
         * Drone's parameters organized by type.
         * @private
         * @member {Array} 
         * */
        this.parameters = [];
        this.parameters_group_list = {};
    }

    getParam(iName){
        for(let i = 0; i < this.parameters.length; i++){
            if(this.parameters[i].name === iName){
                return this.parameters[i];
            }
        }
        return null;
    }

    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);

        this.param_container = this.getContainer().querySelector('.as-param-container');
        
        this.refresh_button = this.getContainer().querySelector('.as-parameters-page-actions-refresh-button');
        this.save_button = this.getContainer().querySelector('.as-parameters-page-actions-save-button');
        this.file_button = this.getContainer().querySelector('.as-parameters-page-actions-palette-button');

        this.file_save_button = this.getContainer().querySelector('.as-parameters-page-actions-file-save-button');
        this.file_load_button = this.getContainer().querySelector('.as-parameters-page-actions-file-load-button');
        

        this.file_menu = this.getContainer().querySelector('.as-parameters-page-menu-file');
        
        this.refresh_button.onclick = this.onRefreshButtonClick.bind(this);
        this.save_button.onclick = this.onSaveButtonClick.bind(this);
        this.file_button.onclick = this.onFileButtonClick.bind(this);
        this.file_save_button.onclick = this.onFileSaveButtonClick.bind(this);
        this.file_load_button.onclick = this.onFileLoadButtonClick.bind(this);
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @override
     */
    onShow(){
        this.loadParameters();
    }

    /**
     * Called when user clicks on the refresh button.
     * Reload the parameters.
     */
    onRefreshButtonClick(){
        this.loadParameters();
    }
    /**
     * Called when user clicks on the save button.
     * Save the modified parameters to the drone.
     */
    onSaveButtonClick(){  
    }

    onFileButtonClick(){
        var menu = new mdc.menu.MDCMenu(this.file_menu);
        menu.open = !menu.open;   
    }

    onFileSaveButtonClick(){
        var text = this.convertParametersToText();
        var blob = new Blob([text], {
            type: 'text/plain'
        });
        Utils.downloadBlobAsFile(blob, 'params.txt');
    }

    onFileLoadButtonClick(){
        var context = this;        
        Utils.openFileChooser()
            .then(function(files){
                if(files.length < 1){ return; }
                console.log(files[0]);

                var reader = new FileReader();

                reader.onload = function(iEvent) {
                    context.convertTextToParameters(reader.result);
                }

                // Read in the image file as a binary string.
                reader.readAsText(files[0]);
            });
    }

    loadParameters(){
        var context = this;
        context.enableSpinner(true); 

        var job = new Job('px4', {
            cmd: 'parameters-list'
        });

        job.start(
            function(iJob){
                context.enableSpinner(false); 
                context.parameters = iJob.result;

                for(let i = 0; i < context.parameters.length; i++){
                    let curr_param_name = context.parameters[i].name;
                    let curr_param_group_id = curr_param_name.split('_')[0];

                    if(!context.parameters_group_list[curr_param_group_id]){
                        context.parameters_group_list[curr_param_group_id] = {
                            id: curr_param_group_id,
                            expanded: false
                        }
                    }
                }
                context.updateUI();
            },
            function(iJob){
                context.enableSpinner(false);
            }
        );
    }

    updateUI(){
        var template = '';
        if(this.parameters.length === 0){
            this.param_container.innerHTML = template;
            return;
        }

        var curr_parameters_group = null;        

        for(let i = 0; i < this.parameters.length; i++){
            let curr_param_name = this.parameters[i].name;
            let curr_param_value = this.parameters[i].value;

            let curr_param_group_id = curr_param_name.split('_')[0];

            if(!curr_parameters_group || curr_param_group_id !== curr_parameters_group.id){
                if(curr_parameters_group){
                    template += '</table></div>';
                }
                curr_parameters_group = this.parameters_group_list[curr_param_group_id];

                var curr_expanded_class = false;
                if(curr_parameters_group){
                    curr_expanded_class = curr_parameters_group.expanded?'as-param-group-expanded':'as-param-group-collapsed';
                }
                template += `
                    <div 
                        class="as-param-group as-param-group-${curr_param_group_id} ${curr_expanded_class}" 
                        group="${curr_param_group_id}"
                    >
                        <div class="as-param-group-header">
                            <i class="mdc-icon-button material-icons as-param-group-expand-icon">expand_more</i>
                            <i class="mdc-icon-button material-icons as-param-group-collapse-icon">expand_less</i>
                            ${curr_param_group_id}
                        </div>
                        <table>
                `;
            }

            template += `
                <tr class="as-param-group-row">
                    <th class="as-param-name-cell">${curr_param_name}</th>
                    <th class="as-param-value-cell">
                        <div class="mdc-text-field mdc-text-field--fullwidth as-node-create-args-input">
                            <input 
                                class="mdc-text-field__input" 
                                type="number" 
                                pattern="[0-9]"
                                placeholder="${curr_param_name}" 
                                value="${curr_param_value}" 
                                aria-label="Full-Width Text Field">
                        </div>
                    </th>
                </tr>`;
        }
        template += '</table></div>';

        this.param_container.innerHTML = template;

        var text_field_list = this.param_container.querySelectorAll('.mdc-text-field');
        for(let i = 0; i < text_field_list.length; i++){
            let curr_icon = new mdc.textField.MDCTextField(text_field_list[i]);
            curr_icon.listen('change', this.onParamTextChange.bind(this));
        }

        var header_list = this.param_container.querySelectorAll('.as-param-group-header');
        for(let i = 0; i < header_list.length; i++){
            header_list[i].onclick = this.onParamGroupExpandToggleButtonChange.bind(this);
        }
    }

    onParamGroupExpandToggleButtonChange(iEvent){
        var group_container = iEvent.currentTarget.parentElement;
        if(!group_container){ return; }
        var group_id = group_container.getAttribute('group');
        if(!group_id){ return; }
        var group = this.parameters_group_list[group_id];
        if(!group){ return; }

        if(group.expanded){
            group_container.classList.remove('as-param-group-expanded');
            group_container.classList.add('as-param-group-collapsed');
        }
        else{
            group_container.classList.add('as-param-group-expanded');
            group_container.classList.remove('as-param-group-collapsed');
        }
        group.expanded = !group.expanded;
    }
    onParamTextChange(iEvent){
        var param_row = iEvent.target.parentElement.parentElement.parentElement;
        var param_name_cell = param_row.querySelector('.as-param-name-cell');

        var param_name = param_name_cell.innerHTML;
        var param_new_value = parseFloat(iEvent.target.value);

        var param_drone = this.getParam(param_name);
        param_drone.user_value = param_new_value;

        if(param_drone.value !== param_drone.user_value){
            param_row.classList.add('dirty');
        }
        else{
            param_row.classList.remove('dirty');
        }
    }

    convertParametersToText(){
        var res = '';
        res += '# Onboard parameters for system MAV 001\n';
        res += '#\n';
        res += '# MAV ID  COMPONENT ID  PARAM NAME  VALUE (FLOAT)\n';

        for(let i = 0; i < this.parameters.length; i++){
            let curr_name = this.parameters[i].name;
            let curr_mav = this.parameters[i].mav;
            let curr_id = this.parameters[i].id;
            let curr_value = this.parameters[i].value;
            let curr_user_value = this.parameters[i].user_value;
            let curr_type = this.parameters[i].type;

            res += `${curr_mav}\t${curr_id}\t${curr_name}\t${curr_user_value?curr_user_value:curr_value}\t${curr_type}\n`;
        }

        return res;
    }

    convertTextToParameters(iText){
        var lines = iText.split(/\r?\n/);
        for(let i = 0; i < lines.length; i++){
            var curr_line = lines[i].trim();
            if(curr_line.startsWith("#")){ continue; }

            var curr_line_items = curr_line.split(/\t/);
            if(curr_line_items.length < 5) { continue; }

            for(let j = 0; j < curr_line_items.length; j++){
                curr_line_items[j] = curr_line_items[j].trim();
            }
            console.log(curr_line_items)
        }
    }
};

export default ParametersPage;