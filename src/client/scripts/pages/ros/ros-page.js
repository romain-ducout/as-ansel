/**
 * Module of Ansel's ROS page.
 * 
 * @requires module:pages/page
 * @requires module:managers/request-manager
 * @requires module:managers/message-manager
 * @requires module:components/element
 * @requires module:components/modal
 * @requires module:components/modal/ros-create-modal/ros-create-modal
 * 
 * @module pages/ros/ros-page
 * @author Romain Ducout
 */
import Page from 'pages/page';
import RequestManager from 'managers/request-manager';
import MessageManager from 'managers/message-manager';
import Element from 'components/element';
import Modal from 'components/modal';
import RosCreateModal from 'components/modal/ros-create-modal/ros-create-modal';

import html from './ros-page.html';
import css from './ros-page.css';

/**
 * Maximum line that can be displayed and kept for one node.
 * 
 * @private
 * @type {number}
 */
const MAX_BUFFER = 100;

/**
 * List of nodes to not display to user.
 * 
 * @private
 * @type {Array.<string>}
 */
const FILTERED_NODE_LIST = [
    '/rosapi',
    '/rosout',
    '/logger',
    '/rosbridge_websocket'
];

/**
 * Ansel's ROS page (Need developer access).
 * Page to access directly to ROS running on JETSON.
 * 
 * @extends module:pages/page~Page
 */
class ROSPage extends Page{
    /**
     * Creates an instance of ROSPage.
     */
    constructor(){
        super();
        this.name = 'ros';
        
        /**
         * Identifier of the currently displayed node.
         * @private
         * @member {string} 
         * */
        this.current_node_id = null;
        /**
         * Out stream of the different nodes.
         * @private
         * @member {Object.<string, Array.<string>>} 
         * */
        this.rosout = {};
    }

    /**
     * Initializes the page's components.
     * 
     * @override
     */
    init(){
        this.initTemplate(html);
        var context = this;

        this.node_info_drawer = this.getContainer().querySelector('.as-right-drawer');

        this.tab_bar_content_elt = this.getContainer().querySelector('.as-tab-bar-content');
        this.node_container_elt = this.getContainer().querySelector('.as-ros-node-container');
        this.node_action_container_elt = this.getContainer().querySelector('.node-actions-container');

        this.delete_node_button = this.getContainer().querySelector('.as-ros-page-actions-delete-node-button');
        this.info_node_button = this.getContainer().querySelector('.as-ros-page-actions-info-node-button');
        this.add_button = this.getContainer().querySelector('.as-ros-page-actions-add-button');
        
        this.delete_node_button.onclick = this.onDeleteNodeButtonClick.bind(this);
        this.info_node_button.onclick = this.onInfoNodeButtonClick.bind(this);
        this.add_button.onclick = this.onAddButtonClick.bind(this);

        var socket = io(); 
        socket.on('rosout', function(iMsg) {
            if(!context.rosout[iMsg.name]){
                context.rosout[iMsg.name] = [];
            }
            if(context.rosout[iMsg.name].length > MAX_BUFFER){
                context.rosout[iMsg.name].splice(0, context.rosout[iMsg.name].length - MAX_BUFFER);
            }
            context.rosout[iMsg.name].push(iMsg);
            context.logMessage(iMsg);
        });            
    }

    /**
     * Method called when the page is being shown on screen.
     * 
     * @override
     */
    onShow(){
        this.refresh();
    }

    /**
     * Gets a node URL endpoint.
     * 
     * @param {string} iNodeId - Node identifier.
     * @returns {string} Node URL endpoint.
     */
    getNodeUrl(iNodeId){
        return 'api/ros/nodes/' + iNodeId.substring(1);
    }

    /**
     * Called when user clicks on the delete node button.
     * Shows a warning message to the user before deleting the node if user agrees.
     */
    onDeleteNodeButtonClick(){
        var context = this;
        if(!this.current_node_id){ return; }

        var title = __('MSG_WARNING_ROS_NODE_DELETE_TITLE').replace(`{${0}}`, this.current_node_id);
        new Modal({
            title: title,
            html: __('MSG_WARNING_ROS_NODE_DELETE'),
            actions: [
                {
                    title: __('CANCEL')
                },
                {
                    accept: true,
                    title: __('VALIDATE'),
                    callback: function(){
                        context.deleteNode(context.current_node_id);
                    }
                }
            ]
        }).show();  
    }

    /**
     * Called when user clicks on the info node button.
     * Display drawer with node informations.
     */
    onInfoNodeButtonClick(){
        this.node_info_drawer.classList.toggle('active');
    }

    /**
     * Called when user clicks on the add button.
     * Display modal to add node / topic/ service.
     */
    onAddButtonClick(){
        var panel = new RosCreateModal(this);
        panel.show();
    }

    /**
     * Refreshes the node views.
     */
    refresh(){
        var context = this;
        this.tab_bar_content_elt.innerHTML = '';

        RequestManager.get('api/ros/nodes', {type: 'json'})
            .then(res => {
                var nodes = [];
                for(let i = 0; i < res.length; i++){
                    if(FILTERED_NODE_LIST.indexOf(res[i]) < 0){
                        nodes.push(res[i]);
                    }
                }
                if(nodes.length < 1){
                    return;
                }
                
                for(let i = 0; i < nodes.length; i++){
                    let curr_node_id = nodes[i];
                    let currElement = new Element('a', {
                        class: 'mdc-tab mdc-ripple-upgraded',
                        html: curr_node_id,
                        events: {
                            onclick: function(){
                                context.loadNodeView(curr_node_id);
                            }
                        }
                    }).append(context.tab_bar_content_elt);
                }

                new Element('span', {
                    class: 'mdc-tab-bar__indicator'
                }).append(context.tab_bar_content_elt);

                new mdc.tabs.MDCTabBarScroller(this.getContainer().querySelector('.mdc-tab-bar-scroller'));
                context.loadNodeView(nodes[0]);
            })
            .catch(err => {
            });
    }

    /**
     * Displays a given node view.
     * 
     * @param {string} iNodeId - Identifier of the node.
     */
    loadNodeView(iNodeId){
        var context = this;
        this.current_node_id = iNodeId;

        this.node_container_elt.innerHTML = `
        <div class="as-node-container">
            <ul class="mdc-list mdc-list--dense mdc-list--non-interactive as-node-log-container">
            </ul>
        </div>
        `;

        if(this.rosout[this.current_node_id]){
            for(let i = 0; i < this.rosout[this.current_node_id].length; i++){
                this.logMessage(this.rosout[this.current_node_id][i]);
            }
        }

        this.refreshUI();

        RequestManager.get(this.getNodeUrl(this.current_node_id), {type: 'json'})
            .then(res => {
                this.refreshNodeInfoMenu(res);
            })
            .catch(err => {
                this.refreshNodeInfoMenu(null);
            });
    }

    /**
     * Logs a node message.
     * 
     * @param {Object} iMsg - Message to log.
     * @param {string} iMsg.name - Node id of message.
     * @param {string} iMsg.msg - Text of the message.
     * @param {string} iMsg.function - Function of the mesage.
     */
    logMessage(iMsg){
        if(iMsg.name !== this.current_node_id){ return; }
        var nodeContainer = this.getContainer().querySelector('.as-node-container');
        if(!nodeContainer){ return; }
        var logContainer = this.getContainer().querySelector('.as-node-log-container');
        if(!logContainer){ return; }

        var new_elt = new Element('li', {
            class: 'mdc-list-item',
            html: `${iMsg.msg}<span class="mdc-list-item__meta">${iMsg.function}</span>`
        }).append(logContainer);

        nodeContainer.scrollTo(0, 1000000);
    }

    /**
     * Refreshes user interface.
     */
    refreshUI(){
        if(this.current_node_id){
            this.node_action_container_elt.removeAttribute('hidden');
        }
        else{
            this.node_action_container_elt.add('hidden');
        }
    }
    
    /**
     * Refreshes the node info view based on node information object.
     * 
     * @param {Object} iNodeInfo - Node description.
     * @param {Array} iNodeInfo.publishing - List of topics published by node.
     * @param {Array} iNodeInfo.services - List of node's services.
     * @param {Array} iNodeInfo.subscribing - List of node's subscriptions.
     */
    refreshNodeInfoMenu(iNodeInfo){
        var info_contairer = this.getContainer().querySelector('.as-right-drawer');
        info_contairer.innerHTML = '';
        if(!iNodeInfo){ return; }


        var publishing_template = ``;
        var services_template = ``;
        var subscribing_template = ``;

        for(let i = 0; i < iNodeInfo.publishing.length; i++){
            publishing_template += `<li class="mdc-list-item">${iNodeInfo.publishing[i]}</li>`;
        }
        for(let i = 0; i < iNodeInfo.services.length; i++){
            services_template += `<li class="mdc-list-item">${iNodeInfo.services[i]}</li>`;
        }
        for(let i = 0; i < iNodeInfo.subscribing.length; i++){
            subscribing_template += `<li class="mdc-list-item">${iNodeInfo.subscribing[i]}</li>`;
        }

        var template = `
            <div class="mdc-list-group">
                <h3 class="mdc-list-group__subheader">${__('NODE_INFO_PUBLISHING')}</h3>
                <ul class="mdc-list mdc-list--non-interactive mdc-list--dense">
                    ${publishing_template}
                </ul>
                <hr class="mdc-list-divider">
                <h3 class="mdc-list-group__subheader">${__('NODE_INFO_SERVICES')}</h3>
                <ul class="mdc-list mdc-list--non-interactive mdc-list--dense">
                    ${services_template}
                </ul>
                <hr class="mdc-list-divider">
                <h3 class="mdc-list-group__subheader">${__('NODE_INFO_SUBSCRIBING')}</h3>
                <ul class="mdc-list mdc-list--non-interactive mdc-list--dense">
                    ${subscribing_template}
                </ul>
            </div>
        `;
        new Element('div', {
            html: template
        }).append(this.node_info_drawer);
    }

    /**
     * Creates a new ROS node.
     * 
     * @param {string} iPkg - Package name of the node.
     * @param {string} iNodeType - Type of the node.
     * @param {string} iArgs - Arguments for the node.
     */
    createNode(iPkg, iNodeType, iArgs){
        var context = this;
        this.enableSpinner(true);
            
        RequestManager.post(
            'api/ros/nodes', 
            {
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    pkg: iPkg,
                    type: iNodeType,
                    args: iArgs
                },
                type: 'json'
            }
        )
            .then(res => {
                setTimeout(() => {
                    context.enableSpinner(false); 
                    context.refresh();                        
                }, 1000);
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_CREATE_NODE'));
                context.enableSpinner(false);
                context.refresh();
            });
    }

    /**
     * Deletes a ROS node.
     * 
     * @param {any} iNodeId - Identifier of the node to delete.
     */
    deleteNode(iNodeId){
        var context = this;
        context.enableSpinner(true);
        
        RequestManager.delete(
            this.getNodeUrl(iNodeId), 
            {type: 'json'}
        )
            .then(res => {
                context.enableSpinner(false);
                context.refresh();
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_DELETE_NODE'));
                context.enableSpinner(false);
                context.refresh();
            });
    }

    /**
     * Creates a new ROS topic.
     * 
     * @param {string} iName - Name of the topic.
     * @param {string} iType - Type of the topic.
     * @param {string} iArgs - Arguments for the topic.
     */
    createTopic(iName, iType, iArgs){
        var context = this;
        this.enableSpinner(true);
            
        RequestManager.post(
            'api/ros/topics', 
            {
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    name: iName,
                    type: iType,
                    args: iArgs
                },
                type: 'json'
            }
        )
            .then(res => {
                setTimeout(() => {
                    context.enableSpinner(false);                 
                }, 1000);
            })
            .catch(err => {
                MessageManager.showError(__('MSG_ERROR_CREATE_TOPIC'));
                context.enableSpinner(false);
            });
    }

    /**
     * Creates a new ROS service.
     */
    createService(){
        this.parent.enableSpinner(true);
        this.parent.enableSpinner(false); 
    }
};

export default ROSPage;