/**
 * Ansel server application root module.
 * 
 * @requires module:managers/page-manager
 * @requires module:managers/message-manager
 * @requires module:managers/request-manager
 * @requires module:utils/utils
 * @requires module:components/element
 * @requires module:components/drawer/navigation-drawer
 * @requires module:models/drone
 * 
 * @module ansel
 * @author Romain Ducout
 */
import PageManager from 'managers/page-manager';
import MessageManager from 'managers/message-manager';
import RequestManager from 'managers/request-manager';
import Utils from 'utils/utils';
import Element from 'components/element';
import NavigationDrawer from 'components/drawer/navigation-drawer';
import Drone from 'models/drone';

import css from 'css/ansel.css';
import template from './ansel.html';

/**
 * Ansel client application root class.
 */
class Ansel{
    /**
     * Creates an instance of Ansel.
     */
    constructor(){
        /**
         * Indicates if the debug mode is activated. 
         * @private
         * @member {boolean} 
         * */
        this.debug = false;
    }
    
    /**
     * Initializes the ansel application.
     */
    init(){
        var context = this;
        mdc.autoInit();

        document.body.innerHTML += template;

        this.splash_screen = document.querySelector('#as-splash');
        this.app_root = document.querySelector('#as-app');

        var url = new URL(location.href);
        var debugParam = url.searchParams.get('debug');

        if(debugParam && debugParam.length > 0){
            this.debug = (debugParam.toLowerCase() === 'true');
        }
        if(this.debug){
            document.body.classList.add('debug-mode');
        }

        this.navigation_drawer = new NavigationDrawer(this);
        this.navigation_drawer.init();

        Utils.getCesiumPromise()
            .then(function(){
                context.onLoaded();
            });
    }

    /**
     * Called when the page is entirely loaded.
     * We can now remove the flash screen.
     */
    onLoaded(){
        Drone.connect(this.onDroneChange.bind(this));
        
        this.splash_screen.style.opacity = 0.0;
        this.navigate('drone', true); 

        this.app_root.removeAttribute('hidden');
        setTimeout(() => {
            this.splash_screen.setAttribute('hidden', 'true');            
        }, 300);
    }

    /**
     * Gets root DOM element of the application.
     * 
     * @returns {HTMLElement} DOM root element of application.
     */
    getRootElt(){
        return this.app_root;
    }

    /**
     * Opens the navigation drawer.
     */
    openDrawer(){
        this.navigation_drawer.open();
    }

    /**
     * Closes the navigation drawer.
     */
    closeDrawer(){
        this.navigation_drawer.close();
    }

    /**
     * Navigates to a given page.
     * 
     * @param {string} iPageName - Page name to navigate to.
     * @param {Object} iArgs - Arguments to show page.
     * @param {boolean} iRoot - True if we go to a root page.
     */
    navigate(iPageName, iArgs, iRoot){
        PageManager.navigate(iPageName, iArgs, iRoot);
        this.navigation_drawer.refreshNavigation();
    }

    /**
     * Navigates back to previous page.
     */
    back(){
        PageManager.back();
        this.navigation_drawer.refreshNavigation();
    }

    /**
     * Called when drone status changed.
     */
    onDroneChange(){
        this.setArmedMode(Drone.isArmed());
    }

    /**
     * Shows/hides spinner on screen.
     * 
     * @param {boolean} iEnable - True to show spinner, false to hide it.
     * @param {string} iMessage - Message to show bellow the spinner.
     */
    enableSpinner(iEnable, iMessage){
        var spinner_container = document.getElementById('as-loading-spinner-container');
        var progress_elt = spinner_container.querySelector('.mdc-linear-progress');

        progress_elt.classList.remove('mdc-linear-progress--determinate');
        progress_elt.classList.add('mdc-linear-progress--indeterminate');
        
        if (iEnable) {
            spinner_container.removeAttribute('hidden');
            progress_elt.classList.remove('mdc-linear-progress--closed');
        } else {
            spinner_container.setAttribute('hidden', true);
            progress_elt.classList.add('mdc-linear-progress--closed');
        }
        this.setSpinnerMessage(iMessage);
    }

    /**
     * Sets the spinner at a given progress.
     * 
     * @param {number} iProgress - Progress of the spinner.
     * @param {string} iMessage - Message to display bellow the spinner.
     */
    setSpinnerProgress(iProgress, iMessage){  
        var spinner_container = document.getElementById('as-loading-spinner-container');
        var progress_elt = spinner_container.querySelector('.mdc-linear-progress');    

        progress_elt.classList.remove('mdc-linear-progress--indeterminate');
        progress_elt.classList.add('mdc-linear-progress--determinate');
    
        var linearProgress = mdc.linearProgress.MDCLinearProgress.attachTo(progress_elt);
        linearProgress.progress = iProgress;
        this.setSpinnerMessage(iMessage);
    }

    /**
     * Sets the message of the spinner.
     * 
     * @param {string} iMessage - Message to display bellow the spinner.
     */
    setSpinnerMessage(iMessage){  
        var spinner_container = document.getElementById('as-loading-spinner-container');
        var message_elt = spinner_container.querySelector('.as-loading-spinner-message');    

        if(iMessage){
            message_elt.innerHTML = iMessage;
        }
        else{
            message_elt.innerHTML = '';
        }
    }

    /**
     * Adapt application to armed status of drone.
     * If drone is armed, we block some pages to avoid heavy bandwith consuption.
     * 
     * @param {boolean} iArmed - Armed status of drone.
     */
    setArmedMode(iArmed){
        if(iArmed){
            // force drone page in armed state to avoid bandwidth use.
            var bc_root = PageManager.getBreadCrumbRoot();
            var page = bc_root.page.name;
            if(page !== 'drone' && page !== 'cameras'){
                this.navigate('drone', [], true);
                MessageManager.showInfo(__('MSG_INFO_DRONE_ARMED'));
            }
        }
    }
};

export default Ansel;
