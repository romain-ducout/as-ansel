/**
 * Module of the request manager class.
 * 
 * @requires module:pages/drone/drone-page
 * @requires module:pages/flight/flight-page
 * @requires module:pages/settings/settings-page
 * @requires module:pages/cameras/cameras-page
 * @requires module:pages/command/command-page
 * @requires module:pages/ros/ros-page
 * @requires module:pages/px4/px4-page
 * @requires module:pages/parameters/parameters-page
 * @requires module:pages/image/image-page
 * @requires module:pages/about/about-page
 * 
 * @module managers/page-manager
 * @author Romain Ducout
 */
import DronePage from 'pages/drone/drone-page';
import FlightPage from 'pages/flight/flight-page';
import SettingsPage from 'pages/settings/settings-page';
import CamerasPage from 'pages/cameras/cameras-page';
import CommandPage from 'pages/command/command-page';
import ROSPage from 'pages/ros/ros-page';
import PX4Page from 'pages/px4/px4-page';
import ParametersPage from 'pages/parameters/parameters-page';
import ImagePage from 'pages/image/image-page';
import AboutPage from 'pages/about/about-page';

/**
 * Singleton that manages pages and their manipulation / navigation throughout the application.
 */
class PageManager{
    /**
     * Creates an instance of PageManager.
     */
    constructor(){
        /**
         * Breadcrumb object that represent current navigated pages. 
         * @private
         * @member {Array} 
         * */
        this.breadcrumb = [];

        /**
         * Pages of applications.
         * A page need to be accessed once to be created. 
         * @private
         * @member {Object.<string, module:pages/page~Page>} 
         * */
        this.pages = {};
    }

    /**
     * Gets a page from its name.
     * Creates the page if it does not exist.
     * 
     * @param {string} iPageName - Name of page to retrieve.
     * @returns {Page} Requested page.
     */
    getPage(iPageName){
        if(!this.pages[iPageName]){
            switch (iPageName) {
                case 'drone':
                    this.pages[iPageName] = new DronePage();
                    break;   
                case 'flight':
                    this.pages[iPageName] = new FlightPage();
                    break;   
                case 'image':
                    this.pages[iPageName] = new ImagePage();
                    break;  
                case 'command':
                    this.pages[iPageName] = new CommandPage();
                    break;   
                case 'ros':
                    this.pages[iPageName] = new ROSPage();
                    break;  
                case 'px4':
                    this.pages[iPageName] = new PX4Page();
                    break; 
                case 'parameters':
                    this.pages[iPageName] = new ParametersPage();
                    break; 
                case 'cameras':
                    this.pages[iPageName] = new CamerasPage();
                    break;  
                case 'settings':
                    this.pages[iPageName] = new SettingsPage();
                    break;  
                case 'about':
                    this.pages[iPageName] = new AboutPage();
                    break;            
                default:
                    break;
            }
            this.pages[iPageName].init();
        }
        return this.pages[iPageName];
    }

    /**
     * Gets breadcrumb navigation object.
     * 
     * @returns {Object} Breadcrumb navigation object.
     */
    getBreadCrumb(){
        return this.breadcrumb;
    }

    /**
     * Gets breadcrumb root object.
     * 
     * @returns {Object} Breadcrumb root object.
     */
    getBreadCrumbRoot(){
        return this.breadcrumb[0];
    }

    /**
     * Navigates to a page.
     * 
     * @param {string} iPageName - Name of page to navigate to.
     * @param {Object} iArgs - Arguments to page.
     * @param {boolean} iRoot - True if page is navigation root.
     */
    navigate(iPageName, iArgs, iRoot){
        var last_item = this.breadcrumb[this.breadcrumb.length - 1];
        if(last_item){
            last_item.page.hide();
        }

        var new_page = this.getPage(iPageName);
        new_page.show(iArgs);

        var breadcrumb_item = {
            page: new_page,
            args: iArgs
        };

        if(iRoot){
            this.breadcrumb = [breadcrumb_item];

        }
        else{
            this.breadcrumb.push(breadcrumb_item);
        }
    }

    /**
     * Navigate back on breadcrumb navigation.
     */
    back(){
        if(this.breadcrumb.length < 2){ return; }
        var curr_page = this.breadcrumb[this.breadcrumb.length - 1].page;
        var prev_page = this.breadcrumb[this.breadcrumb.length - 2].page;
        this.breadcrumb.pop();

        curr_page.hide();
        prev_page.show();          
    }
};

export default new PageManager();