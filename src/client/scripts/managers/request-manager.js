/**
 * Module of a class helper to perform HTTP requests.
 * 
 * @module managers/request-manager
 * @author Romain Ducout
 */

/**
 * Class helper to perform HTTP requests.
 */
class RequestManager{
    /**
     * Creates an instance of RequestManager.
     */
    constructor(){
    }

    /**
     * Sends a request.
     * Request can be configured via object with members:
     * 
     * @static
     * @param {string} iUrl - URL of request.
     * @param {Object} iConfig - Configuration of request.
     * @param {string} iConfig.type - Request's response type.
     * @param {string} iConfig.method - Request method (GET, POST...).
     * @param {Object.<string, string>} iConfig.headers - Headers of request.
     * @param {Object} iConfig.data - Data of request.
     * @returns {Promise} Promise of the request being sent.
     */
    static sendRequest(iUrl, iConfig){
        if(!iConfig){
            iConfig = {};
        }
        return new Promise(function(resolve, reject){
            var xhttp = new XMLHttpRequest();
            var method = 'GET';

            if(iConfig.type){
                xhttp.responseType = iConfig.type;
            }
            if(iConfig.method){
                method = iConfig.method;
            }

            xhttp.onreadystatechange = function() {
                if (this.readyState === 4){
                    if(this.status === 200) {
                        resolve(xhttp.response);
                    }
                    else{
                        reject(this);
                    }
                }
            };

            xhttp.open(method, iUrl, true);

            if(iConfig.headers){
                for(let header in iConfig.headers){
                    xhttp.setRequestHeader(header, iConfig.headers[header]);
                }
            }
            
            if(iConfig.data){
                xhttp.send(JSON.stringify(iConfig.data));
            }
            else{
                xhttp.send();
            }

        });
    }

    /**
     * Sends GET request.
     * 
     * @static
     * @param {string} iUrl - URL of request.
     * @param {Object} iConfig - Configuration of request.
     * @returns {Promise} Promise of the request being sent.
     */
    static get(iUrl, iConfig){
        if(!iConfig){
            iConfig = {};
        }
        iConfig.method = 'GET';
        return this.sendRequest(iUrl, iConfig);
    }

    /**
     * Sends POST request.
     * 
     * @static
     * @param {string} iUrl - URL of request.
     * @param {Object} iConfig - Configuration of request.
     * @returns {Promise} Promise of the request being sent.
     */
    static post(iUrl, iConfig){
        if(!iConfig){
            iConfig = {};
        }
        iConfig.method = 'POST';
        return this.sendRequest(iUrl, iConfig);
    }

    /**
     * Sends DELETE request.
     * 
     * @static
     * @param {string} iUrl - URL of request.
     * @param {Object} iConfig - Configuration of request.
     * @returns {Promise} Promise of the request being sent.
     */
    static delete(iUrl, iConfig){
        if(!iConfig){
            iConfig = {};
        }
        iConfig.method = 'DELETE';
        return this.sendRequest(iUrl, iConfig);
    }
};

export default RequestManager;