/**
 * Module of the event manager class.
 * 
 * @module managers/event-manager
 * @author Romain Ducout
 */

/**
 * Helper class to create events based components.
 */
class EventManager{
    /**
     * Creates an instance of EventManager.
     * @param {Object} iEvents - List of events/callback object.
     */
    constructor(iEvents){
        /**
         * Association of events names to list of callback methods. 
         * @private
         * @member {Object.<string, Function[]>} 
         * */
        this.events = {};

        if(!iEvents){
            return;
        }

        for(var event in iEvents){
            this.connect(event, iEvents[event]);
        }
    }

    /**
     * Connects an event to a callback method.
     * 
     * @param {string} iEventName - Name of event.
     * @param {Function} iCallback - Callback method for event. 
     */
    connect(iEventName, iCallback){
        if(!this.events[iEventName]){
            this.events[iEventName] = [];
        }
        this.events[iEventName].push(iCallback);
    }

    /**
     * Disconnects an event to a callback method.
     * 
     * @param {string} iEventName - Name of event.
     * @param {Function} iCallback - Callback method for event. 
     */
    disconnect(iEventName, iCallback){
        if(!this.events[iEventName]){ return; }
        var callback_list = this.events[iEventName];

        for(let i = 0; i < callback_list.length; i++){
            if(callback_list[i] === iCallback){
                callback_list.splice(i, 1);
                return;
            }
        }
    }

    /**
     * Raises an event to call all cackbacks associated.
     * 
     * @param {string} iEventName - Name of event.
     * @param {Object} iArgs - Event arguments
     */
    raise(iEventName, iArgs){
        if(!this.events[iEventName]){ return; }
        var callback_list = this.events[iEventName];

        for(let i = 0; i < callback_list.length; i++){
            callback_list[i].apply(this, iArgs);
        }
    }
};

export default EventManager;
