/**
 * Module of the settings manager class.
 * 
 * @requires module:pages/page
 * @requires module:managers/request-manager
 * @requires module:managers/message-manager
 * @requires module:utils/utils
 * @requires module:utils/constants
 * 
 * @module managers/settings-manager
 * @author Romain Ducout
 */
import Page from 'pages/page';
import RequestManager from 'managers/request-manager';
import MessageManager from 'managers/message-manager';
import Utils from 'utils/utils';
import Constants from 'utils/constants';

/**
 * Manages application settings, their retrieval and saving.
 * 
 * @extends {Page}
 */
class SettingsManager extends Page{
    /**
     * Creates an instance of SettingsManager.
     */
    constructor(){
        super();

        /**
         * Current settings retrieved from server. 
         * @private
         * @member {Object} 
         * */
        this.settings = null;
        /**
         * Promise to the settings loaded from server. 
         * @private
         * @member {Promise} 
         * */
        this.settings_promise = null;
    }
    
    /**
     * Gets a promise of the settings.
     * 
     * @returns {Promise} Promise of the settings.
     */
    getSettings(){
        var context = this;

        if(!this.settings_promise){
            this.settings_promise = new Promise(function(resolve, reject){
                RequestManager.get('api/settings', {type: 'json'})
                    .then(res => {
                        context.settings = res;
                        resolve(context.settings);
                    })
                    .catch(err => {
                        MessageManager.showError(__('MSG_ERROR_LOAD_SETTINGS'));
                        reject();
                    });
            });
        }
        
        return this.settings_promise;
    }

    /**
     * Saves the settings.
     * 
     * @param {Object} iSettings - Settings to save.
     * @returns {Promise} Promise of settings being saved.
     */
    saveSettings(iSettings){
        var context = this;

        this.settings = iSettings;
        
        return new Promise(function(resolve, reject){
            RequestManager.post(
                'api/settings', 
                {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: context.settings,
                    type: 'json'
                }
            )
                .then(res => {
                    resolve();
                })
                .catch(err => {
                    reject();
                });
        });            
    }
};

export default new SettingsManager();