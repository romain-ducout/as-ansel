/**
 * Module of the message manager class.
 * 
 * @requires module:utils/utils
 * 
 * @module managers/message-manager
 * @author Romain Ducout
 */
import Utils from 'utils/utils';

/**
 * Use this singleton manager to display informative / error messages to the user.
 * This will display a snack bar item at the bottom of the application.
 */
class MessageManager{
    /**
     * Creates an instance of MessageManager.
     */
    constructor(){
        /**
         * Snackbar object of Ansel application. 
         * @private
         * @member {Object} 
         * */
        this.snackbar = null;
    }

    /**
     * Initializes manager.
     */
    init(){
        if(!this.snackbar){
            this.snackbar_elt = document.getElementById('as-snackbar');
            this.snackbar = new mdc.snackbar.MDCSnackbar(this.snackbar_elt);
        }
    }

    /**
     * Shows a message.
     * 
     * @param {Object} iData - Message object
     */
    showMessage(iData) {
        this.snackbar.show(iData);
    }

    /**
     * Show an error message.
     * 
     * @param {string} iMessage - Message to display.
     */
    showError(iMessage) {   
        this.init();
        var data =  {
            message: __('ERROR') + ': ' + iMessage,
            actionOnBottom: false,
            multiline: false,
            timeout: 2750
        };
        this.snackbar_elt.classList.add('error');

        this.showMessage(data);
    }

    /**
     * Show an information message.
     * 
     * @param {string} iMessage - Message to display.
     */
    showInfo(iMessage) {
        this.init();
        var data =  {
            message: iMessage,
            actionOnBottom: false,
            multiline: false,
            timeout: 2750
        };
        this.snackbar_elt.classList.remove('error');

        this.showMessage(data);
    }
};

export default new MessageManager();