/**
 * Module of the flight model class.
 * 
 * @requires module:managers/request-manager
 * @requires module:models/image
 * @requires module:models/job
 * @requires module:utils/utils
 * @module models/flight
 * @author Romain Ducout
 */
import RequestManager from 'managers/request-manager';
import Image from 'models/image';
import Job from 'models/job';
import Utils from 'utils/utils';

/**
 * Model class to represents a flight made by the drone.
 * 
 * @requires module:managers/request-manager
 */
class Flight{
    /**
     * Creates an instance of Flight.
     * @param {string} iId - Id of the flight.
     */
    constructor(iId){
        /**
         * Flight identifier. 
         * @private
         * @member {string} 
         * */
        this.id = iId;
        /**
         * Flight data retrieved from server. 
         * @private
         * @member {Object} 
         * */
        this.data = null;
        /**
         * Flight images separated by type. 
         * @private
         * @member {Array.<module:models/image~Image[]>} 
         * */
        this.images = [];
    }

    /**
     * Gets the title of the flight.
     * 
     * @returns {string} Title of the flight.
     */
    getTitle(){
        var date = Utils.computeFlightDate(this.id);
        return (date)?date.toLocaleString():this.id;
    }

    /**
     * Gets the images taken during the flight of a given type.
     * 
     * @param {number} iType - Type of images to retrieve.
     * @returns {Array.<Image>} Images taken during the flight of given type.
     */
    getImages(iType){
        return this.images[iType];
    }

    /**
     * Gets a specific image taken during the flight.
     * Image is specified by its type and indice.
     * 
     * @param {number} iType - Type of image to retrieve.
     * @param {number} iInd - Indice of image to retrieve.
     * @returns {module:models/image#Image} Image corresponding to inputs.
     */
    getImage(iType, iInd){
        if(!this.images[iType]){
            return null;
        }
        return this.images[iType][iInd];
    }

    /**
     * Gets the markers used in the flight.
     * 
     * @returns {Array} Markers used in the flight.
     */
    getMarkers(){
        return this.data.markers;
    }

    /**
     * Gets the flight's id.
     * 
     * @returns {string} Id of the flight.
     */
    getId(){
        return this.id;
    }

    /**
     * Gets count of all images taken during the flight of all types.
     * 
     * @returns {number} Total of all images of the flight.
     */
    getTotalImageCount(){
        var res = 0;
        for(let i = 0; i < this.images.length; i++){
            for(let j = 0; j < this.images[i].length; j++){
                res++;
            }
        }
        return res;
    }

    /**
     * Loads a flight from server.
     * 
     * @param {Function} iProgress - Progress callback function.
     * @returns {Promise} Promise to the flight loaded.
     */
    load(iProgress){
        var context = this;
        
        return new Promise(function(resolve, reject){
            context.loadData()
                .then(() => {
                    context.loadImagesData(iProgress)
                        .then(() => {
                            resolve();
                        })
                        .catch(err => {
                            reject();
                        });
                })
                .catch(err => {
                    reject();
                });
        });
    }
    
    /**
     * Loads a flight's data from server.
     * 
     * @param {Function} iProgress - Progress callback function.
     * @returns {Promise} Promise to the flight's data loaded.
     */
    loadData(iProgress){
        var context = this;
        return new Promise(function(resolve, reject){
            RequestManager.get('api/flights/' + context.id, {type: 'json'})
                .then(res => {
                    context.data = res;
                    resolve();
                })
                .catch(err => {
                    reject(err);
                }
            );
        });
    }
    
    /**
     * Loads a flight's image from server.
     * 
     * @param {Function} iProgress - Progress callback function.
     * @returns {Promise} Promise to the flight's image loaded.
     */
    loadImagesData(iProgress){
        var context = this;            
        this.images = [];

        return new Promise(function(resolve, reject){
            if(context.data.images.length === 0){ 
                resolve();
                return;
            }

            var total_file_count = 0;
            for(let i = 0; i < context.data.images.length; i++){
                for(let j = 0; j < context.data.images[i].length; j++){
                    total_file_count++;
                }
            }
            
            var image_loaded_count = 0;

            var onImageLoaded = function(iImage){
                if(iImage){
                    if(!context.images[iImage.type]){
                        context.images[iImage.type] = []
                    }
                    context.images[iImage.type][iImage.ind] = iImage;
                }
                image_loaded_count++;
                if(image_loaded_count === total_file_count){
                    resolve();
                }
                else{
                    if(iProgress && image_loaded_count % 10 === 0){
                        iProgress();
                    }
                }
            }

            for(let i = 0; i < context.data.images.length; i++){
                for(let j = 0; j < context.data.images[i].length; j++){
                    let curr_image_id = context.data.images[i][j];
                    let curr_image = new Image(context.id, curr_image_id);
                    curr_image.load()
                        .then((image) => {
                            onImageLoaded(image);
                        })
                        .catch(err => {
                            onImageLoaded(null);
                        });
                }
            }
        });
    }

    /**
     * Deletes the flight from server.
     * 
     * @returns {Promise} Promise of the flight deleted.
     */
    delete(){
        var context = this;
        return new Promise(function(resolve, reject){
            RequestManager.delete('api/flights/' + context.id, {})
                .then(res => {
                    resolve();
                })
                .catch(err => {
                    reject();    
                }
            );
        });
    }

    /**
     * Downloads a flight's archive from server.
     * The archive is a zip file of all photo taken by the flight.
     * 
     * @param {Function} iSuccess - Success download callback method.
     * @param {Function} iError - Error download callback method.
     * @param {Function} iProgress - Progress download callback method.
     */
    download(iSuccess, iError, iProgress){
        var context = this;

        var total_file_count = this.getTotalImageCount();

        import('zip-js/zip.js')
            .then(function(zipjs){
                let zip = zipjs.zip;
                zip.workerScriptsPath = 'zipjs/';

                zip.createWriter(new zip.BlobWriter(), function(zipWriter) {
                    var zipped_file_count = 0;
        
                    var zipping = false;
                    var files_to_zip_blob_list = [];
                    var files_to_zip_name_list = [];
        
                    var onZipEnd = function(zippedBlob){
                        Utils.downloadBlobAsFile(zippedBlob, context.id + '.zip');
                        iSuccess();
                    };
        
                    var onFileZipped = function(){
                        zipped_file_count++;
        
                        iProgress(zipped_file_count / total_file_count);
        
                        if(zipped_file_count === total_file_count){
                            zipWriter.close(onZipEnd);
                        }
                    };
        
                    var zipNextFile = function(){
                        if(zipping){ return; }
                        if(files_to_zip_blob_list.length <= 0){ return; }
                        var blob = files_to_zip_blob_list[0];
                        var name = files_to_zip_name_list[0];
                        files_to_zip_blob_list.splice(0, 1);
                        files_to_zip_name_list.splice(0, 1);
                        zipping = true;
        
                        zipWriter.add(name, new zip.BlobReader(blob), function() {
                            zipping = false;
                            onFileZipped();
                            zipNextFile();
                        });
                    }
        
                    for(let i = 0; i < context.images.length; i++){
                        for(let j = 0; j < context.images[i].length; j++){
                            let curr_image = context.images[i][j];
        
                            RequestManager.get(curr_image.getImageUrl(), {type: 'arraybuffer'})
                                .then(res => {
                                    var blob = new Blob([new Uint8Array(res)], {type: 'image/jpeg'});
                                    files_to_zip_name_list.push(curr_image.id);
                                    files_to_zip_blob_list.push(blob);
                                    zipNextFile();
                                })
                                .catch(err => {
                                });                        
                        }
                    }
                });

            });
    }

    /**
     * Search the flight's image to detect markers.
     * 
     * @param {Function} iSuccess - Success search callback method.
     * @param {Function} iError - Error search callback method.
     * @param {Function} iProgress - Progress search callback method.
     */
    search(iSuccess, iError, iProgress){
        var job = new Job('detector', {
            flight: this.id
        });

        job.start(
            function(iJob){
                iSuccess(iJob);
            },
            function(iJob){
                iError(iJob);
            },
            function(iJob){
                iProgress(iJob.progress);
            }
        );
    }
};

export default Flight;