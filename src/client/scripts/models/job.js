/**
 * Module of the job model class.
 * 
 * @requires module:managers/request-manager
 * 
 * @module models/job
 * @author Romain Ducout
 */
import RequestManager from 'managers/request-manager';

/**
 * Represents a Job being performed on server.
 * A job can be started and then client subscribe to events:
 * - error: an error happened during the job.
 * - finish: the job finished.
 * - progress: the job made progress on server and give update of progress.
 */
class Job{
    /**
     * Creates an instance of Job.
     * @param {string} iId - Id of job.
     * @param {Object} iOptions - Options of job.
     */
    constructor(iId, iOptions){
        /**
         * Job identifier. 
         * @private
         * @member {number} 
         * */
        this.id = iId;
        /**
         * Job options. 
         * @private
         * @member {Object} 
         * */
        this.options = iOptions;
    }

    /**
     * Starts the job on server..
     * 
     * @param {Function} iOnFinish - Finish job callback method.
     * @param {Function} iOnError - Error job callback method.
     * @param {Function} iOnProgress - Progress job callback method.
     */
    start(iOnFinish, iOnError, iOnProgress){
        var context = this;
        var socket = io();

        var onError = function(iJob){
            if(iOnError){
                iOnError(iJob);
            }
        };

        var onFinish = function(iJob){
            if(iOnFinish){
                iOnFinish(iJob);
            }
        }

        var onProgress = function(iJob){
            if(iOnProgress){
                iOnProgress(iJob);
            }
        }

        var onJobUpdate = function(iJob){
            if(iJob.error || iJob.done){
                socket.removeAllListeners('job-' + iJob.id);
            }

            if(iJob.error){
                onError(iJob);
            }
            else if(iJob.done){
                onFinish(iJob);
            }
            else{
                iOnProgress(iJob);
            }
        }

        RequestManager.post('api/jobs', {
            type: 'json',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {                    
                type: this.id,
                options: this.options
            }
        })
            .then(job => {
                context.job = job;
                socket.on('job-' + this.job.id, function(iJob) {
                    onJobUpdate(iJob);
                });
            })
            .catch(err => {  
                onError();
            });
    }
};

export default Job;
