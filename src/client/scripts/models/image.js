/**
 * Module of the image model class.
 * 
 * @requires module:managers/request-manager
 * @requires module:utils/utils
 * 
 * @module models/image
 * @author Romain Ducout
 */
import RequestManager from 'managers/request-manager';
import Utils from 'utils/utils';

/**
 * Model class to represents a flight's image taken by the drone.
 */
class Image{
    /**
     * Creates an instance of Image.
     * @param {string} iFlightId - Id of the flight.
     * @param {string} iId - Id of the image.
     */
    constructor(iFlightId, iId){
        /**
         * Flight identifier corresponding to the image. 
         * @private
         * @member {string} 
         * */
        this.flight_id = iFlightId;
        /**
         * Image identifier 
         * @private
         * @member {string} 
         * */
        this.id = iId;
    }

    /**
     * Loads an image's data.
     * 
     * @param {boolean} [iExif] - True to request exif information (default to false).
     * @returns {Promise} Promise to the image's data.
     */
    load(iExif){
        var context = this;

        if(!iExif){
            iExif = false;
        }

        var url = `${context.getDataUrl()}?exif=${iExif}`
        return new Promise(function(resolve, reject){
            RequestManager.get(url, {type: 'json'})
                .then(res => {
                    context.flight_id = res.flight;
                    context.gps = res.gps;
                    if(res.exif){
                        context.exif = res.exif;
                    }
                    context.ind = res.ind;
                    context.type = res.type;
                    resolve(context);
                })
                .catch(err => {
                    reject(err);    
                }
            );
        });
    }

    /**
     * Get image's data endpoint URL.
     * 
     * @returns {string} Image's data endpoint URL.
     */
    getDataUrl(){
        return 'api/flights/' + this.flight_id + '/images/' + this.id;
    }

    /**
     * Get image's file endpoint URL.
     * 
     * @param {number} [iSize] - Size to resize the image to.
     * @returns {string} Image's file endpoint URL.
     */
    getImageUrl(iSize){
        if(!iSize){
            return 'api/flights/' + this.flight_id + '/images/' + this.id + '/image';
        }
        return 'api/flights/' + this.flight_id + '/images/' + this.id + '/image?width=' + iSize;
    }

    /**
     * Gets the coordinates in latitude / langitude of a point on the image.
     * X and Y coordinates are expressed between 0 and 1.
     * 
     * @param {number} iX - X coordinate on the image [0, 1].
     * @param {number} iY - Y coordinate on the image [0, 1].
     * @returns {Object} Latitude and longitude of the image's point.
     */
    getCoordinates(iX, iY){
        // Hardcoded angles to be changed!
        var ang_h = 68.5 / 180.0 * Math.PI;
        var ang_v = 54.2 / 180.0 * Math.PI;

        var picture_ground_horizontal = this.gps.altitude * Math.tan(ang_h * 0.5);
        var picture_ground_vertical = this.gps.altitude * Math.tan(ang_v * 0.5);

        var rot = -Cesium.Math.toRadians(this.gps.direction);
        return Utils.convertMetersToLatLong(
            Utils.rotate({
                x: -(iX * 2 - 1) * picture_ground_horizontal, 
                y: (iY * 2 - 1) * picture_ground_vertical
            }, rot), 
            this.gps
        );
    }

    /**
     * Gets coordinates in latitude / longitude for each corners of the image.
     * Result is an object of:
     * - tl: top left corner.
     * - tr: top right corner.
     * - bl: bottom left corner.
     * - br: bottom right corner.
     * 
     * @returns {Object} Coordinates by corner of image.
     */
    getBoundingCoordinates(){
        return {
            tl: this.getCoordinates(0, 0),
            tr: this.getCoordinates(1, 0),
            bl: this.getCoordinates(0, 1),
            br: this.getCoordinates(1, 1),
        };
    }
};

export default Image;
