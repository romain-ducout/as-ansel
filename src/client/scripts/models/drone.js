/**
 * Module of the drone model class.
 * 
 * @requires module:managers/event-manager
 * @requires module:managers/request-manager
 * 
 * @module models/drone
 * @author Romain Ducout
 */
import EventManager from 'managers/event-manager';
import RequestManager from 'managers/request-manager';

/**
 * Singleton used to represents the current status of the drone.
 */
class Drone{
    /**
     * Creates an instance of Drone.
     */
    constructor(){
        /**
         * Drone data retrieved from server. 
         * @private
         * @member {Object} 
         * */
        this.data = null;
        this.setup();
    }

    /**
     * Loads drone state.
     * 
     * @returns {Promise} Promise of the drone state.
     */
    load(){
        var context = this;
        return new Promise(function(resolve, reject){
            RequestManager.get('api/drone', {type: 'json'})
                .then(res => {
                    context.data = res;
                    resolve(context);
                })
                .catch(err => {
                    reject(err);    
                }
            );
        });
    }

    /**
     * Gets drone's data.
     * 
     */
    getData(){            
        return this.data;
    }

    /**
     * Setup socket connection to drone.
     */
    setup(){
        var context = this;

        var socket = io(); 
        socket.on('drone', function(message) {
            context.data = message;
            context.event_manager.raise('onChange', [context]); 
        });
        
        this.event_manager = new EventManager();
    }

    /**
     * Connects a callback method to drone's change events.
     * 
     * @param {Function} iCallback Callback method to be called on change event.
     */
    connect(iCallback){
        this.event_manager.connect('onChange', iCallback);
    }

    /**
     * Disconnects a callback method to drone's change events.
     * 
     * @param {Function} iCallback Callback method to disconnect to event.
     */
    disconnect(iCallback){
        this.event_manager.disconnect('onChange', iCallback);
    }

    /**
     * Get armed status of drone.
     * 
     * @returns {boolean} True if drone is armed.
     */
    isArmed(){
        if(!this.data){
            return false;
        }
        return this.data.armed;
    }
};

export default new Drone();
