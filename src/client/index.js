/**
 * @file Root file of Ansel client application.
 * 
 * @requires module:ansel
 * @requires module:utils/utils
 * 
 * @author Romain Ducout
 */
import Ansel from 'ansel';
import Utils from 'utils/utils';

// Starts application after page baing loaded.
window.onload = function(){
    Utils._ANSEL_APP_ = new Ansel();
    Utils._ANSEL_APP_.init();
};