/**
 * Utility module for querying path to access application data.
 * 
 * @requires module:utils/path
 * 
 * @module utils/utils
 * @author Romain Ducout
 */

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');


/**
 * Utility class for Ansel server application.
 */
class Path{
    /**
     * Creates an instance of path.
     */
    constructor() {
        /**
         * Root path of the application. 
         * @private
         * @member {String} 
         * */
        this.app_path = '/home/aerosense/as-ansel';
        /**
         * Root path where images of flights are stored. 
         * @private
         * @member {String} 
         * */
        this.images_path = '/media/TransferJet';
        /**
         * Root path of Ansel data. 
         * @private
         * @member {String} 
         * */
        this.data_path = `${this.images_path}/as-ansel`;
    }

    /**
     * Apply Ansel configuration file to path.
     * @param {Object} iConf - Ansel configuration.
     */
    applyConf(iConf){
        if(!iConf){ return; }

        if(iConf.APP_PATH){
            this.app_path = iConf.APP_PATH;
        }
        if(iConf.IMAGES_PATH){
            this.images_path = iConf.IMAGES_PATH;
        }
        if(iConf.DATA_PATH){
            this.data_path = iConf.DATA_PATH;
        }
    }

    /**
     * Initializes folder structure required by Ansel if not already existing.
     */
    createDataFolders(){
        if (!fs.existsSync(this.getDataPath())){
            fs.mkdirSync(this.getDataPath());
        }
        if (!fs.existsSync(this.getDataTMPPath())){
            fs.mkdirSync(this.getDataTMPPath());
        }
        if (!fs.existsSync(this.getDataLogsPath())){
            fs.mkdirSync(this.getDataLogsPath());
        }
        if (!fs.existsSync(this.getDataFlightsPath())){
            fs.mkdirSync(this.getDataFlightsPath());
        }
    }

    /**
     * Gets root path of the application.
     * 
     * @static
     * @returns {String} Root path of the application.
     */
    getAppPath(){
        return this.app_path;
    }
    /**
     * Gets path to folder that stores the drone's images.
     * 
     * @static
     * @returns {String} Path of drone images.
     */
    getImagesPath(){
        return this.images_path;
    }
    /**
     * Gets path to application data.
     * 
     * @static
     * @returns {String} Path of application data.
     */
    getDataPath(){
        return this.data_path;
    }

    /**
     * Gets path to application scripts.
     * 
     * @static
     * @returns {String} Path of application scripts.
     */
    getAppScriptsPath(){
        return `${this.getAppPath()}/scripts`;
    }

    /**
     * Gets temporary application folder path.
     * 
     * @static
     * @returns {String} Temporary application folder path.
     */
    getDataTMPPath(){
        return `${this.getDataPath()}/tmp`;
    }
    /**
     * Gets logs extracted from PX4 folder path.
     * 
     * @static
     * @returns {String} Logs extracted from PX4 folder path.
     */
    getDataLogsPath(){
        return `${this.getDataPath()}/logs`;
    }
    /**
     * Gets folder path for flight data.
     * 
     * @static
     * @returns {String} Folder path for flight data.
     */
    getDataFlightsPath(){
        return `${this.getDataPath()}/flights`;
    }
    /**
     * Gets Ansel settings configuration path.
     * 
     * @static
     * @returns {String} Ansel settings configuration path.
     */
    getDataSettingsPath(){
        return `${this.getDataPath()}/ansel.json`;
    }
};

module.exports = new Path();
