/**
 * Application constants for Ansel server application.
 * 
 * @module utils/constants
 * @author Romain Ducout
 */

/**
 * Path module.
 * Allows path operation.
 * @const
 */
const path = require('path');

/**
 * Application root path.
 * 
 * @private
 * @type {string}
 */
var APP_PATH = '/home/aerosense/as-ansel';

/**
 * Path where drone data are stored.
 * 
 * @private
 * @type {string}
 */
var DRONE_DATA_PATH = '/media/TransferJet';

/**
 * Application data path.
 * 
 * @private
 * @type {string}
 */
var ANSEL_DATA_PATH = `${DRONE_DATA_PATH}/as-ansel`;

/**
 * Constant application object.
 * 
 * @type {Object}
 * @property {Object} ANSEL_IO Socket IO of application.
 * @property {number} SERVER_PORT Port number of application.
 * @property {boolean} HTTPS True to run Ansel with HTTPS.
 * @property {string} HOSTAPD_CONF Hostapd configuration file path.
 * 
 * @property {Object} ROS ROS constants.
 * @property {string} ROS.URL URL of ROS bridge.
 * @property {string} ROS.PATH Path to ROS workspace.
 * 
 * @property {Object} PX4 PX4 constants.
 * @property {string} PX4.DEV Path to usb connected to PX4.
 * @property {string} PX4.SPEED Baudrate of connection to PX4.
 * @property {string} PX4.UTILS_SCRIPT_PATH Utility python script for PX4.
 * @property {string} PX4.LOGS_DUMP_SCRIPT_PATH Log dump script for PX4.
 * @property {string} PX4.LOGS_PATH Log path on PX4.
 */
const Constants = {

    ANSEL_IO: null,
    
    SERVER_PORT: 4200,
    HTTPS: false,
    HOSTAPD_CONF: '/etc/hostapd/hostapd.conf',

    SCRIPTS: {
        MARKER_DETECTION_FOLDER: '/home/aerosense/marker-detection/python',
        MARKER_DETECTION_SCRIPT: 'marker_detector.py'
    },

    ROS: {
        URL: 'ws://localhost:9090',
        PATH: '/home/aerosense/as-ros/workspace',
    },

    PX4: {
        DEV: '/dev/ttyACM0',
        SPEED: '115200',
        UTILS_SCRIPT_NAME: `px4_utils.py`,
        LOGS_DUMP_SCRIPT_NAME: `sdlog2_dump.py`,
        LOGS_PATH: '/fs/microsd/log',
    },

    ANSEL_ENV: {
        AUTOJUMP_DATA_DIR: '/home/aerosense/.local/share/autojump',
        CMAKE_PREFIX_PATH: '/home/aerosense/as-ros/workspace/devel:/opt/ros/kinetic',
        CPATH: '/home/aerosense/as-ros/workspace/devel/include:/opt/ros/kinetic/include',
        LD_LIBRARY_PATH: '/home/aerosense/as-ros/workspace/devel/lib:/opt/ros/kinetic/lib',
        PKG_CONFIG_PATH: '/home/aerosense/as-ros/workspace/devel/lib/pkgconfig:/opt/ros/kinetic/lib/pkgconfig',
        PYTHONPATH: '/home/aerosense/as-ros/workspace/devel/lib/python2.7/dist-packages:/opt/ros/kinetic/lib/python2.7/dist-packages',
        PATH: '/opt/ros/kinetic/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games',
        ROS_DISTRO: 'kinetic',
        ROS_PACKAGE_PATH: '/home/aerosense/as-ros/workspace/src:/opt/ros/kinetic/share:/opt/ros/kinetic/stacks',
        PROS_ETC_DIRATH: '/opt/ros/kinetic/etc/ros',
        ROS_ROOT: '/opt/ros/kinetic/share/ros',
        ROS_MASTER_URI: 'http://localhost:11311',
        ROSLISP_PACKAGE_DIRECTORIES: '/home/aerosense/as-ros/workspace/devel/share/common-lisp'
    }
};

module.exports = Constants;
