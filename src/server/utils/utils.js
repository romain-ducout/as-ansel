/**
 * Utility module for Ansel server application.
 * 
 * @requires module:utils/constants
 * 
 * @module utils/utils
 * @author Romain Ducout
 */
const Constants = require(__ansel_root + 'utils/constants');

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');

/**
 * Path module.
 * Allows path operation.
 * @const
 */
const path = require('path');

/**
 * ROSLIB module.
 * Allows connection to ROS.
 * @const
 */
const ROSLIB = require('roslib');

/**
 * Promise of ROS bridge connection.
 * 
 * @private
 * @type {Promise}
 */
const ROS_PROMISE = new Promise(function(resolve, reject){
    var connectROS = function(){
        ros = new ROSLIB.Ros({
            url : Constants.ROS.URL,
            encoding: 'ascii'
        });
    
        ros.on('connection', function() {
            resolve(ros);
        });
    
        ros.on('error', function(error) {
            setTimeout(() => {
                connectROS();                
            }, 1000);
        });
    };

    connectROS();
});

/**
 * Utility class for Ansel server application.
 */
class Utils{
    /**
     * Gets promise of ROS connection.
     * 
     * @static
     * @returns {Promise} Promise to ROSLIB connected object.
     */
    static getROS(){
        return ROS_PROMISE;
    }

    /**
     * Converts DMS coordinates to Decimal coordinates.
     * 
     * @static
     * @param {Object} iCoordinates - Coordinates in DMS format.
     * @returns {Object} Coordinates in decimal format.
     */
    static convertsDMSToDecimalCoordinates(iCoordinates){
        var latpos = iCoordinates.latitude.position;
        var lngpos = iCoordinates.longitude.position;
        var latitude = latpos[0] + (latpos[1] / 60) + (latpos[2] / 3600);
        var longitude = lngpos[0] + (lngpos[1] / 60) + (lngpos[2] / 3600);

        if(iCoordinates.latitude.orientation === 'S'){
            latitude *= -1;
        }
        if(iCoordinates.longitude.orientation === 'S'){
            longitude *= -1;
        }

        return {
            latitude: latitude,
            longitude: longitude
        };
    }
    
    /**
     * Converts Decimal coordinates to DMS coordinates.
     * 
     * @static
     * @param {Object} iCoordinates - Coordinates in decimal format.
     * @returns {Object} Coordinates in DMS format.
     */
    static latitudeDecimalToDMS(iCoordinates){
        var decimalLat = iCoordinates.latitude;
        var decimalLng = iCoordinates.latitude;

        var dmsLat = {
            position: [0, 0, 0],
            orientation: null
        };
        var dmsLng = {
            position: null,
            orientation: null
        };

        dmsLat.position[0] = Math.round(decimalLat);
        dmsLat.position[1] = Math.round((decimalLat - dmsLat.position[0]) * 60);
        dmsLat.position[2] = (decimalLat - dmsLat.position[0] - dmsLat.position[1] / 60.0) * 3600;

        dmsLat.orientation = 'S';
        if(decimalLat >= 0){
            dmsLat.orientation = 'N';
        }

        dmsLng.position[0] = Math.round(decimalLng);
        dmsLng.position[1] = Math.round((decimalLng - dmsLng.position[0]) * 60);
        dmsLng.position[2] = (decimalLng - dmsLng.position[0] - dmsLng.position[1] / 60.0) * 3600;

        dmsLng.orientation = 'W';
        if(decimalLng >= 0){
            dmsLng.orientation = 'E';
        }

        return {
            latitude: dmsLat,
            longitude: dmsLng
        };
    }

    /**
     * Deletes a folder from the disc and its content recursively.
     * 
     * @static
     * @param {Object} iPath - Path of folder to delete.
     */
    static deleteFolderRecursive(iPath) {
        if(fs.existsSync(iPath)) {
            fs.readdirSync(iPath).forEach(function(file, index){
                var cur_path = path.join(iPath, file);
                if(fs.lstatSync(cur_path).isDirectory()) {
                    deleteFolderRecursive(cur_path);
                } else {
                    fs.unlinkSync(cur_path);
                }
            });
            fs.rmdirSync(iPath);
        }
    }

};

module.exports = Utils;
