/**
 * Flight model module.
 * This model represents a flight previously performed by the drone.
 * 
 * @requires module:models/image
 * @requires module:utils/path
 * @requires module:utils/utils
 * 
 * @module models/flight
 * @author Romain Ducout
 */
const Image = require(__ansel_root + 'models/image');
const Path = require(__ansel_root + 'utils/path');
const Utils = require(__ansel_root + 'utils/utils');

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');

/**
 * Path module.
 * Allows path operation.
 * @const
 */
const path = require('path');

/**
 * Model class to represent a flight made by the drone.
 */
class Flight{
    /**
     * Creates an instance of Flight.
     * @param {string} iId - Id of the flight.
     */
    constructor(iId) {
        /**
         * Flight data. 
         * @private
         * @property {string} id Flight identifier.
         * @property {Array.<module:models/image~Image[]>} images List of images taken during flight separated by type.
         * @property {Array} markers List of markers detected in flight.
         * @member {Object} 
         * */
        this.data = {
            id: iId,
            images: [],
            markers: null
        };
        this.data.date = this.computeDate();
    }

    /**
     * Returns the data of the flight.
     * 
     * @returns {Object} Data of the flight.
     */
    getData(){
        return this.data;
    }

    /**
     * Parses the flight's folder and returns a promise for when the parsing is complete.
     * 
     * @returns {Promise} Promise of the flight parsed.
     */
    parse(){
        var context = this;
        return new Promise(function(resolve, reject) {
            context.parseImages()
                .then(function(){
                    context.parseData()
                        .then(function(){
                            resolve(context);
                        })
                        .catch(function(){
                            reject();
                        });
                })
                .catch(function(){
                    reject();
                });
        });
    }

    /**
     * Parses the images of the flight and returns a promise for when the parsing is complete.
     * 
     * @returns {Promise} Promise of the flight's images parsed.
     */
    parseImages(){
        var context = this;
        return new Promise(function(resolve, reject) {
            let flight_path = context.getPath();
            fs.exists(flight_path, (exists) => {
                if(!exists){ reject(); }

                fs.readdir(flight_path, (err, files) => {
                    if(err){
                        return null;
                    }
                    var res = [];

                    files.forEach(curr_file => {
                        if(
                            fs.statSync(path.join(flight_path, curr_file)).isFile() &&
                            curr_file.toLowerCase().endsWith('jpg')
                        ){
                            let image = new Image(context.data.id, curr_file);
                            let image_type = image.getType();
                            let image_ind = image.getInd();
                            if(!context.data.images[image_type]){
                                context.data.images[image_type] = [];
                            }

                            context.data.images[image_type][image_ind] = curr_file;
                        }
                    });

                    resolve();
                });
            })    
        });
    }

    /**
     * Parses the data of the flight and returns a promise for when the parsing is complete.
     * 
     * @returns {Promise} Promise of the flight's data parsed.
     */
    parseData(){
        var context = this;
        return new Promise(function(resolve, reject) {
            let flight_data_path = context.getDataPath();
            fs.exists(flight_data_path, (exists) => {
                if(!exists){ resolve(); }
                let marker_file_path = `${flight_data_path}/markers.json`;
                if(fs.existsSync(marker_file_path)){
                    context.data.markers = require(`${flight_data_path}/markers.json`);
                }
                resolve();
                
            })    
        });
    }

    /**
     * Gets the path to the flight's folder.
     * 
     * @returns {string} Path to the flight folder.
     */
    getPath(){
        return path.join(Path.getImagesPath(), this.getId());
    }

    /**
     * Gets the path to the flight's data folder.
     * 
     * @returns {string} Path to the flight's data folder.
     */
    getDataPath(){
        return path.join(Path.getDataFlightsPath(), this.getId());
    }

    /**
     * Get id of the flight.
     * 
     * @returns {string} Id of the flight.
     */
    getId(){
        return this.data.id;
    }

    /**
     * Compute the date of the flight based on its id.
     * Returns null in case of the id not corresponding to a date.
     * 
     * @returns {Date} Date of the flight, null if id is not a date.
     */
    computeDate(){
        var pattern = /(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})*/;
        return new Date(this.data.id.replace(pattern,'$1-$2-$3T$4:$5'));
    }

    /**
     * Get list of flights' ids present on the drone.
     * 
     * @static
     * @returns {Promise} Promise to array of flights' ids present on drone..
     */
    static getFlights(){
        return new Promise(function(resolve, reject) {
            fs.readdir(Path.getImagesPath(), (err, files) => {
                var res = [];
                files.forEach(curr_file => {
                    if(curr_file === 'as-ansel'){ return; }
                    if(fs.statSync(path.join(Path.getImagesPath(), curr_file)).isDirectory()){
                        res.push(curr_file);
                    }
                });
    
                resolve(res);
            })
        });
    }

    /**
     * Get a flight based on its id.
     * 
     * @static
     * @param {string} iId - Id of flight to retrieve.
     * @returns {Flight} Flight corresponding to id.
     */
    static getFlight(iId){
        var flight = new Flight(iId);
        return flight.parse();
    }

    /**
     * Deletes a flight based on its id.
     * 
     * @static
     * @param {string} iId - Id of flight to delete.
     * @returns {Promise} Promise of flight deleted.
     */
    static deleteFlight(iId){
        return new Promise(function(resolve, reject) {
            var flight = new Flight(iId);
            Utils.deleteFolderRecursive(flight.getPath());
            Utils.deleteFolderRecursive(flight.getDataPath());
            resolve();
        });
    }
};

module.exports = Flight;
