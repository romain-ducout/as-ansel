/**
 * Drone model module.
 * This model represents the current state of the drone.
 * 
 * @requires module:utils/constants
 * @requires module:utils/path
 * @requires module:utils/utils
 * 
 * @module models/drone
 * @author Romain Ducout
 */
const Constants = require(__ansel_root + 'utils/constants');
const Path = require(__ansel_root + 'utils/path');
const Utils = require(__ansel_root + 'utils/utils');

/**
 * ROSLIB module.
 * Allows connection to ROS.
 * @const
 */
const ROSLIB = require("roslib");

/**
 * Child process module.
 * Allows creating processes.
 * @const
 */
const child_process = require('child_process');

/**
 * Model class to represent the drone's state.
 */
class Drone{
    /**
     * Creates an instance of Drone.
     */
    constructor() {
        var context = this;

        /**
         * Drone data. 
         * @private
         * @property {string} model Drone model name.
         * @property {Object} gps Drone current GPS position.
         * @property {Object} mission Drone current mission state.
         * @property {Object} camera Drone camera status.
         * @property {boolean} armed Drone armed state.
         * @property {string} flight Current flight name.
         * @member {Object} 
         * */
        this.data = {
            model: 'AS-MC03',
            gps: null,
            mission: null,
            camera: null,
            armed: false,
            flight: null
        };

        /**
         * ROS connection object. 
         * @private
         * @member {Object} 
         * */
        this.ros = null;

        Utils.getROS()
            .then(ros => {
                context.ros = ros;
                context.onConnectedROS();                
            })
            .catch(ros => {             
            });
    }

    /**
     * Binds ROS events to JavaScript functions.
     */
    onConnectedROS(){
        this.global_position_listener = new ROSLIB.Topic({
            ros : this.ros,
            name : '/mavlink_interface/DroneGlobalPosition',
            messageType : 'mavlink_interface/DroneGlobalPosition'
        });

        this.attitude_listener = new ROSLIB.Topic({
            ros : this.ros,
            name : '/mavlink_interface/DroneAttitude',
            messageType : 'mavlink_interface/DroneAttitude'
        });

        this.mission_state_listener = new ROSLIB.Topic({
            ros : this.ros,
            name : '/mavlink_interface/DroneMissionState',
            messageType : 'mavlink_interface/DroneMissionState'
        });

        this.camera_state_listener = new ROSLIB.Topic({
            ros : this.ros,
            name : '/camera/CameraState',
            messageType : 'camera_interface/CameraState'
        });

        this.flight_update_listener = new ROSLIB.Topic({
            ros : this.ros,
            name : '/exif_writer/ExifWritten',
            messageType : 'exif_writer/ExifWritten'
        });
        
        this.mission_start_listener = new ROSLIB.Topic({
            ros : this.ros,
            name : '/mavlink_interface/DroneMissionStart',
            messageType : 'mavlink_interface/DroneMissionStart'

        });

        this.global_position_listener.subscribe(this.onReceiveDroneGlobalPosition.bind(this));
        this.attitude_listener.subscribe(this.onReceiveDroneAttitude.bind(this));
        this.mission_state_listener.subscribe(this.onReceiveMissionState.bind(this));
        this.camera_state_listener.subscribe(this.onReceiveCameraState.bind(this));
        this.flight_update_listener.subscribe(this.onReceiveExifWritten.bind(this));
        this.mission_start_listener.subscribe(this.onReceiveCameraStart.bind(this));
    }

    /**
     * Transmit drone state when global position changed.
     * 
     * @param {Object} iMessage - ROS message.
     */
    onReceiveDroneGlobalPosition(iMessage){
        this.data.gps = {
            latitude: iMessage.lat,
            longitude: iMessage.lng,
            altitude: iMessage.alt,
            direction: iMessage.heading
        };        

        this.push();
    }

    /**
     * Transmit drone state when attitude changed.
     * 
     * @param {Object} iMessage - ROS message.
     */
    onReceiveDroneAttitude(iMessage){
        this.data.attitude = iMessage;
        this.push();
    }

    /**
     * Transmit drone state when mission state changed.
     * 
     * @param {Object} iMessage - ROS message.
     */
    onReceiveMissionState(iMessage){
        this.data.mission = {
            current: iMessage.seq,
            total: iMessage.count
        };
        this.data.armed = iMessage.armed;

        if(!this.data.armed){
            this.data.flight = null;
            this.data.mission = null;
        }
        this.push();
    }

    /**
     * Transmit drone state when camera state changed.
     * 
     * @param {Object} iMessage - ROS message.
     */
    onReceiveCameraState(iMessage){
        this.data.camera = iMessage;
        this.push();
    }

    /**
     * Handle start of camera event received from ROS.
     * 
     * @param {Object} iMessage - ROS message.
     */
    onReceiveCameraStart(iMessage){
        // Test only
        child_process.exec(`ffmpeg -f video4linux2 -s 640x480 -i /dev/video1 ${Path.getDataTMPPath()}/${new Date().getTime()}.thermal.avi`, (err, stdout, stderr) => {
           
        });
    }

    /**
     * Transmit drone flight when a new image has been written.
     * 
     * @param {Object} iMessage - ROS message.
     */
    onReceiveExifWritten(iMessage){
        this.data.flight = iMessage;
        this.push();
    }

    /**
     * Push drone state to client.
     */
    push(){
        if(!Constants.ANSEL_IO) { return; }        
        Constants.ANSEL_IO.sockets.emit('drone', this.getData());
    }

    /**
     * Returns drone state.
     * 
     * @returns {Object} drone state.
     */
    getData(){
        return this.data;
    }
};

module.exports = new Drone();
