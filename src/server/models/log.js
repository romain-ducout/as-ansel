/**
 * PX4 log model module.
 * This model represents a log from PX4.
 * 
 * @requires module:utils/path
 * @requires module:utils/utils
 * 
 * @module models/log
 * @author Romain Ducout
 */
const Path = require(__ansel_root + 'utils/path');
const Utils = require(__ansel_root + 'utils/utils');

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');

/**
 * Path module.
 * Allows path operation.
 * @const
 */
const path = require('path');

/**
 * Pattern of a log name.
 * 
 * @private
 * @type {RegExp}
 */
const LOG_NAME_PATTERN = /(\d{4})-(\d{2})-(\d{2})-(\d{2})_(\d{2})_(\d{2})*/;

/**
 * Model class of logs extracted from PX4.
 */
class Log{
    /**
     * Creates an instance of Log.
     * @param {string} iId - Id of log.
     */
    constructor(iId) {
        /**
         * Log identifier. 
         * @private
         * @member {string} 
         * */
        this.id = iId;
        /**
         * Log's date. 
         * @private
         * @member {string} 
         * */
        this.date = this.computeName();
        /**
         * Log's name. 
         * @private
         * @member {string} 
         * */
        this.name = this.computeDate();
        /**
         * Log's deployed state. 
         * @private
         * @member {boolean} 
         * */
        this.deployed = false;

        this.init();
    }

    /**
     * Initilizes the log object based on files on disk.
     */
    init(){
        var folder_path = path.join(Path.getDataLogsPath(), this.id);
        try {
            this.deployed = fs.statSync(folder_path).isDirectory();
            if(this.deployed){
                this.urls = {
                    data: `./logs/${this.id}/${this.id}.csv`,
                    params: `./logs/${this.id}/${this.id}.txt`
                }
            }
        } catch (error) {  
        }
    }

    /**
     * Computes the name of the log based on id.
     * 
     * @returns {string} Name of log.
     */
    computeName(){
        return this.id.replace(LOG_NAME_PATTERN,'$1-$2-$3T$4:$5:$6');        
    }

    /**
     * Computes the date of the log based on id.
     * 
     * @returns {string} Date of log.
     */
    computeDate(){
        return this.id.replace(LOG_NAME_PATTERN,'$1-$2-$3 $4:$5:$6');
    }
    
    /**
     * Get a promise of a log based on its id.
     * 
     * @static
     * @param {string} iId - Id of log to retrieve.
     * @returns {Promise} Promise of the log.
     */
    static getLog(iId){
        return new Promise(function(resolve, reject){
            resolve(new Log(iId));
        });         
    }

    /**
     * Get list of flights' logs present on the drone.
     * 
     * @static
     * @returns {Promise} Promise to array of logs present on drone..
     */
    static getLogs(){
        return new Promise(function(resolve, reject){
            fs.readdir(Path.getDataLogsPath(), (err, files) => {
                var res = [];
                files.forEach(curr_file => {
                    if(fs.statSync(path.join(Path.getDataLogsPath(), curr_file)).isDirectory()){
                        res.push(new Log(curr_file));
                    }
                });

                resolve(res);
            })
        });
    }

    /**
     * Deletes a log based on its id.
     * 
     * @static
     * @param {string} iId - id of log to delete.
     * @returns {Promise} Promise of log deleted.
     */
    static deletetLog(iId){
        return new Promise(function(resolve, reject){
            var log = new Log(iId);

            if(!log.deployed){
                reject();
                return;
            }

            var log_path = path.join(Path.getDataLogsPath(), iId);
            Utils.deleteFolderRecursive(log_path);
            resolve();
        });
    }
};

module.exports = Log;
