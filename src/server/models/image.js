/**
 * Image model module.
 * This model represents an image taken by the drone during a previous flight.
 * 
 * @requires module:utils/path
 * @requires module:utils/utils
 * 
 * @module models/image
 * @author Romain Ducout
 */
const Path = require(__ansel_root + 'utils/path');
const Utils = require(__ansel_root + 'utils/utils');

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');

/**
 * Path module.
 * Allows path operation.
 * @const
 */
const path = require('path');

/**
 * Sharp module.
 * Image operations (resize).
 * @const
 */
const sharp = require('sharp');

/**
 * EXIF module.
 * Extract exif data from images.
 * @const
 */
const exif = require('fast-exif');

/**
 * Types of images taken by drone.
 * - COLOR: Colored image taker by QX30 / R10C.
 * - THERMAL1: Black and white version of thermal image.
 * - THERMAL2: Jet colored version of thermal image.
 * 
 * @readonly
 * @private
 * @enum {number}
 */
const IMAGE_TYPES = {
    COLOR: 0,
    THERMAL1: 1,
    THERMAL2: 2,
};

/**
 * Model class of images of a flight made by the drone.
 */
class Image{
    /**
     * Creates an instance of Image.
     * @param {string} iFlightId - Id of flight.
     * @param {string} iId - Id of image.
     */
    constructor(iFlightId, iId) {
        /**
         * Image data. 
         * @private
         * @property {string} flight Flight identifier of image.
         * @property {string} id Image identifier (file name).
         * @property {number} ind Indice of the image in its flight.
         * @property {IMAGE_TYPES} type Type of image.
         * @property {Object} gps GPS data.
         * @property {number} gps.latitude GPS latitude coordinate of image.
         * @property {number} gps.longitude GPS longitude coordinate of image.
         * @property {number} gps.altitude Altitude of image.
         * @property {number} gps.direction Direction of image.
         * @property {Object} exif Full EXIF data of image.
         * @member {Object} 
         * */
        this.data = {
            flight: iFlightId,
            id: iId,
            ind: 0,
            type: 0,
            gps: {
                latitude: null,
                longitude: null,
                altitude: null,
                direction: null
            },
            exif: null
        };

        this.data.ind = Image.computeImageInd(this.data.id);
        this.data.type = Image.computeImageType(this.data.id);
    }

    /**
     * Gets the image's data.
     * 
     * @returns {Object} Image's data.
     */
    getData(){
        return this.data;
    }

    /**
     * Gets the image's type.
     * 
     * @returns {number} Image's type.
     */
    getType(){
        return this.data.type;
    }

    /**
     * Gets the image's indice in flight.
     * 
     * @returns {number} Image's indice in flight.
     */
    getInd(){
        return this.data.ind;
    }

    /**
     * Gets the image's path on disk.
     * 
     * @returns {string} Image's path on disl.
     */
    getPath(){
        return path.join(Path.getImagesPath(), this.data.flight, this.data.id);
    }

    /**
     * Computes the data of an image and return a promise of it.
     * 
     * @param {boolean} iComputesExif - True to add exif information to data.
     * @returns {Promise} Promise to image data computed.
     */
    computeData(iComputesExif){
        var context = this;
        this.data.gps = {
            latitude: null,
            longitude: null,
            altitude: null,
            direction: null
        };
        return new Promise(function(resolve, reject) {
            let image_path = context.getPath();
            fs.exists(image_path, (file_exists) => {
                if(!file_exists){
                    reject();
                    return;
                }
                try {
                    exif.read(image_path)
                        .then((data)=>{
                            var t1 = new Date();
                            if(!data.gps){
                                reject();
                                return;
                            }

                            let coords = Utils.convertsDMSToDecimalCoordinates({
                                latitude: {
                                    position: data.gps.GPSLatitude,
                                    orientation: data.gps.GPSLatitudeRef
                                },
                                longitude:{
                                    position: data.gps.GPSLongitude,
                                    orientation: data.gps.GPSLongitudeRef
                                }
                            });
                            
                            context.data.gps = {
                                latitude: coords.latitude,
                                longitude: coords.longitude,
                                altitude: data.gps.GPSAltitude,
                                direction: data.gps.GPSImgDirection
                            };
                            if(iComputesExif){
                                context.data.exif = data;
                            }
                            resolve();
                        })
                        .catch((err)=>{
                            reject();
                        });
                } catch (error) {
                    reject();
                }
            }) 
        });
    }

    /**
     * Get image file corresponding to object.
     * Image can be resized by given width.
     * 
     * @param {number} [iWidth] - Width to resize the image to.
     * @returns {Promise} Promise of image.
     */
    getImageFile(iWidth){
        var context = this;
        return new Promise(function(resolve, reject) {
            let image_path = context.getPath();
            fs.exists(image_path, (file_exists) => {
                if(!file_exists){
                    reject();
                    return;
                }

                if(iWidth){
                    var width = parseInt(iWidth);
                    sharp(image_path)
                        .resize(width)
                        .toBuffer()
                        .then( data => {
                            resolve(data);
                        });
                }
                else{
                    sharp(image_path)
                        .toBuffer()
                        .then( data => {
                            resolve(data);
                        });
                }
            }) 
        });
    }

    /**
     * Computes the type of an image based on its id.
     * 
     * @static
     * @param {string} iId - Id of image.
     * @returns {number} Type of image
     */
    static computeImageType(iId){
        try {
            var split = iId.split(".");
            if(split.length <=2){
                return IMAGE_TYPES.COLOR;
            }
            else{
                if(split[1] === 'thermal-1'){
                    return IMAGE_TYPES.THERMAL1;
                }
                if(split[1] === 'thermal-2'){
                    return IMAGE_TYPES.THERMAL2;
                }
            }          
        } catch (error) {     
        }   
        return IMAGE_TYPES.COLOR;     
    }

    /**
     * Computes the indice of an image based on its id.
     * 
     * @static
     * @param {string} iId - Id of image.
     * @returns {number} Indice of image
     */
    static computeImageInd(iId){
        var res = 0;
        try {
            res = iId.split("_AS")[1];
            return parseInt(res.split(".")[0]);
        } catch (error) {
            try {
                res = iId.split("picture")[1];
                return parseInt(res.split(".")[0]);
            } catch (error) {                
            }      
        }
        return res;
    }
};

module.exports = Image;
