#ifndef DEF_STREAMER
#define DEF_STREAMER

#include <nan.h>
#include <string>

#include <gst/gst.h>

using namespace std;

class Streamer: public Nan::ObjectWrap{
public:    
    static NAN_MODULE_INIT(Init);
    
    static NAN_METHOD(New);

    static NAN_METHOD(start);
    static NAN_METHOD(stop);
    static NAN_METHOD(refresh);
    static NAN_METHOD(setBitrate);

    static Nan::Persistent<v8::FunctionTemplate> constructor;

    ~Streamer();

private:
    GstElement *m_pipeline;
    GstBus *m_bus;

    void start(const char* iTarget, const char* device);
    void startRec(const char* iTarget, const char* device, const char* rec_target);
    void stop();
    void refresh();
    void setBitrate();

    void initPipeline(const char* iTarget, const char* device);
    void initPipelineRec(const char* iTarget, const char* device, const char* rec_target);
    void initTestPipeline(const char* iTarget);

    void handleMessage (GstMessage *msg);
};


#endif