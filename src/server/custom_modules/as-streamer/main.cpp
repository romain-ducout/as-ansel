#include <nan.h>

#include "streamer.hpp"


NAN_MODULE_INIT(InitModule) {
    Streamer::Init(target);
}

NODE_MODULE(asstreamer, InitModule);
