{
    "targets": [
        {
            "target_name": "as-streamer",
            "sources": [ "main.cpp", "streamer.cpp" ],
            "include_dirs" : [
                "<!(node -e \"require('nan')\")",
				"<!@(pkg-config gstreamer-1.0 --cflags-only-I | sed s/-I//g)"
            ],
            "cflags" : [
                "-std=c++11"
            ],
			"libraries": [
				"<!@(pkg-config gstreamer-1.0 --libs)"
			]
        }
    ]
}