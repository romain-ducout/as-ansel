#include "streamer.hpp"
#include <nan.h>

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdint.h>
using namespace std;

Nan::Persistent<v8::FunctionTemplate> Streamer::constructor;


NAN_MODULE_INIT(Streamer::Init) {
    v8::Local<v8::FunctionTemplate> ctor = Nan::New<v8::FunctionTemplate>(Streamer::New);
    constructor.Reset(ctor);
    ctor->InstanceTemplate()->SetInternalFieldCount(3);
    ctor->SetClassName(Nan::New("Streamer").ToLocalChecked());

    Nan::SetPrototypeMethod(ctor, "start", start);
    Nan::SetPrototypeMethod(ctor, "stop", stop);
    Nan::SetPrototypeMethod(ctor, "refresh", refresh);
    Nan::SetPrototypeMethod(ctor, "setBitrate", setBitrate);

    target->Set(Nan::New("Streamer").ToLocalChecked(), ctor->GetFunction());
}

NAN_METHOD(Streamer::New) {
    if(!info.IsConstructCall()) {
        return Nan::ThrowError(Nan::New("Streamer::New - called without new keyword").ToLocalChecked());
    }

    if(info.Length() != 0) {
        return Nan::ThrowError(Nan::New("Streamer::New - no expected arguments.").ToLocalChecked());
    }

    Streamer* streamer = new Streamer();
    streamer->m_pipeline = NULL;
    streamer->Wrap(info.This());

    info.GetReturnValue().Set(info.This());
}

NAN_METHOD(Streamer::start) {
    Streamer* self = Nan::ObjectWrap::Unwrap<Streamer>(info.This());

    if(info.Length() < 2) {
        return Nan::ThrowError(Nan::New("Camera::start - expected 2 arguments: ip target, device.").ToLocalChecked());
    }

    if(!info[0]->IsString()) {
        return Nan::ThrowError(Nan::New("Camera::start - ip target argument to be string.").ToLocalChecked());
    }
    if(!info[1]->IsString()) {
        return Nan::ThrowError(Nan::New("Camera::start - device argument to be string.").ToLocalChecked());
    }

    string target = *v8::String::Utf8Value(info[0]->ToString());
    string device = *v8::String::Utf8Value(info[1]->ToString());

    if(info[2]->IsString()) {
        string rec_target = *v8::String::Utf8Value(info[2]->ToString());
        self->startRec(target.c_str(), device.c_str(), rec_target.c_str());
    }
    else{
        self->start(target.c_str(), device.c_str());
    }
}

NAN_METHOD(Streamer::stop) {
    Streamer* self = Nan::ObjectWrap::Unwrap<Streamer>(info.This());
    self->stop();
}

NAN_METHOD(Streamer::refresh) {
    Streamer* self = Nan::ObjectWrap::Unwrap<Streamer>(info.This());
    self->refresh();
}

NAN_METHOD(Streamer::setBitrate) {
    Streamer* self = Nan::ObjectWrap::Unwrap<Streamer>(info.This());
    self->setBitrate();
}

void Streamer::start(const char* target, const char* device){
    if(m_pipeline == NULL){
        this->initPipeline(target, device);
    }

    GstStateChangeReturn ret = gst_element_set_state (m_pipeline, GST_STATE_PLAYING);
    if(ret == GST_STATE_CHANGE_FAILURE){
        gst_object_unref(m_pipeline);
        return;
    }
}
void Streamer::startRec(const char* target, const char* device, const char* rec_target){
    if(m_pipeline == NULL){
        this->initPipelineRec(target, device, rec_target);
    }

    GstStateChangeReturn ret = gst_element_set_state (m_pipeline, GST_STATE_PLAYING);
    if(ret == GST_STATE_CHANGE_FAILURE){
        gst_object_unref(m_pipeline);
        return;
    }
}

void Streamer::stop(){
    gst_element_set_state (m_pipeline, GST_STATE_NULL);
    gst_object_unref(m_pipeline);
    gst_object_unref(m_bus);
    m_pipeline = NULL;
}

void Streamer::refresh(){
    GstMessage *msg = gst_bus_timed_pop (m_bus, 100 * GST_MSECOND);

    gst_element_set_state (m_pipeline, GST_STATE_PAUSED);
    gst_element_set_state (m_pipeline, GST_STATE_PLAYING);
    handleMessage(msg);
}

void Streamer::setBitrate(){
}

void Streamer::initTestPipeline(const char* target){
    if(m_pipeline != NULL){
        return;
    }

    // Initialize GStreamer
    gst_init (NULL, NULL);

    /* Create the elements */
    GstElement* videotestsrc = gst_element_factory_make("videotestsrc", "videotestsrc");
    GstElement* omxh264enc = gst_element_factory_make("omxh264enc", "omxh264enc");
    GstElement* cfilter1 = gst_element_factory_make("capsfilter","cfilter1");
    GstElement* rtph264pay = gst_element_factory_make("rtph264pay", "rtph264pay");
    GstElement* udpsink = gst_element_factory_make("udpsink", "udpsink");

    char* caps_1_str = (char*)"video/x-h264, stream-format=byte-stream";

    g_object_set(videotestsrc, "pattern", 1, NULL);

    g_object_set(cfilter1, "caps", gst_caps_from_string(caps_1_str), NULL);

    g_object_set(omxh264enc, "control-rate", 2, NULL);
    g_object_set(omxh264enc, "bitrate", 4000000, NULL);

    g_object_set(udpsink, "host", target, NULL);
    g_object_set(udpsink, "port", 5000, NULL);
    
    // Create pipeline
    m_pipeline = gst_pipeline_new("stream_pipeline");
    m_bus = gst_element_get_bus (m_pipeline);

    gst_bin_add_many(GST_BIN(m_pipeline), 
        videotestsrc, 
        omxh264enc, 
        cfilter1, 
        rtph264pay, 
        udpsink, 
        NULL);

    if (!gst_element_link_many(
        videotestsrc, 
        omxh264enc, 
        cfilter1, 
        rtph264pay, 
        udpsink, 
        NULL)
    ) {
        g_warning("Failed to link elements");
    }

}


void Streamer::initPipeline(const char* target, const char* device){
    if(m_pipeline != NULL){
        return;
    }

    // Initialize GStreamer
    gst_init (NULL, NULL);
    
    /* Create the elements */
    GstElement* v4l2src = gst_element_factory_make("v4l2src", "v4l2src");
    GstElement* videoscale = gst_element_factory_make("videoscale","videoscale");
    GstElement* videoconvert = gst_element_factory_make("videoconvert","videoconvert");
    GstElement* videorate = gst_element_factory_make("videorate","videorate");
    GstElement* omxh264enc = gst_element_factory_make("omxh264enc", "omxh264enc");
    GstElement* cfilter2 = gst_element_factory_make("capsfilter","cfilter2");
    GstElement* h264parse = gst_element_factory_make("h264parse", "h264parse");
    GstElement* rtph264pay = gst_element_factory_make("rtph264pay", "rtph264pay");
    GstElement* udpsink = gst_element_factory_make("udpsink", "udpsink");

    char* caps_str = (char*)"video/x-h264, stream-format=(string)byte-stream";

    g_object_set(v4l2src, "device", device, NULL);

    g_object_set(omxh264enc, "control-rate", 2, NULL);
    g_object_set(omxh264enc, "bitrate", 3000000, NULL);

    g_object_set(cfilter2, "caps", gst_caps_from_string(caps_str), NULL);

    g_object_set(rtph264pay, "mtu", 1400, NULL);

    g_object_set(udpsink, "host", target, NULL);
    g_object_set(udpsink, "port", 5000, NULL);

    // Create pipeline
    m_pipeline = gst_pipeline_new("stream_pipeline");
    m_bus = gst_element_get_bus (m_pipeline);

    gst_bin_add_many(GST_BIN(m_pipeline), 
        v4l2src, 
        videoscale,
        videoconvert,
        videorate,
        omxh264enc, 
        cfilter2,
        h264parse, 
        rtph264pay, 
        udpsink, 
        NULL);

    if (!gst_element_link_many(
        v4l2src, 
        videoscale,
        videoconvert,
        videorate,
        omxh264enc, 
        cfilter2,
        h264parse, 
        rtph264pay, 
        udpsink, 
        NULL)
    ) {
        g_warning("Failed to link elements");
    }
}

void Streamer::initPipelineRec(const char* target, const char* device, const char* rec_target){
    if(m_pipeline != NULL){
        return;
    }

    // Initialize GStreamer
    gst_init (NULL, NULL);
    
    /* Create the elements */
    GstElement* v4l2src = gst_element_factory_make("v4l2src", "v4l2src");
    GstElement* videoscale = gst_element_factory_make("videoscale","videoscale");
    GstElement* videoconvert = gst_element_factory_make("videoconvert","videoconvert");
    GstElement* videorate = gst_element_factory_make("videorate","videorate");
    GstElement* tee = gst_element_factory_make("tee","tee");

    GstElement* omxh264enc = gst_element_factory_make("omxh264enc", "omxh264enc");
    GstElement* cfilter2 = gst_element_factory_make("capsfilter","cfilter2");
    GstElement* h264parse = gst_element_factory_make("h264parse", "h264parse");
    GstElement* rtph264pay = gst_element_factory_make("rtph264pay", "rtph264pay");
    GstElement* udpsink = gst_element_factory_make("udpsink", "udpsink");

    GstElement* cfilter1_rec = gst_element_factory_make("capsfilter","cfilter1_rec");
    GstElement* omxh264enc_rec = gst_element_factory_make("omxh264enc", "omxh264enc_rec");
    GstElement* cfilter2_rec = gst_element_factory_make("capsfilter","cfilter2_rec");
    GstElement* mpegpsmux_rec = gst_element_factory_make("mpegpsmux","mpegpsmux");
    GstElement* filesink = gst_element_factory_make("filesink", "filesink");

    char* caps_str = (char*)"video/x-h264, stream-format=(string)byte-stream";
    char* cfilter1_rec_str = (char*)"video/x-raw,framerate=30/1";

    g_object_set(v4l2src, "device", device, NULL);

    g_object_set(omxh264enc, "control-rate", 2, NULL);
    g_object_set(omxh264enc, "bitrate", 3000000, NULL);
    g_object_set(cfilter2, "caps", gst_caps_from_string(caps_str), NULL);
    g_object_set(rtph264pay, "mtu", 1400, NULL);
    g_object_set(udpsink, "host", target, NULL);
    g_object_set(udpsink, "port", 5000, NULL);

    g_object_set(cfilter1_rec, "caps", gst_caps_from_string(cfilter1_rec_str), NULL);
    g_object_set(omxh264enc_rec, "control-rate", 2, NULL);
    g_object_set(omxh264enc_rec, "bitrate", 5000000, NULL);    
    g_object_set(cfilter2_rec, "caps", gst_caps_from_string(caps_str), NULL);
    g_object_set(filesink, "location", rec_target, NULL);

    // Create pipeline
    m_pipeline = gst_pipeline_new("stream_pipeline");
    m_bus = gst_element_get_bus (m_pipeline);

    gst_bin_add_many(GST_BIN(m_pipeline), 
        v4l2src, 
        videoscale,
        videoconvert,
        videorate,
        tee,
        NULL);
    gst_bin_add_many(GST_BIN(m_pipeline), 
        omxh264enc, 
        cfilter2,
        h264parse, 
        rtph264pay, 
        udpsink,
        NULL);
    gst_bin_add_many(GST_BIN(m_pipeline),  
        cfilter1_rec,
        omxh264enc_rec, 
        cfilter2_rec, 
        mpegpsmux_rec,
        filesink, 
        NULL);

    if (!gst_element_link_many(
        v4l2src, 
        videoscale,
        videoconvert,
        videorate,
        tee,
        NULL)
    ) {
        g_warning("Failed to link elements");
    }
    if (!gst_element_link_many(
        omxh264enc, 
        cfilter2,
        h264parse, 
        rtph264pay, 
        udpsink, 
        NULL)
    ) {
        g_warning("Failed to link elements");
    }
    if (!gst_element_link_many(
        cfilter1_rec,
        omxh264enc_rec, 
        cfilter2_rec,
        mpegpsmux_rec,
        filesink, 
        NULL)
    ) {
        g_warning("Failed to link elements");
    }

    GstPad *tee_stream_pad = gst_element_get_request_pad(tee, "src_%u");
    GstPad *omxh264enc_stream_pad = gst_element_get_static_pad(omxh264enc, "sink");
    GstPad *tee_rec_pad = gst_element_get_request_pad(tee, "src_%u");
    GstPad *omxh264enc_rec_pad = gst_element_get_static_pad(cfilter1_rec, "sink");
    if (
        gst_pad_link (tee_stream_pad, omxh264enc_stream_pad) != GST_PAD_LINK_OK ||
        gst_pad_link (tee_rec_pad, omxh264enc_rec_pad) != GST_PAD_LINK_OK
        
    ) {
        g_warning ("Tee could not be linked.\n");
    }
}

void Streamer::handleMessage (GstMessage *msg) {
    if(msg == NULL){
        return;
    }

    cout << GST_MESSAGE_TYPE (msg) << endl;

    switch (GST_MESSAGE_TYPE (msg)) {
        case GST_MESSAGE_STATE_CHANGED: {
            break;
        }
        case GST_MESSAGE_ERROR: {
            cout<< "HandleMessage: GST_MESSAGE_ERROR"<< endl;
            GError *err;
            gchar *debug;

            gst_message_parse_error (msg, &err, &debug);
            g_print ("Error: %s\n", err->message);
            g_error_free (err);
            g_free (debug);

            gst_element_set_state (m_pipeline, GST_STATE_READY);
            this->stop();
            break;
        }
        case GST_MESSAGE_EOS:
            cout<< "HandleMessage: GST_MESSAGE_EOS"<< endl;
            gst_element_set_state (m_pipeline, GST_STATE_READY);
            this->stop();
            break;
        default:
            break;
    }
}

Streamer::~Streamer()
{
    gst_element_set_state (m_pipeline, GST_STATE_NULL);
    gst_object_unref(m_pipeline);
    gst_object_unref(m_bus);
}