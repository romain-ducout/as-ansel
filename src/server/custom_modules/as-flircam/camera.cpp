#include "camera.hpp"
#include <nan.h>

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdint.h>

#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>


#include <jpeglib.h>


using namespace std;

Nan::Persistent<v8::FunctionTemplate> Camera::constructor;


static int xioctl(int fd, int request, void *arg)
{
    for (int i = 0; i < 100; i++) {
        int r = ioctl(fd, request, arg);
        if (r != -1 || errno != EINTR) {
            return r;
        }
    }
    return -1;
}

NAN_MODULE_INIT(Camera::Init) {
    v8::Local<v8::FunctionTemplate> ctor = Nan::New<v8::FunctionTemplate>(Camera::New);
    constructor.Reset(ctor);
    ctor->InstanceTemplate()->SetInternalFieldCount(3);
    ctor->SetClassName(Nan::New("Camera").ToLocalChecked());

    Nan::SetPrototypeMethod(ctor, "getDevice", getDevice);
    Nan::SetPrototypeMethod(ctor, "getWidth", getWidth);
    Nan::SetPrototypeMethod(ctor, "getHeight", getHeight);
    Nan::SetPrototypeMethod(ctor, "getImage", getImage);
    Nan::SetPrototypeMethod(ctor, "start", start);
    Nan::SetPrototypeMethod(ctor, "stop", stop);

    target->Set(Nan::New("Camera").ToLocalChecked(), ctor->GetFunction());
}

NAN_METHOD(Camera::New) {
    // throw an error if constructor is called without new keyword
    if(!info.IsConstructCall()) {
        return Nan::ThrowError(Nan::New("Camera::New - called without new keyword").ToLocalChecked());
    }

    // expect exactly 3 arguments
    if(info.Length() < 3) {
        return Nan::ThrowError(Nan::New("Camera::New - expected arguments: device, width, height.").ToLocalChecked());
    }

    // check argument types
    if(!info[0]->IsString()) {
        return Nan::ThrowError(Nan::New("Camera::New - device argument to be string.").ToLocalChecked());
    }
    if(!info[1]->IsNumber()) {
        return Nan::ThrowError(Nan::New("Camera::New - width argument to be number.").ToLocalChecked());
    }
    if(!info[2]->IsNumber()) {
        return Nan::ThrowError(Nan::New("Camera::New - height argument to be number.").ToLocalChecked());
    }

    Camera* cam = new Camera();
    cam->device = *v8::String::Utf8Value(info[0]->ToString());
    cam->width = info[1]->NumberValue();
    cam->height = info[2]->NumberValue();
    cam->Wrap(info.This());

    info.GetReturnValue().Set(info.This());
}

NAN_METHOD(Camera::getDevice) {
    Camera* self = Nan::ObjectWrap::Unwrap<Camera>(info.This());
    info.GetReturnValue().Set(Nan::New(self->device).ToLocalChecked());
}

NAN_METHOD(Camera::getWidth) {
    Camera* self = Nan::ObjectWrap::Unwrap<Camera>(info.This());
    info.GetReturnValue().Set(Nan::New<v8::Number>(self->width));
}

NAN_METHOD(Camera::getHeight) {
    Camera* self = Nan::ObjectWrap::Unwrap<Camera>(info.This());
    info.GetReturnValue().Set(Nan::New<v8::Number>(self->height));
}

NAN_METHOD(Camera::start) {
    Camera* self = Nan::ObjectWrap::Unwrap<Camera>(info.This());

    if(!self->openDevice()){
        return Nan::ThrowError(Nan::New("Camera::start - impossible to open device.").ToLocalChecked());
    }

    if(!self->queryBuffers()){
        return Nan::ThrowError(Nan::New("Camera::start - impossible to query buffers.").ToLocalChecked());
    }
}

NAN_METHOD(Camera::stop) {
    Camera* self = Nan::ObjectWrap::Unwrap<Camera>(info.This());

    if(!self->stopDevice()){
        return Nan::ThrowError(Nan::New("Camera::stop - impossible to stop device.").ToLocalChecked());
    }
}

NAN_METHOD(Camera::getImage) {
    if (info.Length() != 3){
        return Nan::ThrowError(Nan::New("Camera::getImage - expected arguments: format, width, height.").ToLocalChecked());
    }

    if (!info[0]->IsNumber()){
        return Nan::ThrowError(Nan::New("Camera::getImage - format argument to be number.").ToLocalChecked()); 
    }
    if (!info[1]->IsNumber()){
        return Nan::ThrowError(Nan::New("Camera::getImage - width argument to be number.").ToLocalChecked()); 
    }
    if (!info[2]->IsNumber()){
        return Nan::ThrowError(Nan::New("Camera::getImage - height argument to be number.").ToLocalChecked()); 
    }

    int format = info[0]->Uint32Value();
    int output_width = info[1]->Uint32Value();
    int output_height = info[2]->Uint32Value();

    Camera* self = Nan::ObjectWrap::Unwrap<Camera>(info.This());
    self->captureImage();

    unsigned char* data = NULL;
    int size = 0;

    switch(format) {
        case 0: // RGB
            data = self->yuv420ToRGB(self->buffer.start, self->width, self->height, output_width, output_height);
            size = output_width * output_height * 3 * sizeof (uint8_t);
            break;
        case 1: // RGBA
            data = self->yuv420ToRGBA(self->buffer.start, self->width, self->height, output_width, output_height);
            size = output_width * output_height * 4 * sizeof (uint8_t);
            break;
        case 2: // RGB_JET
            data = self->yuv420ToRGBJet(self->buffer.start, self->width, self->height, output_width, output_height);
            size = output_width * output_height * 3 * sizeof (uint8_t);
            break;
        case 3: // GRAY
            data = self->yuv420ToGrayScale(self->buffer.start, self->width, self->height, output_width, output_height);
            size = output_width * output_height * sizeof (uint8_t);
            break;
        
    }
    
    std::copy(data, data + size, data);

    const auto flag = v8::ArrayBufferCreationMode::kInternalized;
    auto buf = v8::ArrayBuffer::New(info.GetIsolate(), data, size, flag);
    auto array = v8::Uint8Array::New(buf, 0, size);    
    info.GetReturnValue().Set(array);
}

bool Camera::openDevice(){
    // 1. Open capture device
    this->fd = open(this->device.c_str(), O_RDWR | O_NONBLOCK, 0);
    if (this->fd == -1){ return false; }
    
    // 2. Query the capture
    struct v4l2_capability caps;
    if (-1 == xioctl(this->fd, VIDIOC_QUERYCAP, &caps)){
        return false;
    }

    // 3. Image format
    struct v4l2_format fmt;
    memset(&fmt, 0, sizeof fmt);
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width = this->width;
    fmt.fmt.pix.height = this->height;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;
    fmt.fmt.pix.field = V4L2_FIELD_NONE;

    if (-1 == xioctl(this->fd, VIDIOC_S_FMT, &fmt)) {
        return false;
    }

    return true;
}

bool Camera::stopDevice(){
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (xioctl(this->fd, VIDIOC_STREAMOFF, &type) == -1) 
        return false;
    munmap(this->buffer.start, this->buffer.length);

    struct v4l2_requestbuffers req;
    memset(&req, 0, sizeof req);
    req.count = 0;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;
    if (xioctl(this->fd, VIDIOC_REQBUFS, &req) == -1)
        return false;

    close(this->fd);
    return true;
}

bool Camera::queryBuffers(){
    struct v4l2_requestbuffers req;
    memset(&req, 0, sizeof req);
    req.count = 1;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;
    if (-1 == xioctl(this->fd, VIDIOC_REQBUFS, &req))
    {
        perror("Requesting Buffer");
        return false;
    }

    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = 0;
    if(-1 == xioctl(this->fd, VIDIOC_QUERYBUF, &buf))
    {
        perror("Querying Buffer");
        return false;
    }

    this->buffer.length = buf.length;
    this->buffer.start = (uint8_t*)mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, this->fd, buf.m.offset);

    return true;
}


bool Camera::captureImage(){  

    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = 0;
    if(-1 == xioctl(this->fd, VIDIOC_QBUF, &buf)) {
        return false;
    }

    if(-1 == xioctl(this->fd, VIDIOC_STREAMON, &buf.type)) {
        return false;
    }

    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(this->fd, &fds);
    struct timeval tv;
    memset(&tv, 0, sizeof tv);
    tv.tv_sec = 2;
    int r = select(this->fd+1, &fds, NULL, NULL, &tv);
    if(-1 == r) {
        return false;
    }
    if(-1 == xioctl(this->fd, VIDIOC_DQBUF, &buf)) {
        return false;
    }

    return 0;
}

uint8_t* Camera::yuv420ToGrayScale(uint8_t* yuv420, uint32_t input_width, uint32_t input_height, uint32_t output_width, uint32_t output_height)
{
    float wr = (float)input_width / (float)output_width;
    float hr = (float)input_height / (float)output_height;

    uint8_t* gs = (uint8_t*)calloc(output_width * output_height, sizeof (uint8_t));
    for (size_t i = 0; i < output_height; i++) {
        for (size_t j = 0; j < output_width; j++) {
            int input_i = i * hr;
            int input_j = j * wr;
            size_t input_index = input_i * input_width + input_j;
            size_t output_index = i * output_width + j;

            gs[output_index] = yuv420[input_index];
        }
    }
    return gs;
}
uint8_t* Camera::yuv420ToRGB(uint8_t* yuv420, uint32_t input_width, uint32_t input_height, uint32_t output_width, uint32_t output_height)
{
    float wr = (float)input_width / (float)output_width;
    float hr = (float)input_height / (float)output_height;

    uint8_t* rgb = (uint8_t*)calloc(output_width * output_height * 3, sizeof (uint8_t));
    for (size_t i = 0; i < output_height; i++) {
        for (size_t j = 0; j < output_width; j++) {
            int input_i = i * hr;
            int input_j = j * wr;
            size_t input_index = input_i * input_width + input_j;
            size_t output_index = i * output_width + j;

            rgb[output_index * 3 + 0] = yuv420[input_index];
            rgb[output_index * 3 + 1] = yuv420[input_index];
            rgb[output_index * 3 + 2] = yuv420[input_index];
        }
    }
    return rgb;
}
uint8_t* Camera::yuv420ToRGBA(uint8_t* yuv420, uint32_t input_width, uint32_t input_height, uint32_t output_width, uint32_t output_height)
{
    float wr = (float)input_width / (float)output_width;
    float hr = (float)input_height / (float)output_height;

    uint8_t* rgb = (uint8_t*)calloc(output_width * output_height * 4, sizeof (uint8_t));
    for (size_t i = 0; i < output_height; i++) {
        for (size_t j = 0; j < output_width; j++) {
            int input_i = i * hr;
            int input_j = j * wr;
            size_t input_index = input_i * input_width + input_j;
            size_t output_index = i * output_width + j;

            rgb[output_index * 4 + 0] = yuv420[input_index];
            rgb[output_index * 4 + 1] = yuv420[input_index];
            rgb[output_index * 4 + 2] = yuv420[input_index];
            rgb[output_index * 4 + 3] = 255;
        }
    }
    return rgb;
}


uint8_t* Camera::yuv420ToRGBJet(uint8_t* yuv420, uint32_t input_width, uint32_t input_height, uint32_t output_width, uint32_t output_height)
{
    float wr = (float)input_width / (float)output_width;
    float hr = (float)input_height / (float)output_height;

    uint8_t* rgb = (uint8_t*)calloc(output_width * output_height * 3, sizeof (uint8_t));
    for (size_t i = 0; i < output_height; i++) {
        for (size_t j = 0; j < output_width; j++) {
            int input_i = i * hr;
            int input_j = j * wr;
            size_t input_index = input_i * input_width + input_j;
            size_t output_index = i * output_width + j;
            
            double gray = yuv420[input_index] / 255.0;
            rgb[output_index * 3 + 0] = GetJetPaletteColor(gray - 0.25) * 255;
            rgb[output_index * 3 + 1] = GetJetPaletteColor(gray) * 255;
            rgb[output_index * 3 + 2] = GetJetPaletteColor(gray + 0.25) * 255;
        }
    }
    return rgb;
}

double Camera::GetJetPaletteColor(double val)
{
  val = 2.0 * (val - 0.5);
  if ( val <= -0.75 ) return 0;
  else if ( val <= -0.25 ) return (val - (-0.75)) * (1.0-0.0) / (-0.25-(-0.75)) + 0.0;
  else if ( val <= 0.25 ) return 1.0;
  else if ( val <= 0.75 ) return (val - 0.25) * (0.0-1.0) / (0.75-0.25) + 1.0;
  else return 0.0;
}