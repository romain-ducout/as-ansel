#include <nan.h>

#include "camera.hpp"


NAN_MODULE_INIT(InitModule) {
    Camera::Init(target);
}

NODE_MODULE(asflircam, InitModule);
