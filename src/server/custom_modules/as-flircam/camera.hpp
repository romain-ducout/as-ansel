#ifndef DEF_CAMERA
#define DEF_CAMERA

#include <nan.h>
#include <string>

using namespace std;

class Camera: public Nan::ObjectWrap{
public:
    typedef struct {
        uint8_t* start;
        size_t length;
    } buffer_t;
    
    static NAN_MODULE_INIT(Init);
    static NAN_METHOD(New);

    static NAN_METHOD(getDevice);
    static NAN_METHOD(getWidth);
    static NAN_METHOD(getHeight);

    static NAN_METHOD(start);
    static NAN_METHOD(stop);
    static NAN_METHOD(getImage);

    static Nan::Persistent<v8::FunctionTemplate> constructor;

private:
    string device;
    uint32_t width;
    uint32_t height;

    int fd;
    buffer_t buffer;

    bool openDevice();
    bool queryBuffers();
    bool captureImage();
    bool stopDevice();

    static uint8_t* yuv420ToGrayScale(uint8_t* yuv420, uint32_t input_width, uint32_t input_height, uint32_t output_width, uint32_t output_height);
    static uint8_t* yuv420ToRGB(uint8_t* yuv420, uint32_t input_width, uint32_t input_height, uint32_t output_width, uint32_t output_height);
    static uint8_t* yuv420ToRGBA(uint8_t* yuv420, uint32_t input_width, uint32_t input_height, uint32_t output_width, uint32_t output_height);
    static uint8_t* yuv420ToRGBJet(uint8_t* yuv420, uint32_t input_width, uint32_t input_height, uint32_t output_width, uint32_t output_height);

    static double GetJetPaletteColor(double val);
};


#endif