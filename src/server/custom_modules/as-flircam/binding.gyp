{
    "targets": [
        {
            "target_name": "as-flircam",
            "sources": [ "main.cpp", "camera.cpp" ],
            "include_dirs" : [
                "<!(node -e \"require('nan')\")"
            ],
            "cflags" : [
                "-std=c++11"
            ]
        }
    ]
}