var flircam = require('./build/Release/as-flircam.node');

module.exports.Camera = flircam.Camera;
module.exports.format = {
    RGB: 0,
    RGBA: 1,
    RGB_JET: 2,
    GRAY: 3,
};
