/**
 * Module to manage the jobs running on drone.
 * 
 * @requires module:jobs/commands-job
 * @requires module:jobs/px4-job
 * @requires module:jobs/detector-job
 * 
 * @module managers/job-manager
 * @author Romain Ducout
 */
const CommandsJob = require(__ansel_root + 'jobs/commands-job');
const PX4Job = require(__ansel_root + 'jobs/px4-job');
const DetectorJob = require(__ansel_root + 'jobs/detector-job');

/**
 * Singleton class for managing jobs running on server.
 */
class JobManager{
    /**
     * Creates an instance of JobManager.
     */
    constructor() {
        /**
         * List of jobs running on server. 
         * @private
         * @member {Array.<module:jobs/job~Job>} 
         * */
        this.jobs = [];
    }

    /**
     * Creates and starts a new job.
     * 
     * @param {string} iType - Type of the job to start.
     * @param {Object} iOptions - Parameters of the job to start.
     * @returns {Job} Newly created and running job.
     */
    startJob(iType, iOptions){
        var new_job = this.createJob(this.jobs.length, iType, iOptions);
        this.jobs.push(new_job);
        new_job.run();
        return new_job;
    }

    /**
     * Creates a new job.
     * 
     * @param {number} iId - Id of job to create.
     * @param {string} iType - Type of the job to create.
     * @param {Object} iOptions - Parameters of the job to create.
     * @returns {Job} Newly created job.
     */
    createJob(iId, iType, iOptions){
        var job = null;
        switch (iType) {
            case 'commands':
                job = new CommandsJob(iId, iOptions);
                break;  
            case 'px4':
                job = new PX4Job(iId, iOptions);
                break;  
            case 'detector':
                job = new DetectorJob(iId, iOptions);
                break;        
            default:
                break;
        }
        return job;
    }

    /**
     * Checks if a job type actually exists.
     * 
     * @param {string} iType - Job type to test.
     * @returns {boolean} True if job type exists.
     */
    isJob(iType){
        return this.getJobTypeList().indexOf(iType) >= 0;
    }

    /**
     * Returns list of existing jobs.
     * 
     * @returns {Array} List of existing jobs.
     */
    getJobTypeList(){
        return [
            'px4',
            'commands',
            'detector'
        ]
    }

    /**
     * Gets a job based on its id.
     * 
     * @param {number} iId - Id of job to retrieve.
     * @returns {Job} Job corresponding to id.
     */
    getJob(iId){
        return this.jobs[iId];
    }

    /**
     * Deletes a job based on its id.
     * 
     * @param {number} iId - Id of job to delete.
     */
    deleteJob(iId){
        this.jobs[iId] = null;
        return;
    }
};

module.exports = new JobManager();
