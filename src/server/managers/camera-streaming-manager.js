/**
 * Module to manage the streaming via UDP.
 * 
 * @requires module:utils/constants
 * @requires module:utils/path
 * 
 * @module managers/camera-streaming-manager
 * @author Romain Ducout
 */
const Constants = require(__ansel_root + 'utils/constants');
const Path = require(__ansel_root + 'utils/path');

/**
 * Child process module.
 * Allows creating processes.
 * @const
 */
const child_process = require('child_process');

/**
 * AS-Streamer custom module.
 * Access to custom module to start/stop camera stream over UDP via gstreamer.
 * @const
 */
Streamer = null;
try {
    Streamer = require(__ansel_root + 'custom_modules/as-streamer'); 
} catch (error) {    
}

const fs = require('fs');

/**
 * Singleton class for managing camera streaming via UDP.
 */
class CameraStreamingManager{
    /**
     * Creates an instance of CameraStreamingManager.
     */
    constructor() {
        if(!Streamer){ return; }
        this.streaming = false;
        this.streamer = new Streamer();

        /**
         * Current thermal camera data. 
         * @private
         * @property {number} bitrate Streaming bitrate.
         * @property {boolean} active Indicates if the streaming is active.
         * @member {Object} 
         * */
        this.data = {
            bitrate: 3000000,
            active: false
        };
    }

    /**
     * Sets data to the camera manager.
     * 
     * @param {Object} iData - Data to set.
     */
    setData(iData){
        iData.active = this.data.active;
        this.data = Object.assign(this.data, iData);
    }

    /**
     * Gets current data of the camera manager.
     * 
     * @returns {Object} Current data.
     */
    getData(){ 
        return this.data;
    }

    /**
     * Check the streamer module for messages.
     */
    refresh(){
        var context = this;
        if(this.data.active){
            setTimeout(() => {
                context.refresh();
            }, 100);
        }         
    }

    /**
     * Starts camera streaming over UDP port 5000.
     * 
     * @param {string} iTarget - IP adress of requester.
     * @param {boolean} record - True to record stream in file.
     */
    startStream(iTarget, record){
        if(!Streamer){ return; }
        if(this.data.active){ return; }
        for(let i = 0; i < 10; i++){
            let curr_dev = `/dev/video${i}`;
                        
            if (fs.existsSync(curr_dev)) {
                if(record){
                    var date = new Date();
                    var file_name = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}_${date.getHours()}-${date.getMinutes()}-${date.getSeconds()}.mpeg`;
                    var rec_path = `${Path.getImagesPath()}/${file_name}`;
                    this.streamer.start(iTarget, curr_dev, rec_path);
                }
                else{
                    this.streamer.start(iTarget, curr_dev);
                }
                this.data.active = true; 
                this.refresh();
                
                return true;
            }
        }
        return false;
    }

    /**
     * Stops camera streaming over UDP port 5000.
     */
    stopStream(){
        if(!Streamer){ return; }
        if(!this.data.active){ return; }
        this.streamer.stop();
        this.data.active = false;
    }
};

module.exports = new CameraStreamingManager();
