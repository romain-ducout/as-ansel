/**
 * Module to manage the thermal camera connected to drone.
 * 
 * @requires module:utils/constants
 * 
 * @module managers/camera-thermal-manager
 * @author Romain Ducout
 */
const Constants = require(__ansel_root + 'utils/constants');

/**
 * JPEG turbo module.
 * Allows creating jpeg images from buffers.
 * @const
 */
const jpg = require('jpeg-turbo');

/**
 * AS-FlirCam custom module.
 * Access to custom module to retrieve thermal camera stream.
 * @const
 */
const flircam = null;
try {
    const flircam = require(__ansel_root + 'custom_modules/as-flircam');    
} catch (error) {    
}

/**
 * Thermal camera base width resolution.
 * 
 * @private
 * @type {number}
 */
var CAM_WIDTH = 640;

/**
 * Thermal camera base height resolution.
 * 
 * @private
 * @type {number}
 */
var CAM_HEIGHT = 512;

/**
 * Singleton class for managing thermal camera.
 */
class CameraThermalManager{
    /**
     * Creates an instance of CameraThermalManager.
     */
    constructor() {
        /**
         * Thermal Camera object. 
         * @private
         * @member {Object} 
         * */
        this.camera = null;

        /**
         * Current thermal camera data. 
         * @private
         * @property {number} format Camera stream format (0: B&W, 2: Jet color).
         * @property {number} quality Quality of the camera stream (0: low, 1: medium, 2: high).
         * @property {boolean} active Indicates if the camera is active.
         * @member {Object} 
         * */
        this.data = {
            format: 0,
            quality: 1,
            active: false
        };
    }

    /**
     * Initializes camera.
     * 
     * @returns {boolean} True if camera has been correctly initialized.
     */
    initCamera(){
        if(!flircam) { return; }
        for(let i = 0; i < 10; i++){
            try {
                this.camera = new flircam.Camera('/dev/video' + i, CAM_WIDTH, CAM_HEIGHT);
                this.camera.start();
                return true;
            } catch (error) {}
        }
        this.camera = null;        
        return false;
    }

    /**
     * Sets data to the camera manager.
     * 
     * @param {Object} iData - Data to set.
     */
    setData(iData){
        iData.active = this.data.active;
        this.data = Object.assign(this.data, iData);
    }

    /**
     * Gets current data of the camera manager.
     * 
     * @returns {Object} Current data.
     */
    getData(){ 
        return this.data;
    }

    /**
     * Starts camera to capture video stream and emit it.
     * 
     * @returns {boolean} True if camera successfully started.
     */
    startCapture(){
        if(!this.camera){
            var res = this.initCamera();
            if(!res){
                return false;
            }
        }

        this.data.active = true;
        this.emit();
        return true;
    }

    /**
     * Stops camera to capture video stream and to emit.
     * 
     * @returns {boolean} True if camera successfully stopped.
     */
    stopCapture(){
        try {
            this.camera.stop();
        } catch (error) {}
        this.camera = null;
        this.data.active = false;
        return true;
    }

    /**
     * Emits camera's stream while camera is started and until it is stopped.
     */
    emit(){
        if(!flircam) { return; }
        if(!this.data.active){
            return;
        }
        var context = this;

        var output_width = this.camera.getWidth();
        var output_height = this.camera.getHeight();

        if(this.data.quality === 0){
            output_width = this.camera.getWidth() * 0.25;
            output_height = this.camera.getHeight() * 0.25;
        }
        else if(this.data.quality === 1){
            output_width = this.camera.getWidth() * 0.4;
            output_height = this.camera.getHeight() * 0.4;
        }

        if(this.data.format === 0){
            var data_array = this.camera.getImage(flircam.format.GRAY, output_width, output_height);    
    
            var options = {
                format: jpg.FORMAT_GRAY,
                width: output_width,
                height: output_height,
                subsampling: jpg.SAMP_GRAY,
            }; 
        }        
        else{
            var data_array = this.camera.getImage(flircam.format.RGB_JET, output_width, output_height);    
    
            var options = {
                format: jpg.FORMAT_RGB,
                width: output_width,
                height: output_height,
                subsampling: jpg.SAMP_444,
            };
        }

        var preallocated = new Buffer(jpg.bufferSize(options))

        var jpegImageData = jpg.compressSync(data_array, preallocated, options)

        Constants.ANSEL_IO.sockets.emit('thermal', jpegImageData);

        setTimeout(() => {
            context.emit();
        }, 30);

    }

};

module.exports = new CameraThermalManager();
