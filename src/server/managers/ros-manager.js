/**
 * Module to manage the ROS running on drone.
 * 
 * @requires module:utils/constants
 * @requires module:utils/utils
 * 
 * @module managers/ros-manager
 * @author Romain Ducout
 */
const Constants = require(__ansel_root + 'utils/constants');
const Utils = require(__ansel_root + 'utils/utils');

/**
 * ROSLIB module.
 * Allows connection to ROS.
 * @const
 */
const ROSLIB = require("roslib");

/**
 * Child process module.
 * Allows creating processes.
 * @const
 */
const child_process = require('child_process');

/**
 * Singleton class for communicating with ROS.
 */
class ROSManager{
    /**
     * Creates an instance of ROSManager.
     */
    constructor() {
        var context = this;
        
        /**
         * ROS connection object. 
         * @private
         * @member {Object} 
         * */
        this.ros = null;

        Utils.getROS()
            .then(ros => {
                context.ros = ros;
                context.onConnectedROS();                
            })
            .catch(ros => {             
            });
    }

    /**
     * Initializes listeners once ROS is connected.
     */
    onConnectedROS(){
        this.rosout_listener = new ROSLIB.Topic({
            ros : this.ros,
            name : '/rosout_agg',
            messageType : 'rosgraph_msgs/Log'
        });
        this.rosout_listener.subscribe(function(res){
            //Constants.ANSEL_IO.sockets.emit('rosout', res);
        });
    }

    /**
     * Gets a node's details based on its id.
     * 
     * @param {string} iId - Id of node to retrieve.
     * @returns {Promise} Promise to the node details.
     */
    getNode(iId){
        var context = this;
        return new Promise(function(resolve, reject){
            Utils.getROS()
                .then(ros => {
                    ros.getNodeDetails(context.getRosNodeIdFull(iId), function(res){
                        resolve(res);
                    });
                })
                .catch(err => {
                    reject();
                });
        });          
    }

    /**
     * Deletes a node based on its id.
     * 
     * @param {string} iId - Id of node to delete.
     * @returns {Promise} Promise of node delete.
     */
    deleteNode(iId){
        this.execRosCmd('source /home/aerosense/as-ros/workspace/devel/setup.zsh');
        return this.execRosCmd('rosnode kill ' + this.getRosNodeIdFull(iId));
    }

    /**
     * Creates a new node.
     * 
     * @param {string} iPkg - Package name.
     * @param {string} iType - Type of node.
     * @param {string} iArgs - Arguments to the node.
     * @returns {Promise} Promise of the node created.
     */
    createNode(iPkg, iType, iArgs){
        return this.execRosCmd('rosrun ' + iPkg + ' ' + iType + ' ' + iArgs + ' &');
    }

    /**
     * Publishes a new topic.
     * 
     * @param {string} iName - Topic's name.
     * @param {string} iType - Topic's type.
     * @param {string} iArgs - Arguments for the topic.
     * @returns {Promise} Promise of the topic created.
     */
    publishTopic(iName, iType, iArgs){
        return this.execRosCmd('rostopic pub -1 ' + iName + ' ' + iType + ' ' + iArgs + ' &');
    }

    /**
     * Gets nodes currently running on ROS.
     * 
     * @returns {Promise} Promise to the list of nodes.
     */
    getNodes(){
        return new Promise(function(resolve, reject){
            Utils.getROS()
                .then(ros => {
                    ros.getNodes(function(res){
                        resolve(res);
                    });
                })
                .catch(err => {
                    reject();
                });
        });            
    }

    /**
     * Gets the full is of a node.
     * 
     * @param {string} iId - Id of node.
     * @returns {string} Full id of node.
     */
    getRosNodeIdFull(iId){
        return '/' + iId;
    }

    /**
     * Execute a command to ROS.
     * 
     * @param {string} iCmd - Command to execute.
     * @returns {Promise} Promise to the command executed.
     */
    execRosCmd(iCmd){
        var context = this;

        return new Promise(function(resolve, reject){
            context.result = {
                cwd: Constants.ROS.PATH,
                data: []
            };

            try {
                var cmd = child_process.spawn(iCmd, { 
                    cwd: Constants.ROS.PATH,
                    env: Constants.ANSEL_ENV,
                    shell: '/bin/zsh'
                });

                cmd.on('exit', () => {
                    resolve();
                });  

                cmd.on('error', () => {
                    reject();
                });  
            } catch (error) {
                reject();   
            } 
        });
    }
};

module.exports = new ROSManager();
