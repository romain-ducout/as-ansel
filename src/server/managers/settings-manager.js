/**
 * Module to manage the settings of Ansel application.
 * 
 * @requires module:utils/constants
 * @requires module:utils/path
 * @requires module:utils/utils
 * 
 * @module managers/settings-manager
 * @author Romain Ducout
 */
const Constants = require(__ansel_root + 'utils/constants');
const Path = require(__ansel_root + 'utils/path');
const Utils = require(__ansel_root + 'utils/utils');

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');

/**
 * Path module.
 * Allows path operation.
 * @const
 */
const path = require('path');

/**
 * Default Hostapd configuration file.
 * Need to replace the following parameters:
 * - UAV_SSID: SSID for WIFI.
 * - UAV_WPA_PASSPHRASE: WIFI Pass phrase.
 * 
 * @private
 * @type {string}
 */
const HOSTAPD_TEMPLATE = `interface=wlan0
hw_mode=g
channel=3
ssid=UAV_SSID
wpa=2
wpa_passphrase=UAV_WPA_PASSPHRASE
`;

/**
 * Default settings JSON.
 * 
 * @private
 * @type {Object}
 * @property {Object} app Application settings.
 * @property {Object} app.language Application language (en, ja).
 * @property {Object} app.mode Application view mode for maps (0: 2D, 1: 3D).
 * @property {Object} wifi WIFI settings.
 * @property {Object} wifi.ssid WIFI SSID.
 * @property {Object} wifi.wpa_passphrase WIFI pass phrase.
 */
var DEFAULT_SETTINGS = {
    app: {
        language: 'en',
        mode: 1,
    },    
    wifi: {
        ssid: 'as-mc03',
        wpa_passphrase: 'as-mc03-pass',
    }
}

/**
 * Singleton class for managing Ansel settings.
 */
class SettingsManager{
    /**
     * Creates an instance of SettingsManager.
     */
    constructor() {
        /**
         * Current Ansel settings. 
         * @private
         * @property {Object} app Application settings.
         * @property {Object} wifi wifi settings.
         * @member {Object} 
         * */
        this.settings = {
            app: null,
            wifi: null
        };

        /**
         * Promise of the application settings loaded. 
         * @private
         * @member {Promise} 
         * */
        this.app_settings_promise = null;
        /**
         * Promise of the wifi settings loaded. 
         * @private
         * @member {Promise} 
         * */
        this.wifi_settings_promise = null;
    }

    /**
     * Gets promise to the application settings. 
     * 
     * @returns {Promise} Promise to the application settings. 
     */
    getAppSettings(){
        if(!this.app_settings_promise){
            var context = this;
            this.app_settings_promise = new Promise(function(resolve, reject){
                fs.readFile(Path.getDataSettingsPath(), 'utf8', function(err, data) {
                    context.settings.app = DEFAULT_SETTINGS.app;
                    if (err) {
                        resolve(context.settings.app);
                        return;
                    }
                    try {                    
                        context.settings.app = JSON.parse(data);
                    } catch (error) {                    
                    }
                    resolve(context.settings.app);
                });
            });
        }
        return this.app_settings_promise;
    }

    /**
     * Gets promise to the wifi settings. 
     * 
     * @returns {Promise} Promise to the wifi settings. 
     */
    getWifiSettings(){
        if(!this.wifi_settings_promise){
            var context = this;
            this.wifi_settings_promise = new Promise(function(resolve, reject){
                fs.readFile(Constants.HOSTAPD_CONF, 'utf8', function(err, data) {
                    context.settings.wifi = {};
                    if (err) {
                        resolve(context.settings.wifi);
                        return;
                    }
                    context.settings.wifi = {
                        ssid: null,
                        wpa_passphrase: null
                    }

                    var lines = data.split('\n');
                    for(let i = 0; i < lines.length; i++){
                        var curr_param = lines[i].trim().split('=');
                        if(curr_param.length !== 2){ continue; }
                        switch (curr_param[0]) {
                            case 'ssid':
                                context.settings.wifi.ssid = curr_param[1];
                                break;
                            case 'wpa_passphrase':
                                context.settings.wifi.wpa_passphrase = curr_param[1];
                                break;                    
                            default:
                                break;
                        }
                    }

                    resolve(context.settings.wifi);
                });
            });
        }
        return this.wifi_settings_promise;
    }

    /**
     * Apply new settings to default value.
     * 
     * @param {Object} iSettings - Settings to apply.
     * @param {Object} iDefault - Default values of the settings.
     * @returns {Object} Settings applied to default.
     */
    applySettings(iSettings, iDefault){
        var res = {};
        for(var key in iDefault) res[key] = iDefault[key];
        for(var key in iSettings) res[key] = iSettings[key];
        return res;
    }

    /**
     * Save application settings
     * 
     * @param {Object} iSettings - Application settings to save.
     * @returns {Promise} Promise of the application settings saved.
     */
    saveAppSettings(iSettings){
        var context = this;
        return new Promise(function(resolve, reject){
            if(!iSettings){ resolve(); }

            context.settings.app = context.applySettings(iSettings, DEFAULT_SETTINGS.app);
            
            fs.writeFile(Path.getDataSettingsPath(), JSON.stringify(context.settings.app), (err) => {
                if (err) throw reject();
                resolve();
            });
        });
    }

    /**
     * Save wifi settings
     * 
     * @param {Object} iSettings - Wifi settings to save.
     * @returns {Promise} Promise of the wifi settings saved.
     */
    saveWifiSettings(iSettings){
        var context = this;
        var template = HOSTAPD_TEMPLATE;
        return new Promise(function(resolve, reject){
            if(!iSettings){ resolve(); }

            context.settings.wifi = context.applySettings(iSettings, DEFAULT_SETTINGS.wifi);
            template = template.replace('UAV_SSID', context.settings.wifi.ssid);
            template = template.replace('UAV_WPA_PASSPHRASE', context.settings.wifi.wpa_passphrase);
            
            fs.writeFile(Constants.HOSTAPD_CONF, template, (err) => {
                if (err) throw reject();
                resolve();
            });
        });
    }

    /**
     * Gets all settings.
     * 
     * @returns {Promise} Promise of all settings.
     */
    getSettings(){
        var res = {};
        var context = this;

        return new Promise(function(resolve, reject){
            Promise.all([context.getAppSettings(), context.getWifiSettings()])
                .then(() => {
                    resolve(context.settings);
                })
                .catch((err) => {
                    reject();
                });
        });
    }

    /**
     * Saves all settings.
     * 
     * @param {Object} iSettings - Settings to save.
     * @returns {Promise} Promise to settings saved.
     */
    saveSettings(iSettings){
        var context = this;
        return new Promise(function(resolve, reject){

            Promise.all([context.saveAppSettings(iSettings.app), context.saveWifiSettings(iSettings.wifi)])
                .then(() => {
                    resolve();
                })
                .catch(() => {
                    reject();
                });
        });
    }
};

module.exports = new SettingsManager();
