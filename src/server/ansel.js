/**
 * Ansel server application root class module.
 * 
 * @requires module:routers/api-router
 * @requires module:managers/settings-manager
 * @requires module:utils/constants
 * @requires module:utils/path
 * 
 * @module ansel
 * @author Romain Ducout
 */
 /**
 * Module path prefix.
 * 
 * @global
 * @type {string}
 */
global.__ansel_root = __dirname + '/';

const api_router = require(__ansel_root + 'routers/api-router');
const SettingsManager = require(__ansel_root + 'managers/settings-manager');
const Constants = require(__ansel_root +'utils/constants');
const Path = require(__ansel_root +'utils/path');

/**
 * HTTP module.
 * Allows to start server using HTTP.
 * @const
 */
const http = require('http');
/**
 * HTTPS module.
 * Allows to start server using HTTPS.
 * @const
 */
const https = require('https');
/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');
/**
 * Express module.
 * Setup HTTP server and routes.
 * @const
 */
const express = require('express');

/**
 * Ansel server application root class.
 */
class Ansel{

    /**
     * Creates an instance of Ansel.
     * @param {Object} iConf - Ansel configuration.
     */
    constructor(iConf) { 
        /**
         * Ansel Express application. 
         * @private
         * @member {Object} 
         * */
        this.app = null; 
        /**
         * Ansel server object. 
         * @private
         * @member {Object} 
         * */
        this.server = null;

        this.applyConf(iConf);
    }

    /**
     * Apply Ansel configuration file.
     * @param {Object} iConf - Ansel configuration.
     */
    applyConf(iConf){
        if(!iConf){ return; }

        if(iConf.SERVER_PORT){
            Constants.SERVER_PORT = iConf.SERVER_PORT;
        }
        if(iConf.HTTPS){
            Constants.SERVER_PORT = iConf.HTTPS;
        }
        if(iConf.PX4_DEV){
            Constants.PX4.DEV = iConf.PX4_DEV;
        }
        if(iConf.PX4_BAUDRATE){
            Constants.PX4.SPEED = iConf.PX4_BAUDRATE;
        }
        
        Path.applyConf(iConf);
    }

    /**
     * Starts application.
     */
    start(){
        Path.createDataFolders();

        this.createServer();
        this.initRoutes();

        this.server.listen(Constants.SERVER_PORT);
    }

    /**
     * Gets express application object.
     * 
     * @returns {Object} Express application object.
     */
    getApp(){
        return this.app;
    }

    /**
     * Gets server object.
     * 
     * @returns {Object} Server object.
     */
    getServer(){
        return this.server;
    }

    /**
     * Creates server object.
     */
    createServer(){
        this.app = express();

        if(Constants.HTTPS){
            const credentials = {
                key: fs.readFileSync(__dirname + '/encryption/server.key'),
                cert: fs.readFileSync(__dirname + '/encryption/server.crt')
            }
            this.server = https.createServer(credentials, this.app);
        }
        else{
            this.server = http.createServer(this.app);
        }
        Constants.ANSEL_IO = require('socket.io').listen(this.server);
        Constants.ANSEL_IO.sockets.on('connection', function (socket) {});
    }

    /**
     * Initializes Ansel's routes.
     */
    initRoutes(){
        this.app.get('/', function(req, res) {
            SettingsManager.getAppSettings()
                .then(function(iSettings){
                    res.sendFile(`${Path.getAppPath()}/dist/index.${iSettings.language}.html`);                    
                })
                .catch(function(){
                    res.sendFile(`${Path.getAppPath()}/dist/index.en.html`);
                })
        });

        this.app.use(express.static('dist'));
        this.app.use('/api', api_router);

        this.app.use('/plotly', express.static('node_modules/plotly.js/dist/'));
        this.app.use('/cesium', express.static('node_modules/cesium/Build/Cesium/'));
        this.app.use('/zipjs', express.static('node_modules/zip-js/WebContent/'));
        this.app.use('/material', express.static('node_modules/material-components-web/dist/'));

        this.app.use(express.static(Path.getDataPath()));

        // Add error handler
        this.app.use(function(err, req, res, next) {
            console.log('Unexpected Exception:');
            console.log(err);
            res.status(500).send({"stack" : err.stack});
        });
    }
};

module.exports = Ansel;
