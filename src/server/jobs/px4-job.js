/**
 * Job module of the PX4 related jobs.
 * 
 * @requires module:jobs/job
 * @requires module:utils/constants
 * @requires module:utils/path
 * @requires module:models/log
 * 
 * @module jobs/px4-job
 * @author Romain Ducout
 */
const Job = require(__ansel_root + 'jobs/job');
const Constants = require(__ansel_root + 'utils/constants');
const Path = require(__ansel_root + 'utils/path');
const Log = require(__ansel_root + 'models/log');

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');

/**
 * Child process module.
 * Allows creating processes.
 * @const
 */
const child_process = require('child_process');

/**
 * Job used to extract logs from PX4 to JETSON.
 * The commands available are the following:
 * - deploy: Deploys logs from PX4 to JETSON.
 * - delete: Deletes a log on PX4.
 * - list: lists the logs available on PX4.
 * 
 * @extends module:jobs/job~Job
 */
class PX4Job extends Job{  
    /**
     * Creates an instance of PX4Job.
     * @param {number} iId - Id of job.
     * @param {Object} iOptions - Options of job.
     */    
    constructor(iId, iOptions) {
        super(iId, iOptions);

        /**
         * Indicates the current log identifier in the list of logs to process. 
         * @private
         * @member {number} 
         * */
        this.curr_log_ind = 0;
    }

    /**
     * Starts the execution of the job.
     * 
     * @override
     */
    run(){
        switch (this.options.cmd) {
            case 'logs-deploy':
                this.runLogsDeploy();
                break;
            case 'logs-delete':
                this.runLogsDelete();
                break;
            case 'logs-list':
                this.runLogsList();                
                break;
            case 'parameters-list':
                this.runParametersList();                
                break;
            default:
                this.error = true;
                this.emit();
                break;
        }        
    }

    /**
     * Gets string command for PX4 utility scrip.
     * 
     * @param {string} iArg - Arguments to add to script command.
     * @returns {string} String command for PX4 utility scrip.
     */
    getPX4UtilsScriptCmd(iArg){
        return `${Path.getAppScriptsPath()}/${Constants.PX4.UTILS_SCRIPT_NAME} ${Constants.PX4.DEV} ${Constants.PX4.SPEED} ${iArg}`;
    }

    /**
     * Runs PX4 command to retrieve list of drone parameters.
     */
    runParametersList(){
        var context = this;
        child_process.exec(this.getPX4UtilsScriptCmd('get_parameters_list'), (err, stdout, stderr) => {
            if (err) {
                context.error = true;
                context.emit();
                return;
            }

            var res = [];
            var lines = stdout.split('\n');
            for(let i = 0; i < lines.length; i++){
                if(lines[i].length === 0){ continue; }
                let curr_param_str = lines[i].trim();
                let curr_param_split = curr_param_str.split(':');
                if(curr_param_split.length < 2){ continue; }

                let curr_param_name = curr_param_split[0].trim();
                let curr_param_value = curr_param_split[1].trim();

                if(
                    curr_param_name.startsWith('x ') ||
                    curr_param_name.startsWith('+ ') ||
                    curr_param_name.startsWith('* ')
                ){
                    curr_param_name = curr_param_name.substring(2);
                }

                res.push({
                    name: curr_param_name,
                    mav: 1,
                    id: 50,
                    value: parseFloat(curr_param_value),
                    type: 9
                });
            }

            res = res.sort(function(a, b){
                if(a.name < b.name){
                    return -1;
                }
                else if(a.name === b.name){
                    return 0
                }
                return 1;
            });

            context.result = res;
            context.done = true;
            context.emit();
        });
    }
    
    /**
     * Runs PX4 command to retrieve list of logs on PX4.
     */
    runLogsList(){
        var context = this;
        child_process.exec(this.getPX4UtilsScriptCmd('get_log_list'), (err, stdout, stderr) => {
            if (err) {
                context.error = true;
                context.emit();
                return;
            }

            var res = [];
            var lines = stdout.split('\n');
            for(let i = 0; i < lines.length; i++){
                if(lines[i].length === 0){ continue; }
                let curr_log = new Log(lines[i].trim());
                res.push(curr_log);
            }

            context.result = res;
            context.done = true;
            context.emit();
        });
    }
    
    /**
     * Runs PX4 command to deploy a list of logs.
     */
    runLogsDeploy(){
        if(this.options.logs.length === 0){
            this.done = true;
            this.emit();
            return;
        }

        var context = this;
        this.curr_log_ind = 0;

        var on_log_deployed = function(){
            context.curr_log_ind++;
            if(context.curr_log_ind >= context.options.logs.length){
                context.done = true;
                context.emit();
            }
            else{
                context.deployLog(context.options.logs[context.curr_log_ind], on_log_deployed);                
            }
        };

        this.deployLog(this.options.logs[this.curr_log_ind], on_log_deployed);
    }

    /**
     * Runs PX4 command to delete a given log on PX4.
     */
    runLogsDelete(){
        if(this.options.logs.length === 0){
            this.done = true;
            this.emit();
            return;
        }

        this.done = true;
        this.emit();
    }

    /**
     * Emit job progress to client.
     * 
     * @param {string} iOperation - Operation name.
     */
    emitProgress(iOperation){
        switch (iOperation) {
            case 0:
                this.message = `Copying log ${this.options.logs[this.curr_log_ind]} (${this.curr_log_ind} / ${this.options.logs.length}).`;                
                break;
            case 1:
                this.message = `Extracting log ${this.options.logs[this.curr_log_ind]} (${this.curr_log_ind} / ${this.options.logs.length}).`;                
                break;
            case 2:
                this.message = `Deleting binary of log ${this.options.logs[this.curr_log_ind]} (${this.curr_log_ind} / ${this.options.logs.length}).`;                 
                break;
        }
        this.progress = this.curr_log_ind / this.options.logs.length;
        this.emit();
    }

    /**
     * Deploys logs from PX4 from JETSON.
     * 
     * @param {string} iLog - Identifier of log to deploy.
     * @param {Function} iCallback - Callback when logs are deployed.
     */
    deployLog(iLog, iCallback){
        var context = this;

        var onError = function(err){
            console.log(err)
            iCallback();
        };

        this.emitProgress(0);
        this.downloadBinary(iLog)
            .then(() => {
                context.emitProgress(1);
                context.extractBinary(iLog)
                    .then(() => {
                        context.emitProgress(2);
                        context.deleteBinary(iLog)
                            .then(() => {
                                iCallback(); 
                            })
                            .catch(onError);
                    })
                    .catch(onError);
            })
            .catch(onError);
    }

    /**
     * Downloads binary file of a log from PX4 from JETSON
     * 
     * @param {string} iLog - Identifier of log to deploy.
     * @returns {Promise} Promise of the binary downloaded.
     */
    downloadBinary(iLog){
        var context = this;
        return new Promise(function(resolve, reject){
            child_process.exec(context.getPX4UtilsScriptCmd('get_log ' + iLog + ' ' + Path.getDataTMPPath()), (err, stdout, stderr) => {
                if (err) {
                    reject();
                    return;
                }
                resolve();
            });
        });
    }

    /**
     * Extract binary of a log downloaded on JETSON.
     * 
     * @param {string} iLog - Identifier of log to deploy.
     * @returns {Promise} Promise of the binary extracted.
     */
    extractBinary(iLog){
        var context = this;
        return new Promise(function(resolve, reject){            
            var extract_data_cmd = `${Path.getAppScriptsPath()}/${Constants.PX4.LOGS_DUMP_SCRIPT_NAME} ${context.getLogBinaryPath(iLog)} > ${context.getLogDataPath(iLog)}`;
            var extract_params_cmd = `${Path.getAppScriptsPath()}/${Constants.PX4.LOGS_DUMP_SCRIPT_NAME} ${context.getLogBinaryPath(iLog)} -p > ${context.getLogParamsPath(iLog)}`;

            try {                
                fs.mkdirSync(context.getLogFolderPath(iLog));
            } catch (error) {}
            
            child_process.exec(extract_data_cmd, (err, stdout, stderr) => {
                if (err) {
                    reject();
                    return;
                }
                child_process.exec(extract_params_cmd, (err, stdout, stderr) => {
                    if (err) {
                        reject();
                        return;
                    }
                    resolve();
                });
            });
        });
    }

    /**
     * Deletes a binary from JETSON
     * 
     * @param {string} iLog - Identifier of log to deploy.
     * @returns {Promise} Promise of the binary deleted.
     */
    deleteBinary(iLog){
        var log_path = this.getLogBinaryPath(iLog);
        return new Promise(function(resolve, reject){
            try {
                fs.unlinkSync(log_path);
            } catch (error) { }
            resolve();
        });
    }

    /**
     * Gets binary path of a given log.
     * 
     * @param {string} iLog - Id of log.
     * @returns {string} Binary path of log.
     */
    getLogBinaryPath(iLog){
        return `${Path.getDataTMPPath()}/${iLog}.bin`;
    }
    /**
     * Gets log folder path of a given log.
     * 
     * @param {string} iLog - Id of log.
     * @returns {string} Log folder path of log.
     */
    getLogFolderPath(iLog){
        return `${Path.getDataTMPPath()}/${iLog}`;
    }
    /**
     * Gets log data CSV file path of a given log.
     * 
     * @param {string} iLog - Id of log.
     * @returns {string} Log data CSV file path of log.
     */
    getLogDataPath(iLog){
        return `${this.getLogFolderPath(iLog)}/${iLog}.csv`;
    }
    /**
     * Gets log parameters file path of a given log.
     * 
     * @param {string} iLog - Id of log.
     * @returns {string} Log parameters file path of log.
     */
    getLogParamsPath(iLog){
        return `${this.getLogFolderPath(iLog)}/${iLog}.txt`;
    }
};

module.exports = PX4Job;
