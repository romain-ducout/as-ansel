/**
 * Job module for running long operations on servers.
 * 
 * @requires module:utils/constants
 * 
 * @module jobs/job
 * @author Romain Ducout
 */
const Constants = require(__ansel_root + 'utils/constants');

/**
 * Abstract class that represents a job to be run on server.
 * A job is a long operation on server that should not be made using a simple HTTP request.
 */
class Job{
    /**
     * Creates an instance of Job.
     * @param {number} iId - Id of job.
     * @param {Object} iOptions - Options of job.
     */
    constructor(iId, iOptions) {
        /**
         * Job identifier. 
         * @private
         * @member {number} 
         * */
        this.id = iId;
        /**
         * Job's options. 
         * @private
         * @member {Object} 
         * */
        this.options = iOptions;
        /**
         * Progress value of the job [0, 1]. 
         * @private
         * @member {number} 
         * */
        this.progress = 0;
        /**
         * True if the job encountered an error. 
         * @private
         * @member {boolean} 
         * */
        this.error = false;
        /**
         * True if the job is done and false if still running. 
         * @private
         * @member {boolean} 
         * */
        this.done = false;
        /**
         * Job result. 
         * @private
         * @member {any} 
         * */
        this.result = null;
        /**
         * Job progress message. 
         * @private
         * @member {string} 
         * */
        this.message = null;
    }

    /**
     * Starts the execution of the job.
     * To be implemented.
     * 
     * @abstract
     */
    run(){
    }

    /**
     * Reads the status of the jobs for the client.
     * 
     * @returns {Object} JSON representation of the job.
     */
    read(){
        return this.toJSON();
    }

    /**
     * Emits via sockets the status of the job.
     */
    emit(){
        Constants.ANSEL_IO.sockets.emit('job-' + this.id, this.read());
    }

    /**
     * Gets JSON representation of the job.
     * 
     * @returns {Object} JSON representation of the job.
     */
    toJSON(){
        return {
            id: this.id,
            options: this.options,
            progress: this.progress,
            error: this.error,
            done: this.done,
            result: this.result,
            message: this.message
        };
    }
};

module.exports = Job;
