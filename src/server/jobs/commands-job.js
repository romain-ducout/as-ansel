/**
 * Job module of the commands related jobs.
 * 
 * @requires module:jobs/job
 * @requires module:utils/constants
 * 
 * @module jobs/commands-job
 * @author Romain Ducout
 */
const Job = require(__ansel_root + 'jobs/job');
const Constants = require(__ansel_root + 'utils/constants');

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');

/**
 * Child process module.
 * Allows creating processes.
 * @const
 */
const child_process = require('child_process');

/**
 * Job used to execute UNIX commands on drone.
 * 
 * @extends module:jobs/job~Job
 */
class CommandsJob extends Job{
    /**
     * Creates an instance of CommandsJob.
     * @param {number} iId - Id of job.
     * @param {Object} iOptions - Options of job.
     */
    constructor(iId, iOptions) {
        super(iId, iOptions);

        /**
         * Command's process. 
         * @private
         * @member {Object} 
         * */
        this.cmd = null;
    }

    /**
     * Starts the execution of the job.
     * 
     * @override
     */
    run(){
        var context = this;

        this.result = {
            data: []
        };
        try {
            this.cmd = child_process.spawn(this.options.cmd, { 
                env: Constants.ANSEL_ENV,
                shell: '/bin/zsh'
            });

            this.cmd.stdout.on('data', function(data) {
                context.result.data.push([0, data.toString()]);
                context.emit();
            });

            this.cmd.stderr.on('data', function(data) {
                context.result.data.push([1, data.toString()]);
                context.emit();
            });

            this.cmd.on('exit', (code) => {
                context.done = true;
                context.emit();
            });  
        } catch (error) {
            context.error = true;  
            context.emit(); 
        }     
    }
    
    /**
     * Reads the status of the jobs for the client.
     * After reading, clear the content of result.
     * 
     * @override
     * @returns {Object} JSON representation of the job.
     */
    read(){
        var data = JSON.parse(JSON.stringify(this.toJSON()));
        this.result.data = [];
        return data;
    }
};

module.exports = CommandsJob;
