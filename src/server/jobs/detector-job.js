/**
 * Job module of the detector related jobs.
 * 
 * @requires module:jobs/job
 * @requires module:utils/constants
 * @requires module:utils/path
 * 
 * @module jobs/detector-job
 * @author Romain Ducout
 */
const Job = require(__ansel_root + 'jobs/job');
const Constants = require(__ansel_root + 'utils/constants');
const Path = require(__ansel_root + 'utils/path');

/**
 * FS module.
 * Allows access to local files.
 * @const
 */
const fs = require('fs');

/**
 * Child process module.
 * Allows creating processes.
 * @const
 */
const child_process = require('child_process');

/**
 * Job used to search objects through the images of a flight.
 * 
 * @extends module:jobs/job~Job
 */
class DetectorJob extends Job{    
    /**
     * Creates an instance of DetectorJob.
     * @param {number} iId - Id of job.
     * @param {Object} iOptions - Options of job.
     */
    constructor(iId, iOptions) {
        super(iId, iOptions);
    }

    /**
     * Starts the execution of the job.
     * 
     * @override
     */
    run(){   
        var context = this;

        var flight_dir = `${Path.getImagesPath()}/${this.options.flight}`;
        var output_dir = `${Path.getDataFlightsPath()}/${this.options.flight}`;

        if (!fs.existsSync(output_dir)){
            fs.mkdirSync(output_dir);
        }

        var command = `python3 ${Constants.SCRIPTS.MARKER_DETECTION_FOLDER}/${Constants.SCRIPTS.MARKER_DETECTION_SCRIPT} -i ${flight_dir} -o ${output_dir}/markers.json`;

        var cmd = child_process.spawn(command, { 
            shell: '/bin/zsh'
        });

        cmd.stdout.on('data', function(data) {
            var lines = data.toString().match(/[^\r\n]+/g);
            for(let i = 0; i < lines.length; i++){
                let curr_line = lines[i];
                let curr_split = curr_line.split(':');
                if(curr_split && curr_split.length === 3){
                    context.progress = parseInt(curr_split[1]) / parseInt(curr_split[2]);
                }
            }
            context.emit();
        });

        cmd.on('exit', () => {
            context.done = true;
            context.emit();
        });  

        cmd.on('error', () => {
            context.error = true;
            context.emit();
        }); 
    }
};

module.exports = DetectorJob;
