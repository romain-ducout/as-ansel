/**
 * Router module for cameras.
 * 
 * @requires module:managers/camera-thermal-manager
 * 
 * @module routers/cameras-router
 * @author Romain Ducout
 */
const ThermalManager = require(__ansel_root + 'managers/camera-thermal-manager');
const StreamingManager = require(__ansel_root + 'managers/camera-streaming-manager');

/**
 * Express module.
 * @const
 */
const express = require('express');

/**
 * Express router for cameras.
 * @const
 */
var router = express.Router();

/**
 * Starts thermal camera.
 * 
 * @function
 * @name GET - /thermal/start
 */
router.get('/thermal/start', (req, res) => {
    var started = ThermalManager.startCapture();
    if(started){
        res.send(ThermalManager.getData());   
    }
    else{
        res.status(500);
        res.send();        
    } 
});

/**
 * Stops thermal camera.
 * 
 * @function
 * @name GET - /thermal/stop
 */
router.get('/thermal/stop', (req, res) => {
    ThermalManager.stopCapture();  
    res.send(ThermalManager.getData());   
});

/**
 * Gets thermal camera data.
 * 
 * @function
 * @name GET - /thermal
 * @return {Object} Camera data.
 */
router.get('/thermal', (req, res) => {
    res.send(ThermalManager.getData());   
});

/**
 * Sets thermal camera data.
 * 
 * @function
 * @name POST - /thermal
 * @param {Object} body - Camera data.
 */
router.post('/thermal', (req, res) => {
    ThermalManager.setData(req.body);  
    res.send(ThermalManager.getData());   
});


/**
 * Starts camera streaming via UDP.
 * 
 * @function
 * @name GET - /streaming/start
 */
router.get('/streaming/start', (req, res) => {
    var target = req.connection.remoteAddress;
    target = target.replace(/^.*:/, '');

    var record = false;
    if(req.query.rec){
        record = true;
    }

    StreamingManager.startStream(target, record);
    res.send(StreamingManager.getData());
});

/**
 * Stops camera streaming via UDP.
 * 
 * @function
 * @name GET - /streaming/stop
 */
router.get('/streaming/stop', (req, res) => {
    StreamingManager.stopStream();
    res.send(StreamingManager.getData());
});

/**
 * Gets current streaming data.
 * 
 * @function
 * @name GET - /streaming
 */
router.get('/streaming', (req, res) => {
    res.send(StreamingManager.getData());
});

/**
 * Sets streaming data.
 * 
 * @function
 * @name POST - /streaming
 */
router.post('/streaming', (req, res) => {
    StreamingManager.setData(req.body);  
    res.send(StreamingManager.getData());
});
module.exports = router;