/**
 * Router module for flights.
 * 
 * @requires module:models/flight
 * @requires module:models/image
 * 
 * @module routers/flights-router
 * @author Romain Ducout
 */
const Flight = require(__ansel_root + 'models/flight');
const Image = require(__ansel_root + 'models/image');

/**
 * Express module.
 * @const
 */
const express = require('express');

/**
 * Express router for flight.
 * @const
 */
var router = express.Router();

/**
 * Gets list of flights present on drone.
 * 
 * @function
 * @name GET - /
 * @return {Array.<string>} List of flight identifiers.
 */
router.get('/', (req, res, next) => {
    Flight.getFlights()
        .then(flights => { 
            res.send(flights); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        });; 
});

/**
 * Gets information of a given flight.
 * 
 * @function
 * @name GET - /:fligh_id
 * @param {string} fligh_id - Flight identifier.
 * @return {Object} Flight data.
 */
router.get('/:fligh_id', (req, res) => {
    Flight.getFlight(req.params.fligh_id)
        .then(flight => { 
            res.send(flight.getData()); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        });
});

/**
 * Deletes a given flight.
 * 
 * @function
 * @name DELETE - /:fligh_id
 * @param {string} fligh_id - Flight identifier.
 */
router.delete('/:fligh_id', (req, res) => {
    Flight.deleteFlight(req.params.fligh_id)
        .then(flight => { 
            res.send(); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        });
});

/**
 * Gets information of a given image.
 * 
 * @function
 * @name GET - /:fligh_id/images/:image_id
 * @param {string} fligh_id - Flight identifier.
 * @param {string} image_id - Image identifier.
 * @return {Object} Image data.
 */
router.get('/:fligh_id/images/:image_id', (req, res) => {
    var image = new Image(req.params.fligh_id, req.params.image_id);
    
    var exif = false;
    if(req.query.exif){
        exif = req.query.exif =='true';
    }

    image.computeData(exif)
        .then(() => { 
            res.send(image.getData(false));
        })
        .catch(() => { 
            res.status(404); 
            res.send();
        });
});

/**
 * Gets a given image file.
 * 
 * @function
 * @name GET - /:fligh_id/images/:image_id/image
 * @param {string} fligh_id - Flight identifier.
 * @param {string} image_id - Image identifier.
 * @param {number} width - Query - Image width size.
 * @return {File} Image file.
 */
router.get('/:fligh_id/images/:image_id/image', (req, res) => {
    var image = new Image(req.params.fligh_id, req.params.image_id);

    image.getImageFile(req.query.width)
        .then(image => { 
            res.contentType('image/jpeg');
            res.send(image); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        });
});

module.exports = router;