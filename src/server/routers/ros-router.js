/**
 * Router module for ROS.
 * 
 * @requires module:managers/ros-manager
 * 
 * @module routers/ros-router
 * @author Romain Ducout
 */
const ROSManager = require(__ansel_root + 'managers/ros-manager');

/**
 * Express module.
 * @const
 */
const express = require('express');

/**
 * Express router for ROS connection.
 * @const
 */
var router = express.Router();

/**
 * Gets list of running nodes on ROS.
 * 
 * @function
 * @name GET - /nodes
 * @return {Object} List of nodes.
 */
router.get('/nodes', (req, res) => {
    ROSManager.getNodes()
        .then(nodes => { 
            res.send(nodes); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        }); 
});

/**
 * Gets a specific ROS node's data.
 * 
 * @function
 * @name GET - /nodes/:node_id
 * @param {string} node_id - Node identifier.
 * @return {Object} Node's data.
 */
router.get('/nodes/:node_id', (req, res) => {
    ROSManager.getNode(req.params.node_id)
        .then(data => { 
            res.send(data); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        }); 
});

/**
 * Deletes a running ROS node.
 * 
 * @function
 * @name DELETE - /nodes/:node_id
 * @param {string} node_id - Node identifier.
 */
router.delete('/nodes/:node_id', (req, res) => {
    ROSManager.deleteNode(req.params.node_id)
        .then(() => { 
            res.send(); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        }); 
});

/**
 * Creates and start a new node.
 * 
 * @function
 * @name POST - /nodes
 * @param {Object} body - Node's parameters.
 */
router.post('/nodes', (req, res) => {
    var node_desc = req.body;
    if(!node_desc || !node_desc.type || !node_desc.pkg){
        res.status(404); 
        res.send();
        return;
    }
    ROSManager.createNode(node_desc.pkg, node_desc.type, node_desc.args)
        .then(() => { 
            res.send(); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        }); 
});

/**
 * Publishes a new topic.
 * 
 * @function
 * @name POST - /topics
 * @param {Object} body - Topic's parameters.
 */
router.post('/topics', (req, res) => {
    var topic_desc = req.body;
    if(!topic_desc || !topic_desc.type || !topic_desc.name){
        res.status(404); 
        res.send();
        return;
    }
    ROSManager.publishTopic(topic_desc.name, topic_desc.type, topic_desc.args)
        .then(() => { 
            res.send(); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        }); 
});

module.exports = router;