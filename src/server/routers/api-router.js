/**
 * API router module for Ansel application.
 * This simply attaches specific routers to api router.
 * 
 * @requires module:routers/flights-router
 * @requires module:routers/settings-router
 * @requires module:routers/cameras-router
 * @requires module:routers/jobs-router
 * @requires module:routers/logs-router
 * @requires module:routers/ros-router
 * @requires module:routers/drone-router
 * 
 * @module routers/api-router
 * @author Romain Ducout
 */
const flights_router = require(__ansel_root + 'routers/flights-router');
const settings_router = require(__ansel_root + 'routers/settings-router');
const cameras_router = require(__ansel_root + 'routers/cameras-router');
const jobs_router = require(__ansel_root + 'routers/jobs-router');
const logs_router = require(__ansel_root + 'routers/logs-router');
const ros_router = require(__ansel_root + 'routers/ros-router');
const drone_router = require(__ansel_root + 'routers/drone-router');

/**
 * Express module.
 * @const
 */
const express = require('express');

/**
 * Body parser module.
 * Allows to parse body of requests in JSON.
 * 
 * @const
 */
const bodyParser = require('body-parser');

/**
 * Express Ansel API router.
 * This router is the one on which to mount other routers.
 * This router has the following routers mounted
 * 
 * @const
 */
var router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.use('/flights', flights_router);
router.use('/settings', settings_router);
router.use('/cameras', cameras_router);
router.use('/jobs', jobs_router);
router.use('/logs', logs_router);
router.use('/ros', ros_router);
router.use('/drone', ros_router);

module.exports = router;
