/**
 * Router module for settings.
 * 
 * @requires module:managers/settings-manager
 * 
 * @module routers/settings-router
 * @author Romain Ducout
 */
const SettingsManager = require(__ansel_root + 'managers/settings-manager');

/**
 * Express module.
 * @const
 */
const express = require('express');

/**
 * Express router for settings.
 * @const
 */
var router = express.Router();

/**
 * Gets current Ansel settings.
 * 
 * @function
 * @name GET - /
 * @return {Object} Current settings.
 */
router.get('/', (req, res) => {
    SettingsManager.getSettings()
        .then(settings => { 
            res.send(settings); 
        })
        .catch(err => { 
            res.status(500); 
            res.send(); 
        }); 
});

/**
 * Sets Ansel settings.
 * 
 * @function
 * @name POST - /
 * @param {Object} body - Settings to save.
 */
router.post('/', (req, res) => {
    SettingsManager.saveSettings(req.body)
        .then(settings => { 
            res.send(); 
        })
        .catch(err => { 
            res.status(500); 
            res.send(); 
        }); 
});

module.exports = router;