/**
 * Router module for jobs.
 * 
 * @requires module:managers/job-manager
 * 
 * @module routers/jobs-router
 * @author Romain Ducout
 */
const JobManager = require(__ansel_root + 'managers/job-manager');

/**
 * Express module.
 * @const
 */
const express = require('express');

/**
 * Express router for jobs.
 * @const
 */
var router = express.Router();

/**
 * Starts a new job.
 * 
 * @function
 * @name POST - /
 * @param {Object} body - Job to create data.
 * @return {Object} Job created data.
 */
router.post('/', (req, res) => {
    var job_desc = req.body;
    if(!job_desc.type){
        res.status(404); 
        res.send();
        return;
    }
    if(!JobManager.isJob(job_desc.type)){
        res.status(404); 
        res.send();
        return;
    }
    var job = JobManager.startJob(job_desc.type, job_desc.options);
    res.send(job.toJSON());
});

/**
 * Deletes a job.
 * 
 * @function
 * @name DELETE - /:job_id
 * @param {string} job_id - Job identifier.
 * @return {Object} Job deleted data.
 */
router.delete('/:job_id', (req, res) => {
    var job = JobManager.getJob(req.params.job_id);
    if(!job){
        res.status(404); 
        res.send();         
    }
    JobManager.deleteJob(req.params.job_id);
    res.send(job.toJSON());     
});

module.exports = router;