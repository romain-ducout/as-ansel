/**
 * Router module for logs.
 * 
 * @requires module:models/log
 * 
 * @module routers/logs-router
 * @author Romain Ducout
 */
const Log = require(__ansel_root + 'models/log');

/**
 * Express module.
 * @const
 */
const express = require('express');

/**
 * Express router for logs.
 * @const
 */
var router = express.Router();

/**
 * Gets list of logs deployed on drone.
 * 
 * @function
 * @name GET - /
 * @return {Array} List of logs.
 */
router.get('/', (req, res) => {
    Log.getLogs()
        .then(logs => { 
            res.send(logs); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        }); 
});

/**
 * Get a specific log data.
 * 
 * @function
 * @name GET - /:log_id
 * @param {string} log_id - Log identifier.
 * @return {Object} Log data.
 */
router.get('/:log_id', (req, res) => {
    Log.getLog(req.params.log_id)
        .then((log) => { 
            res.send(log); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        }); 
});

/**
 * Deletes a specific log.
 * 
 * @function
 * @name DELETE - /:job_id
 * @param {string} log_id - Log identifier.
 */
router.delete('/:log_id', (req, res) => {
    Log.deletetLog(req.params.log_id)
        .then(() => { 
            res.send(); 
        })
        .catch(err => { 
            res.status(404); 
            res.send();
        }); 
});

module.exports = router;
