/**
 * Router module for drone.
 * 
 * @requires module:models/drone
 * 
 * @module routers/drone-router
 * @author Romain Ducout
 */
const Drone = require(__ansel_root + 'models/drone');

/**
 * Express module.
 * @const
 */
const express = require('express');

/**
 * Express router for drone.
 * @const
 */
var router = express.Router();

/**
 * Gets drone status.
 * 
 * @function
 * @name GET - /
 * @return {Object} Drone data.
 */
router.get('/', (req, res) => {    
    res.send(Drone.getData());     
});

module.exports = router;